# coding: utf-8
"""Test program for simulation.

To run all tests just type:
    python -m unittest test_core_simulation

To run only a class test:
    python -m unittest test_core_simulation.TestClass

To run only a specific test:
    python -m unittest test_core_simulation.TestClass.test_method

"""
# -- imports ------------------------------------------------------------------
import unittest
import datetime

from libhydro.core import (simulation, modeleprevision, sitehydro,
                           intervenant as _intervenant)


# -- strings ------------------------------------------------------------------
__version__ = '0.1.8'
__date__ = '2015-05-24'

# HISTORY
# V0.1 - 2013-08-07
#   first shot


# -- class TestPrevisionTendance ----------------------------------------------
class TestPrevisionTendance(unittest.TestCase):

    """PrevisionTendance class tests."""

    def test_base_01(self):
        """Simple prevision."""
        dte = '2012-05-18 18:36'
        res = 33.5
        p = simulation.PrevisionTendance(dte, res)
        self.assertEqual(
            p.item(),
            (datetime.datetime(2012, 5, 18, 18, 36), 'moy', res, 0))

    def test_base_02(self):
        """Prevision with tendency."""
        dte = '2012-05-18 00:00'
        res = 33.5
        tend = 'min'
        incertdte = 5
        p = simulation.PrevisionTendance(dte=dte, tend=tend, res=res,
                                         incertdte=incertdte)
        self.assertEqual(
            p.item(),
            (datetime.datetime(2012, 5, 18), tend, res, incertdte))
        self.assertEqual(
            (p['dte'].item(), p['res'].item(), p['tend'].item(), p['incertdte'].item()),
            (datetime.datetime(2012, 5, 18), res, tend, incertdte))

    def test_str_01(self):
        """Test __str__ method with minimum values."""
        dte = '2012-05-18 00:00'
        res = 33
        p = simulation.PrevisionTendance(dte=dte, res=res)
        self.assertTrue(p.__str__().rfind('de tendance') > -1)
        self.assertTrue(p.__str__().rfind('UTC') > -1)

    def test_error_01(self):
        """Date error."""
        simulation.PrevisionTendance(**{'dte': '2012-10-10 10', 'res': 25.8})
        self.assertRaises(
            ValueError,
            simulation.PrevisionTendance,
            **{'dte': '2012-10-55', 'res': 25.8})

    def test_error_02(self):
        """Test res error."""
        simulation.PrevisionTendance(**{'dte': '2012-10-10 10:10', 'res': 10})
        self.assertRaises(
            ValueError,
            simulation.PrevisionTendance,
            **{'dte': '2012-10-10 10:10', 'res': 'xxx'})

    def test_error_03(self):
        """tendance error."""
        simulation.PrevisionTendance(
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'tend': 'moy'})
        self.assertRaises(
            ValueError,
            simulation.PrevisionTendance,
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'tend': 'median'})
        self.assertRaises(
            ValueError,
            simulation.PrevisionTendance,
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'tend': 'mediane'})


# -- class TestPrevisionPrb ---------------------------------------------------
class TestPrevisionPrb(unittest.TestCase):

    """PrevisionPrb class tests."""

    def test_base_01(self):
        """Simple prevision."""
        dte = '2012-05-18 18:36'
        res = 33.5
        p = simulation.PrevisionPrb(dte, res)
        self.assertEqual(
            p.item(),
            (datetime.datetime(2012, 5, 18, 18, 36), res, 50))

    def test_base_02(self):
        """Prevision with probability."""
        dte = '2012-05-18 00:00'
        res = 33.5
        prb = 22.3
        p = simulation.PrevisionPrb(dte=dte, res=res, prb=prb)
        self.assertEqual(
            p.item(),
            (datetime.datetime(2012, 5, 18), res, int(prb)))
        self.assertEqual(
            (p['dte'].item(), p['res'].item(), p['prb'].item()),
            (datetime.datetime(2012, 5, 18), res, int(prb)))

    def test_str_01(self):
        """Test __str__ method with minimum values."""
        dte = '2012-05-18 00:00'
        res = 33
        p = simulation.PrevisionPrb(dte=dte, res=res)
        self.assertTrue(p.__str__().rfind('avec une probabilite') > -1)
        self.assertTrue(p.__str__().rfind('UTC') > -1)

    def test_error_01(self):
        """Date error."""
        simulation.PrevisionPrb(**{'dte': '2012-10-10 10', 'res': 25.8})
        self.assertRaises(
            ValueError,
            simulation.PrevisionPrb,
            **{'dte': '2012-10-55', 'res': 25.8})

    def test_error_02(self):
        """Test res error."""
        simulation.PrevisionPrb(**{'dte': '2012-10-10 10:10', 'res': 10})
        self.assertRaises(
            ValueError,
            simulation.PrevisionPrb,
            **{'dte': '2012-10-10 10:10', 'res': 'xxx'})

    def test_error_03(self):
        """Prb error."""
        simulation.PrevisionPrb(
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'prb': 22.3})
        self.assertRaises(
            ValueError,
            simulation.PrevisionPrb,
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'prb': -1})
        self.assertRaises(
            ValueError,
            simulation.PrevisionPrb,
            **{'dte': '2012-10-10 10:10', 'res': 25.8, 'prb': 111})


class TestPrevisionDeterministe(unittest.TestCase):
    """PrevisionDeterministe class tests."""

    def test_base_01(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        res = 8956321.2569
        incertdte = 15
        prv = simulation.PrevisionDeterministe(dte=dte, res=res,
                                               incertdte=incertdte)
        self.assertEqual(
            (prv['dte'].item(), prv['res'].item(), prv['incertdte'].item()),
            (dte, res, incertdte)
        )
        strprv = prv.__str__()
        self.assertEqual(strprv,
                         ('8956321.2569 pour le 1953-05-18 à 18:05:33 UTC'
                          ' (incertitude de 15 minutes)'))

    def test_res(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        incertdte = -8
        data = [
            (0, 0.0),
            ('153', 153.0),
            (-8.4, -8.4)
        ]
        for res, expected in data:
            prv = simulation.PrevisionDeterministe(dte=dte, res=res,
                                                   incertdte=incertdte)
        self.assertEqual(prv['res'].item(), expected)

    def test_incertdte(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        res = 157.6
        prv = simulation.PrevisionDeterministe(dte=dte, res=res)
        self.assertEqual(prv['incertdte'], 0)
        data = [
            (0, 0),
            ('153', 153),
            (-8.4, -8)
        ]
        for incertdte, expected in data:
            prv = simulation.PrevisionDeterministe(dte=dte, res=res,
                                                   incertdte=incertdte)
        self.assertEqual(prv['incertdte'].item(), expected)

    def test_error(self):
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        res = 157.6
        incertdte = 8
        simulation.PrevisionDeterministe(dte=dte, res=res, incertdte=incertdte)
        with self.assertRaises(ValueError):
            simulation.PrevisionDeterministe(
                dte='2012-13-01', res=res, incertdte=incertdte)
        with self.assertRaises(ValueError):
            simulation.PrevisionDeterministe(
                dte=dte, res='foo', incertdte=incertdte)
        with self.assertRaises(ValueError):
            simulation.PrevisionDeterministe(
                dte=dte, res=res, incertdte='foo')


class TestPrevisionsDeterministes(unittest.TestCase):
    """PrevisionDeterministe class tests."""

    def test_base_01(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        res = 8956321.2569
        incertdte = 15
        prv = simulation.PrevisionDeterministe(dte=dte, res=res,
                                               incertdte=incertdte)
        prvs = simulation.PrevisionsDeterministes(prv)
        self.assertEqual(len(prvs), 1)
        self.assertEqual(prvs['res'].tolist(), [res])
        self.assertEqual(prvs['incertdte'].tolist(), [incertdte])
        self.assertEqual(prvs.index.tolist(), [dte])

    def test_base_02(self):
        """Test previsions with one prevision."""
        dtes = [
            datetime.datetime(1953, 5, 18, 18, 5, 33),
            datetime.datetime(1970, 5, 18, 18, 5, 33),
            datetime.datetime(1998, 4, 18, 18, 5, 33)
        ]
        res = [8956321.2569, 158, -8.7]
        incertdte = [0, 15, -5]

        prv = []
        for i in range(0, 3):
            prv.append(simulation.PrevisionDeterministe(
                dte=dtes[i], res=res[i], incertdte=incertdte[i]))

        prvs = simulation.PrevisionsDeterministes(*prv)
        self.assertEqual(len(prvs), 3)
        self.assertEqual(prvs['res'].tolist(), res)
        self.assertEqual(prvs['incertdte'].tolist(), incertdte)
        self.assertEqual(prvs.index.tolist(), dtes)


# -- class TestPrevisionsPrb --------------------------------------------------
class TestPrevisionsPrb(unittest.TestCase):

    """PrevisionsPrb class tests."""

    def test_base_01(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        res = 8956321.2569
        prb = 75
        prvs = simulation.PrevisionsPrb(
            simulation.PrevisionPrb(dte=dte, res=res, prb=prb))
        self.assertEqual(prvs.tolist(), [res])
        self.assertEqual(prvs.index.tolist(), [(dte, prb), ])

    def test_base_02(self):
        """Base previsions."""
        d = [datetime.datetime(2012, 5, 18, 18, 0),
             datetime.datetime(2012, 5, 18, 18, 5),
             datetime.datetime(2012, 5, 18, 18, 10)]
        r = [33.5, 35, 40]
        p = simulation.PrevisionsPrb(
            simulation.PrevisionPrb(d[0], r[0]),
            simulation.PrevisionPrb(d[1], r[1]),
            simulation.PrevisionPrb(d[2], r[2]))
        self.assertEqual(p.tolist(), r)
        self.assertEqual([t[0] for t in p.index.tolist()], d)
        self.assertEqual(
            [t[1] for t in p.index.tolist()], [50, 50, 50])

    def test_error_01(self):
        """Prevision error."""
        prv = simulation.PrevisionPrb(
            **{'dte': '2012-10-10 10', 'res': 25.8, 'prb': 3})
        simulation.PrevisionsPrb(prv)
        simulation.PrevisionsPrb(*[prv, prv, prv])
        self.assertRaises(TypeError, simulation.PrevisionsPrb, 44)


class TestPrevEnsemble(unittest.TestCase):
    """PrevisionDeterministe class tests."""

    def test_base_01(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        lb = 'libellé'
        poids = 2
        res = 8956321.2569
        prv = simulation.PrevEnsemble(dte=dte, lb=lb, res=res, poids=poids)
        self.assertEqual(
            (prv['dte'].item(), prv['lb'].item(), prv['res'].item(),
             prv['poids'].item()),
            (dte, lb, res, poids)
        )
        strprv = prv.__str__()
        self.assertEqual(strprv,
                         ('8956321.2569 du membre libellé de poids 2'
                          ' pour le 1953-05-18 à 18:05:33 UTC'))

    def test_res(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        lb = 'libellé'
        poids = 2
        data = [
            (0, 0.0),
            ('153', 153.0),
            (-8.4, -8.4)
        ]
        for res, expected in data:
            prv = simulation.PrevEnsemble(dte=dte, lb=lb, res=res, poids=poids)
        self.assertEqual(prv['res'].item(), expected)

    def test_poids(self):
        """Test attribute poids."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        lb = 'libellé'
        res = 154.2
        data = [
            (1, 0),
            ('153', 153),
            (5, 5)
        ]
        for poids, expected in data:
            prv = simulation.PrevEnsemble(dte=dte, lb=lb, res=res,
                                          poids=poids)
        self.assertEqual(prv['poids'].item(), expected)

        for poids in [-5, 0]:
            with self.assertRaises(ValueError) as context:
                simulation.PrevEnsemble(dte=dte, lb=lb, res=res,
                                        poids=poids)
            self.assertEqual(
                str(context.exception),
                'poids must be greater than or equal to 1'
            )

    def test_error(self):
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        lb = 'libellé'
        res = 154.2
        prv = simulation.PrevEnsemble(
            dte=dte, lb=lb, res=res)
        self.assertEqual(prv['poids'].item(), 1)
        with self.assertRaises(ValueError):
            simulation.PrevEnsemble(
                dte='2012-13-01', lb=lb, res=res)
        with self.assertRaises(ValueError):
            simulation.PrevEnsemble(
                dte=dte, lb=lb, res='foo')
        with self.assertRaises(ValueError):
            simulation.PrevEnsemble(
                dte=dte, lb='lb', res=res,
                poids='foo')


class TestPrevsEnsemble(unittest.TestCase):
    """PrevisionDeterministe class tests."""

    def test_base_01(self):
        """Test previsions with one prevision."""
        dte = datetime.datetime(1953, 5, 18, 18, 5, 33)
        lb = 'libellé'
        res = 154.2
        prv = simulation.PrevEnsemble(
            dte=dte, lb=lb, res=res)
        prvs = simulation.PrevsEnsemble(prv)
        self.assertEqual(len(prvs), 1)

        # self.assertEqual(prvs['lb'].tolist(), [lb])
        self.assertEqual(prvs['res'].tolist(), [res])
        self.assertEqual(prvs['poids'].tolist(), [1])
        self.assertEqual(prvs.index.tolist(), [(dte, lb)])

    def test_base_02(self):
        """Test previsions with one prevision."""
        dtes = [
            datetime.datetime(1953, 5, 18, 18, 5, 33),
            datetime.datetime(1970, 5, 18, 18, 5, 33),
            datetime.datetime(1998, 4, 18, 18, 5, 33)
        ]
        lb = ['libellé 1', 'libellé 2', 'libellé 3']
        res = [8956321.2569, 158, -8.7]
        poids = [1, 5, 9]

        prv = []
        for i in range(0, 3):
            prv.append(simulation.PrevEnsemble(
                dte=dtes[i], lb=lb[i], res=res[i], poids=poids[i]))

        prvs = simulation.PrevsEnsemble(*prv)
        self.assertEqual(len(prvs), 3)
        self.assertEqual(prvs['res'].tolist(), res)
        self.assertEqual(prvs['poids'].tolist(), poids)
        self.assertEqual(prvs.index.tolist(), list(zip(dtes, lb)))

    def test_error(self):
        with self.assertRaises(TypeError) as context:
            simulation.PrevsEnsemble(56.5)
        self.assertEqual(
            str(context.exception),
            '56.5 is not an ensemble Prevision'
        )


# -- class TestPrevEvol -------------------------------------------------------
class TestPrevEvol(unittest.TestCase):
    """PrevEvol class tests."""

    def test_01(self):
        evol = 5
        dtdeb = datetime.datetime(2015, 1, 7, 14, 15, 30)
        incertdte = 5
        prvevol = simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)
        self.assertEqual(
            (prvevol.evol, prvevol.dtdeb, prvevol.incertdte),
            (evol, dtdeb, incertdte))

    def test_error_incertdte(self):
        evol = 5
        dtdeb = datetime.datetime(2015, 1, 7, 14, 15, 30)
        incertdte = 5
        simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)
        incertdte = 'foo'
        with self.assertRaises(TypeError) as context:
            simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)
        self.assertEqual(str(context.exception), 'incertdte must be an integer')

    def test_error_evol(self):
        evol = 5
        dtdeb = datetime.datetime(2015, 1, 7, 14, 15, 30)
        incertdte = 5
        simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)
        for evol in [-5, 10, 'foo', None]:
            with self.assertRaises(ValueError):
                simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)

    def test_str(self):
        evol = 5
        dtdeb = datetime.datetime(2015, 1, 7, 14, 15, 30)
        incertdte = 5
        prvevol = simulation.PrevEvol(evol=evol, dtdeb=dtdeb, incertdte=incertdte)
        self.assertEqual(str(prvevol),
                         'stabilité à partir de 2015-01-07 14:15:30'
                         ' (incertitude de 5 minute(s))')


# -- class TestScenarioSimul --------------------------------------------------
class TestScenarioSimul(unittest.TestCase):
    """ScenarioSimul class tests."""

    def test_01(self):
        libelle = 'libellé'
        descriptif = 'Descriptif'
        scenario = simulation.ScenarioSimul(
            libelle=libelle, descriptif=descriptif)
        self.assertEqual(
            (scenario.libelle, scenario.descriptif),
            (libelle, descriptif)
        )
        strscenario = str(scenario)
        self.assertTrue(strscenario.find(libelle) > 0)
        self.assertTrue(strscenario.find(descriptif) > 0)

    def test_02(self):
        libelle = 'libellé'
        scenario = simulation.ScenarioSimul(
            libelle=libelle)
        self.assertEqual(
            (scenario.libelle, scenario.descriptif),
            (libelle, None)
        )
        strscenario = str(scenario)
        self.assertTrue(strscenario.find(libelle) > 0)
        self.assertTrue(strscenario.find('<sans descriptif>') > 0)


# -- class TestSimulation -----------------------------------------------------
class TestSimulation(unittest.TestCase):

    """Simulation class tests."""

    def test_base_01(self):
        """Empty simulation."""
        sim = simulation.Simulation(strict=False)
        self.assertEqual(
            (sim.entite, sim.dtprod, sim.grandeur, sim.code, sim.qualite,
             sim.dtfinvalidite, sim.dtdeb, sim.dtfin, sim.dtbase, sim.dtderobs,
             sim.modecalcul, sim.statut, sim.sysalti, sim.contexte,
             sim.commentaire, sim.cmntprive, sim.mode, sim.modeleprevision,
             sim.contact, sim.intervenant, sim.publication,
             sim.previsions_tend, sim.previsions_det, sim.previsions_prb,
             sim.prevs_ensemble, sim.prevs_evol, sim.scenario),
            (None, None, None, None, None,
             None, None, None, None, None,
             0, 4, None, None,
             None, None, 0, None,
             None, None, 22,
             None, None, None, None, [], None)
        )

    def test_base_02(self):
        """Full simulation."""
        entite = sitehydro.Sitehydro(code=44, strict=False)
        dtprod = datetime.datetime(2012, 5, 18, 18, 36)
        grandeur = 'Q'
        code = 178
        qualite = 53
        dtfinvalidite = datetime.datetime(2012, 6, 1, 10, 20, 30)
        dtdeb = datetime.datetime(2012, 5, 16, 10, 15, 10)
        dtfin = datetime.datetime(2012, 5, 18, 18, 30, 20)
        dtbase = datetime.datetime(2012, 5, 16, 9, 50, 30)
        dtderobs = datetime.datetime(2012, 5, 18, 18, 0, 0)
        modecalcul = 3
        statut = 16
        sysalti = 31
        contexte = 'Le contexte'
        commentaire = 'Commenatire'
        cmntprive = 'Commentaire privé'
        mode = 1
        modele = modeleprevision.Modeleprevision(code=3)
        contact = _intervenant.Contact(code=444)
        intervenant = _intervenant.Intervenant(code=1537)
        publication = 20
        previsions_tend = simulation.PrevisionsTendance(
            simulation.PrevisionTendance(
                **{'dte': '2012-10-10 10', 'res': 5.7, 'tend': 'min'}),
            simulation.PrevisionTendance(
                **{'dte': '2012-10-10 10', 'res': 25.8, 'tend': 'moy'}),
            simulation.PrevisionTendance(
                **{'dte': '2012-10-10 10', 'res': 53.4,  'tend': 'max'}),
        )
        previsions_det = simulation.PrevisionsDeterministes(
            simulation.PrevisionDeterministe(
                **{'dte': '2012-11-10 10', 'res': 37.8, 'incertdte': 3}))
        previsions_prb = simulation.PrevisionsPrb(
            simulation.PrevisionPrb(
                **{'dte': '2012-12-10 10', 'res': 25.8, 'prb': 5}))

        prevs_ensemble = simulation.PrevsEnsemble(
            simulation.PrevEnsemble(
                **{'dte': '2013-01-02 10:00:00', 'lb': 'libellée',
                   'res': 25.8, 'poids': 5}))

        prevs_evol = [
            simulation.PrevEvol(
                evol=1,
                dtdeb=datetime.datetime(2013, 1, 2, 10),
                incertdte=5
            )
        ]

        scenario = simulation.ScenarioSimul(
            libelle='Libellé de simulation',
            descriptif='Descriptif de la simulation')

        sim = simulation.Simulation(
            entite=entite,
            dtprod=dtprod,
            grandeur=grandeur,
            code=code,
            qualite=qualite,
            dtfinvalidite=dtfinvalidite,
            dtdeb=dtdeb,
            dtfin=dtfin,
            dtbase=dtbase,
            dtderobs=dtderobs,
            modecalcul=modecalcul,
            statut=statut,
            sysalti=sysalti,
            contexte=contexte,
            commentaire=commentaire,
            cmntprive=cmntprive,
            mode=mode,
            modeleprevision=modele,
            contact=contact,
            intervenant=intervenant,
            publication=publication,
            previsions_tend=previsions_tend,
            previsions_det=previsions_det,
            previsions_prb=previsions_prb,
            prevs_ensemble=prevs_ensemble,
            prevs_evol=prevs_evol,
            scenario=scenario
        )

        self.assertEqual(
            (sim.entite, sim.dtprod, sim.grandeur, sim.code, sim.qualite,
             sim.dtfinvalidite, sim.dtdeb, sim.dtfin, sim.dtbase, sim.dtderobs,
             sim.modecalcul, sim.statut, sim.sysalti, sim.contexte,
             sim.commentaire, sim.cmntprive, sim.mode, sim.modeleprevision,
             sim.contact, sim.intervenant, sim.publication,
             sim.previsions_tend, sim.previsions_det, sim.previsions_prb,
             sim.prevs_ensemble, sim.scenario),
            (entite, dtprod, grandeur, code, qualite,
             dtfinvalidite, dtdeb, dtfin, dtbase, dtderobs,
             modecalcul, statut, sysalti, contexte,
             commentaire, cmntprive, mode, modele,
             contact, intervenant, publication,
             previsions_tend, previsions_det, previsions_prb, prevs_ensemble,
             scenario)
        )

        self.assertEqual(sim.previsions_tend['res'].tolist(),
                         previsions_tend['res'].tolist())
        self.assertEqual(
            sim.previsions_det['res'].tolist(),
            previsions_det['res'].tolist())
        self.assertEqual(
            sim.prevs_ensemble['res'].tolist(),
            prevs_ensemble['res'].tolist())
        self.assertEqual(sim.previsions_prb.all(), previsions_prb.all())

        prvevol = sim.prevs_evol[0]
        self.assertEqual(
            (prvevol.evol, prvevol.dtdeb, prvevol.incertdte),
            (prevs_evol[0].evol, prevs_evol[0].dtdeb, prevs_evol[0].incertdte)
        )

        strsim = sim.__str__()
        self.assertTrue(strsim.find('Prévisions probabilistes:') > 0)
        self.assertTrue(strsim.find('Prévisions déterministes:') > 0)
        self.assertTrue(strsim.find('Prévisions de tendance:') > 0)
        self.assertTrue(strsim.find('Prévisions d\'ensemble:') > 0)
        self.assertTrue(strsim.find('Prévisions d\'évolution:') > 0)
        self.assertTrue(strsim.find('Scénario') > 0)

    def test_base_03(self):
        """Dtprod can be a string."""
        dte = datetime.datetime(2012, 5, 18, 18, 36)
        sim = simulation.Simulation(dtprod=dte)
        self.assertEqual(sim.dtprod, dte)
        sim = simulation.Simulation(dtprod='2012-05-18 18:36')
        self.assertEqual(sim.dtprod, dte)

    def test_str_01(self):
        """Test __str__ method with minimum values."""
        # None values
        sim = simulation.Simulation()
        self.assertTrue(sim.__str__().rfind('Simulation') > -1)
        self.assertTrue(sim.__str__().rfind('Date de production') > -1)
        self.assertTrue(sim.__str__().rfind('Commentaire') > -1)
        self.assertTrue(sim.__str__().rfind('Previsions') > -1)
        # a junk entite
        sim = simulation.Simulation(entite='station 33', strict=False)
        self.assertTrue(sim.__str__().rfind('entite inconnue') > -1)
        # a junk statut
        sim = simulation.Simulation(statut=999, strict=False)
        self.assertTrue(sim.__str__().rfind('sans statut') > -1)
        self.assertTrue(sim.__str__().rfind('sans prévisions de tendance') > 0)
        self.assertTrue(sim.__str__().rfind('sans prévisions probabilistes') > 0)
        self.assertTrue(sim.__str__().rfind('sans prévisions déterministes') > 0)
        self.assertTrue(sim.__str__().rfind('sans prévisions d\'ensemble') > 0)
        self.assertTrue(sim.__str__().rfind('sans prévisions d\'évolution') > 0)

        self.assertTrue(sim.__str__().rfind('sans scénario') > 0)

    def test_str_02(self):
        """Test __str__ method with basic values."""
        previsions_prb = simulation.PrevisionsPrb(
            simulation.PrevisionPrb(
                **{'dte': '2012-10-10 10', 'res': 25.8, 'prb': 3}))
        sim = simulation.Simulation(
            dtprod='2012-05-18T18:36',
            entite=sitehydro.Station(
                code=0, libelle='Toulouse', strict=False),
            previsions_prb=previsions_prb)
        self.assertTrue(sim.__str__().rfind('Simulation') > -1)
        self.assertTrue(sim.__str__().rfind('Date de production') > -1)
        self.assertTrue(sim.__str__().rfind('Commentaire') > -1)
        self.assertTrue(sim.__str__().rfind('Previsions') > -1)
        self.assertTrue(sim.__str__().rfind('Prévisions probabilistes:') > 0)

    def test_str_03(self):
        """Test __str__ method with big Previsions."""
        previsions_prb = simulation.PrevisionsPrb(
            *[simulation.PrevisionPrb(dte='20%i-10-10 10' % x, res=x)
              for x in range(10, 50)])
        sim = simulation.Simulation(
            dtprod='2012-05-18T18:36',
            entite=sitehydro.Station(code=0, strict=False),
            previsions_prb=previsions_prb)
        self.assertTrue(sim.__str__().rfind('Simulation') > -1)
        self.assertTrue(sim.__str__().rfind('Date de production') > -1)
        self.assertTrue(sim.__str__().rfind('Commentaire') > -1)
        self.assertTrue(sim.__str__().rfind('Previsions') > -1)

    def test_fuzzy_mode_01(self):
        """Fuzzy mode test."""
        entite = 'station'
        modele = 'modele'
        grandeur = 'RR'
        statut = 333333
        publication = 10
        commentaire = 'Nong'
        dtprod = datetime.datetime(2012, 5, 18, 18, 36)
        qualite = 100
        previsions_prb = [10, 20, 30]
        sim = simulation.Simulation(
            entite=entite,
            modeleprevision=modele,
            grandeur=grandeur,
            statut=statut,
            qualite=qualite,
            publication=publication,
            commentaire=commentaire,
            dtprod=dtprod,
            previsions_prb=previsions_prb,
            strict=False)
        self.assertEqual(sim.entite, entite)
        self.assertEqual(sim.modeleprevision, modele)
        self.assertEqual(sim.grandeur, grandeur)
        self.assertEqual(sim.statut, statut)
        self.assertEqual(sim.qualite, qualite)
        self.assertEqual(sim.publication, publication)
        self.assertEqual(sim.commentaire, commentaire)
        self.assertEqual(sim.dtprod, dtprod)
        self.assertEqual(sim.previsions_prb, previsions_prb)

    def test_code(self):
        """Test simulation code"""
        data = (
            (15, 15),
            (None, None),
            ('147', 147),
            ('--', '--'),
        )
        for code, expected in data:
            sim = simulation.Simulation(code=code)
            self.assertEqual(sim.code, expected)

        with self.assertRaises(TypeError) as context:
            sim = simulation.Simulation(code='foo')
        self.assertEqual(
            str(context.exception), 'simulation code must be an integer')

        # fuzzy mode
        sim = simulation.Simulation(code='foo', strict=False)
        self.assertEqual(sim.code, 'foo')

    def test_error_01(self):
        """Entite error."""
        # init
        site = sitehydro.Sitehydro(code='A0440101')
        station = sitehydro.Station(code='A044010101')
        simul = simulation.Simulation(
            **{'entite': station, 'grandeur': 'H'})
        self.assertEqual(simul.grandeur, 'H')
        # wrong entite
        self.assertRaises(
            TypeError,
            simulation.Simulation,
            # **{'entite': entite}
            **{'entite': 25})
        # site with a H simulation
        self.assertRaises(
            TypeError,
            simulation.Simulation,
            **{'entite': site, 'grandeur': 'H'})
        # station with a Q simulation
        self.assertRaises(
            TypeError,
            simulation.Simulation,
            **{'entite': station, 'grandeur': 'Q'})

    def test_error_02(self):
        """Modeleprevision error."""
        modele = modeleprevision.Modeleprevision()
        simulation.Simulation(**{'modeleprevision': modele})
        self.assertRaises(
            TypeError,
            simulation.Simulation,
            # **{'modeleprevision': modele}
            **{'modeleprevision': 'gloup !'})

    def test_error_03(self):
        """Grandeur error."""
        simulation.Simulation(**{'grandeur': 'Q'})
        simulation.Simulation(**{'grandeur': None})
        self.assertRaises(
            ValueError,
            simulation.Simulation,
            # **{'grandeur': 'H'}
            **{'grandeur': 'RR'})

    def test_error_04(self):
        """Statut error."""
        simulation.Simulation(**{'statut': 16})
        self.assertRaises(
            ValueError,
            simulation.Simulation,
            # **{'statut': 16}
            **{'statut': None})
        self.assertRaises(
            ValueError,
            simulation.Simulation,
            # **{'statut': 16}
            **{'statut': 111111})

    def test_error_05(self):
        """Qualite error."""
        simulation.Simulation(**{'qualite': 45})
        self.assertRaises(
            ValueError,
            simulation.Simulation,
            # **{'qualite': 45}
            **{'qualite': -1})

    def test_error_06(self):
        """Dtprod error."""
        dtprod = datetime.datetime(2020, 1, 1, 10, 0)
        simulation.Simulation(**{'dtprod': dtprod})
        dtprod = '2020-01-01 10:00'
        simulation.Simulation(**{'dtprod': dtprod})
        self.assertRaises(
            (TypeError, ValueError),
            simulation.Simulation,
            # **{'dtprod': dtprod}
            **{'dtprod': '2020-10 10:00'})

    def test_error_07(self):
        """Test previsions error."""
        prvs = simulation.PrevisionsPrb(
            simulation.PrevisionPrb(
                **{'dte': '2012-10-10 10', 'res': 25.8, 'prb': 3}))
        simulation.Simulation(**{'previsions_prb': prvs})
        self.assertRaises(
            TypeError,
            simulation.Simulation,
            # **{'previsions': prvs}
            **{'previsions_prb': [110, 20, 30]})

    def test_error_scenario(self):
        """Test scenario error."""
        scenario = simulation.ScenarioSimul(
            libelle='lb', descriptif='descriptif')
        simulation.Simulation(scenario=scenario)
        for scenario in ['lb', 5]:
            with self.assertRaises(TypeError) as context:
                simulation.Simulation(scenario=scenario)
            self.assertEqual(
                str(context.exception),
                'scenario must be an instance of ScenarioSimul')
