"""test module conversion hydrometrie json files"""

import os
import datetime
import unittest

from test_conv_xml_from_xml_v2 import (
    CheckConversionValidsAnnee, CheckConversionSimulations,
    CheckConversionGradients, CheckConversionJaugeages, CheckConversionEvenements,
    CheckConversionSeuilsMeteo, CheckConversionSeuilsHydro, CheckConversionSitesHydro,
    CheckConversionSitesMeteo, CheckConversionModelesPrevision, CheckConversionSeriesMeteo,
    CheckConversionSeriesObsElab, CheckConversionCourbesTarage, CheckConversionCourbesCorrection,
    CheckConversionSeriesHydro, CheckConversionSeriesObsMeteoElab, CheckConversionIntervenants
)

from libhydro.conv.xml import Message


class TestFroJsonIntervenants(CheckConversionIntervenants):
    """FromJsonIntervenants class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'intervenants.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.msg = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.msg.scenario, [])
        self.assertNotEqual(self.msg.intervenants, [])
        self.assertEqual(self.msg.siteshydro, [])
        self.assertEqual(self.msg.seuilshydro, [])
        self.assertEqual(self.msg.evenements, [])
        self.assertEqual(self.msg.serieshydro, [])
        self.assertEqual(self.msg.simulations, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.msg.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(
            scenario.dtprod, datetime.datetime(2001, 12, 17, 4, 30, 47))
        self.assertEqual(
            (scenario.reference, scenario.envoi),
            ('modele.xml', 'envoi.xml'))
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.emetteur.contact.code, '525')
        self.assertEqual(
            scenario.destinataire.intervenant.code, '12345671234567')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SIRET')
        self.assertEqual(scenario.destinataire.contact.code, '2')

    def test_intervenant_0(self):
        """Conversion first intervenant test."""
        i = self.msg.intervenants[0]
        self.check_intervenant_0(i)

    def test_intervenant_1(self):
        """Conversion second intervenant test."""
        i = self.msg.intervenants[1]
        self.check_intervenant_1(i)


# -- class TestFromJSonSeriesHydro --------------------------------------------
class TestFromJsonSeriesHydro(CheckConversionSeriesHydro):
    """FromJsonSeriesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'serieshydro.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertNotEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

    def test_full_serie(self):
        """Test serie with all tags"""
        serie = self.data.serieshydro[0]
        self.check_seriehydro_0(serie)

    def test_minimal_serie(self):
        """Serie and obs with only mandatory tags"""
        serie = self.data.serieshydro[1]
        self.check_seriehydro_1(serie)

    def test_serie_without_obs(self):
        """Serie without obs"""
        serie = self.data.serieshydro[2]
        self.check_seriehydro_2(serie)

    def test_serie_v(self):
        """Serie V"""
        serie = self.data.serieshydro[3]
        self.check_seriehydro_3(serie)


# -- class TestFromJsonSeriesMeteo ---------------------------------------------
class TestFromJsonSeriesMeteo(CheckConversionSeriesMeteo):

    """FromJsonSeriesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'seriesmeteo.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertNotEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        # self.assertEqual(len(self.data.sitesmeteo), 1)
        self.assertEqual(len(self.data.seriesmeteo), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_rr(self):
        """Serie RR test."""
        self.check_seriemeteo_rr(self.data.seriesmeteo)

    def test_serie_ta(self):
        """Serie TA test."""
        self.check_seriemeteo_ta(self.data.seriesmeteo)

    def test_without_obs(self):
        """Serie without obs test."""
        serie = self.data.seriesmeteo[2]
        self.assertIsNone(serie.observations)

    @unittest.skip("todo: obs without res")
    def test_without_res(self):
        """Serie with one obs but without res ."""
        serie = self.data.seriesmeteo[3]
        self.assertIsNotNone(serie.observations)


class TestFromJsonSitesHydros(CheckConversionSitesHydro):
    """FromJsonSitesHydro class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'siteshydro.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.msg = Message.from_json(json)

    def test_sitehydro_0(self):
        """Sitehydro 0 test."""
        site = self.msg.siteshydro[0]
        self.check_sitehydro_0(site)

    def test_sitehydro_1(self):
        """Sitehydro 1 test."""
        # check site
        site = self.msg.siteshydro[1]
        self.check_sitehydro_1(site)

    def test_sitehydro_2(self):
        """Sitehydro 1 test."""
        # check site
        site = self.msg.siteshydro[2]
        self.check_sitehydro_2(site)

    def test_sitehydro_3(self):
        """Sitehydro 3 test."""
        # check site
        site = self.msg.siteshydro[3]
        self.check_sitehydro_3(site)


class TestFromJsonSitesMeteo(CheckConversionSitesMeteo):

    """FromJsonSitesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'sitesmeteo.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.msg = Message.from_json(json)

    def test_sitemeteo_0(self):
        """Sitemeteo 0 test."""
        site = self.msg.sitesmeteo[0]
        self.check_sitemeteo_0(site)


# -- class TestFromJsonSeuilsHydro --------------------------------------------
class TestFromJSonSeuilsHydros(CheckConversionSeuilsHydro):

    """FromJsonSeuilsHydro class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'seuilshydro.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertNotEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(len(self.data.siteshydro), 0)
        self.assertEqual(len(self.data.seuilshydro), 6)

    def test_seuils_sitehydro_0(self):
        """Test seuils sitehydro 0."""
        # check the sitehydro
        seuil = self.data.seuilshydro[0]
        self.check_seuilhydro_0(seuil)

    def test_seuils_sitehydro_1(self):
        """Test seuils sitehydro 1."""
        # check the sitehydro
        seuil = self.data.seuilshydro[1]
        self.check_seuilhydro_1(seuil)

    def test_seuils_sitehydro_2(self):
        """Test seuils sitehydro 2."""
        seuils = [self.data.seuilshydro[2], self.data.seuilshydro[3]]
        # check the seuils
        for seuil in seuils:
            self.assertEqual(seuil.sitehydro.code, 'O0144020')
            self.assertEqual(len(seuil.valeurs), 0)

    def test_seuils_sitehydro_3(self):
        """Test seuils sitehydro 3."""
        seuil = self.data.seuilshydro[4]
        self.assertEqual(seuil.code, 999)
        self.assertIsNone(seuil.sitehydro)

    def test_seuils_sitehydro_4(self):
        """Seuil with values associated with sitehydro, station and capteurs"""
        seuil = self.data.seuilshydro[5]
        self.check_seuilhydro_5(seuil)


# -- class TestFromJSonSeuilsMeteo --------------------------------------------
class TestFromJsonSeuilsMeteos(CheckConversionSeuilsMeteo):

    """FromJsonSeuilsMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'seuilsmeteo.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(len(self.data.siteshydro), 0)
        self.assertEqual(len(self.data.seuilshydro), 0)
        self.assertEqual(len(self.data.seuilsmeteo), 2)

    def test_seuils_sitehydro_0(self):
        """Test seuils sitehydro 0."""
        # check the sitehydro
        seuil = self.data.seuilsmeteo[0]
        self.check_seuilmeteo_0(seuil)

    def test_seuils_sitehydro_1(self):
        """Test seuils sitehydro 1."""
        seuil = self.data.seuilsmeteo[1]
        self.check_seuilmeteo_1(seuil)


# -- class TestFromJsonModelesPrevision ----------------------------------------
class TestFromJsonModelesPrevision(CheckConversionModelesPrevision):
    """FromJsonModelesPrevision class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'modelesprevision.json'),
                  encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertNotEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        # len
        self.assertEqual(len(self.data.modelesprevision), 2)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2012, 6, 4, 9, 22, 44))
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.contact.code, '7')

    def test_modeleprevision_0(self):
        """Modeleprevision 0 test."""
        modele = self.data.modelesprevision[0]
        self.check_modele_0(modele)

    def test_modeleprevision_1(self):
        """Modeleprevision 1 test."""
        modele = self.data.modelesprevision[1]
        self.check_modele_1(modele)


# -- class TestFromJsonCourbesTarage --------------------------------------
class TestFromJsonCourbesTarage(CheckConversionCourbesTarage):
    """JsonSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'courbestarage.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertNotEqual(self.data.courbestarage, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_full_courbetarage(self):
        """Conversion full courbe tarage test."""
        courbe = self.data.courbestarage[0]
        self.check_courbetarage_0(courbe)

    def test_min_courbetarage(self):
        """Conversion simple courbe tarage test."""
        courbe = self.data.courbestarage[1]
        self.check_courbetarage_1(courbe)

    def test_courbetarage_puissance(self):
        """Conveversion courbe tarage puissance test."""
        courbe = self.data.courbestarage[2]
        self.check_courbetarage_2(courbe)


# -- class TestFromJsonCourbesCorrection --------------------------------------
class TestFromJsonCourbesCorrection(CheckConversionCourbesCorrection):
    """JsonSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'courbescorrection.json'),
                  encoding='utf-8') as fil:
            json = fil.read()
            self.msg = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.msg.scenario, [])
        self.assertEqual(self.msg.intervenants, [])
        self.assertEqual(self.msg.siteshydro, [])
        self.assertEqual(self.msg.sitesmeteo, [])
        self.assertEqual(self.msg.seuilshydro, [])
        self.assertEqual(self.msg.evenements, [])
        self.assertEqual(self.msg.serieshydro, [])
        self.assertEqual(self.msg.seriesmeteo, [])
        self.assertEqual(self.msg.simulations, [])
        self.assertEqual(self.msg.seriesobselab, [])
        self.assertEqual(self.msg.seriesobselabmeteo, [])
        self.assertEqual(self.msg.courbestarage, [])
        self.assertEqual(len(self.msg.courbescorrection), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.msg.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_full_courbecorrection(self):
        """test full courbecorrection"""
        courbe = self.msg.courbescorrection[0]
        self.check_courbecorrection_0(courbe)

    def test_courbecorrection_1point(self):
        """test courbecorrection 1 point"""
        courbe = self.msg.courbescorrection[1]
        self.check_courbecorrection_1(courbe)

    def test_courbecorrection_0point(self):
        """test courbecorrection 0 point"""
        courbe = self.msg.courbescorrection[2]
        self.check_courbecorrection_2(courbe)


# -- class TestFromJsonJaugeages --------------------------------------
class TestFromJsonJaugeages(CheckConversionJaugeages):
    """Json Jaugeages class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'jaugeages.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertEqual(self.data.courbestarage, [])
        self.assertEqual(self.data.courbescorrection, [])
        self.assertNotEqual(self.data.jaugeages, [])

    def test_full_jaugeage(self):
        """test full jaugeage"""
        jaug = self.data.jaugeages[0]
        self.check_jaug_0(jaug)

    def test_min_jaugeage(self):
        """test minimal jaugeage"""
        jaug = self.data.jaugeages[1]
        self.check_jaug_1(jaug)


# -- class TestFromJsonEvenements --------------------------------------
class TestFromJsonEvenements(CheckConversionEvenements):
    """Json Evenements class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'evenements.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertNotEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertEqual(self.data.courbestarage, [])
        self.assertEqual(self.data.courbescorrection, [])
        self.assertEqual(self.data.jaugeages, [])

    def test_full_evenement(self):
        """test full evenement"""
        evt = self.data.evenements[0]
        self.check_evt_0(evt)

    def test_min_evenement(self):
        """test minimal evenement"""
        evt = self.data.evenements[1]
        self.check_evt_1(evt)

    def test_meteo_evenement(self):
        """test evenement on site meteo"""
        evt = self.data.evenements[2]
        self.check_evt_2(evt)


# -- class TestFromJsonSeriesMeteo --------------------------------------------
class TestFromJsonSeriesObsElab(CheckConversionSeriesObsElab):

    """FromJsonSeriesObsElab( class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'obsselab.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertNotEqual(self.data.seriesobselab, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_qmnj(self):
        """Serie QMNJ test."""
        serie = self.data.seriesobselab[0]
        self.check_serie_qmnj(serie)

    def test_serie_qixnj(self):
        """Serie QIXnJ with tag DtResObsElaborHydro test."""
        serie = self.data.seriesobselab[1]
        self.check_serie_qixnj(serie)

    @unittest.skip("todo: obs without res")
    def test_without_obs(self):
        """Serie without obs test."""
        serie = self.data.seriesmeteo[2]
        self.assertIsNone(serie.observations)

    @unittest.skip("todo: obs without res")
    def test_without_res(self):
        """Serie with one obs but without res ."""
        serie = self.data.seriesmeteo[3]
        self.assertIsNotNone(serie.observations)


# -- class TestFromJsonSeriesObsElabMeteo --------------------------------------
class TestFromJsonSeriesObsElabMeteo(CheckConversionSeriesObsMeteoElab):

    """JsonSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'obsselabmeteo.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertNotEqual(self.data.seriesobselabmeteo, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_ipa(self):
        """Conversion serie with IPA test."""
        serie = self.data.seriesobselabmeteo[0]
        self.check_seriesobselabmeteo_ipa(serie)

    def test_serie_lamedeau(self):
        """Conversion serie lamedeau test."""
        serie = self.data.seriesobselabmeteo[1]
        self.check_seriesobselabmeteo_lamedeau(serie)

    def test_minimal_serie(self):
        """Conversion simple serie obs meteo elab test."""
        serie = self.data.seriesobselabmeteo[2]
        self.check_seriesobselabmeteo_2(serie)


# -- class TestFromJsonGradients ----------------------------------------
class TestFromJsonGradients(CheckConversionGradients):

    """FromJsonGradients class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'gradients.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

        self.assertEqual(len(self.data.seriesgradients), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_gradients_0(self):
        """Gradients 0 test."""
        serie = self.data.seriesgradients[0]
        self.check_gradients_0(serie)

    def test_gradients_1(self):
        """Gradients 1 test."""
        serie = self.data.seriesgradients[1]
        self.check_gradients_1(serie)

    def test_gradients_2(self):
        """Gradients 2 test."""
        serie = self.data.seriesgradients[2]
        self.check_gradients_2(serie)


# -- class TestFromJsonValidsAnnee ----------------------------------------
class TestFromJsonValidsAnnee(CheckConversionValidsAnnee):

    """FromJsonValidsAnnee class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'validsannee.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

        self.assertEqual(self.data.seriesgradients, [])
        self.assertEqual(len(self.data.validsannee), 2)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_validsannee(self):
        """ValidsAnnee test."""
        self.check_validsannee(self.data.validsannee)


# -- class TestFromJSonSimulationss ----------------------------------------
class TestFromJSonSimulations(CheckConversionSimulations):

    """FromJsonSimulations class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        with open(os.path.join('data', 'json', '2', 'simulations.json'), encoding='utf-8') as fil:
            json = fil.read()
            self.data = Message.from_json(json)

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(len(self.data.simulations), 4)

        self.assertEqual(self.data.seriesgradients, [])
        self.assertIsNone(self.data.validsannee)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 9, 30))
        self.assertEqual(scenario.emetteur.contact.code, '41')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '14')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_simulation_0(self):
        """Simulation 0 test."""
        simulation = self.data.simulations[0]
        # check simulation
        self.check_sim_0(simulation)

    def test_simulation_1(self):
        """Simulation 1 test."""
        simulation = self.data.simulations[1]
        # check simulation
        self.check_sim_1(simulation)

    def test_simulation_2(self):
        """Simulation 2 test."""
        simulation = self.data.simulations[2]
        # check simulation
        self.check_sim_2(simulation)

    def test_simulation_3(self):
        """Simulation 3 test."""
        simulation = self.data.simulations[3]
        # check simulation
        self.check_sim_3(simulation)
