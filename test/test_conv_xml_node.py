"""module libhydro.conv.xml.node tests."""

import unittest

from lxml import etree

from libhydro.conv.xml.node import JsonNode, XMLNode


class TestJsonNode(unittest.TestCase):
    """JsonNode class tests."""

    def test_attrib(self):
        """_attrib test."""
        dic = {'attr': 'foo', 'bar': 5}
        node = JsonNode(dic)
        value = node.attrib(None, 'attr')
        self.assertEqual(dic['attr'], value)
        value = node.attrib(None, 'foo2')
        self.assertIsNone(value)

    def test_value(self):
        """_value test."""
        dic = {'foo': 1, 'bar': '2'}
        node = JsonNode(dic)
        value = node.value('bar', int)
        self.assertEqual(2, value)

    def test_subobj(self):
        """_subobj test."""
        subdic = {'bar': 2}
        dic = {'foo': subdic}
        node = JsonNode(dic)
        subobj = node.subobj('foo')
        self.assertEqual(subdic, subobj.dic)

    def test_sublist(self):
        """_subobjlist test."""
        dic = {'foo': [{
           'bar': 0
        }, {
            'bar': 1
        }, {
            'bar': 2
        }]}
        node = JsonNode(dic)
        sublist = node.sublist('foo', None)
        values = [obj.value('bar', int) for obj in sublist]
        self.assertEqual([0, 1, 2], values)

        sublist = node.sublist(None, 'foo')
        values = [obj.value('bar', int) for obj in sublist]
        self.assertEqual([0, 1, 2], values)

        sublist = node.sublist('bar', None)
        self.assertEqual([], sublist)

    def test_to_dict(self):
        """Test to_dict member."""
        dic = {'foo': 1, 'bar': '2'}
        node = JsonNode(dic)
        self.assertEqual(dic, node.to_dict())


class TestXMLNode(unittest.TestCase):
    """XMLNode class tests."""

    def test_attrib(self):
        """_attrib test."""
        xml = '<root><bar attr="foo">5</bar><bar2>5</bar2></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        value = node.attrib("bar", "attr")
        self.assertEqual("foo", value)
        # no attribute for ba2
        value = node.attrib("bar2", 'attr')
        self.assertIsNone(value)
        # no tag bar3
        value = node.attrib("bar3", 'attr')
        self.assertIsNone(value)

    def test_value(self):
        """_value test."""
        xml = '<root><bar attr="foo">5</bar><bar2>6</bar2></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        self.assertEqual(5, node.value("bar", int))
        self.assertEqual("6", node.value("bar2"))
        self.assertIsNone(node.value("bar3"))

    def test_subobj(self):
        """_subobj test."""
        xml = '<root><bar attr="foo"><foo>5</foo></bar></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        sub = node.subobj('bar')
        self.assertEqual(5, sub.value("foo", int))

    def test_sublist(self):
        """_subobjlist test with no taglist."""
        xml = '<root><bar><foo>5</foo></bar><bar><foo>6</foo></bar></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        sublist = node.sublist(None, 'bar')
        values = [obj.value('foo', int) for obj in sublist]
        self.assertEqual([5, 6], values)

    def test_sublist_2(self):
        """Test sublist with taglist and tagobj."""
        xml = '<root><bars><bar><foo>5</foo></bar><bar><foo>6</foo></bar></bars></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        sublist = node.sublist('bars', 'bar')
        values = [obj.value('foo', int) for obj in sublist]
        self.assertEqual([5, 6], values)

    def test_to_dict(self):
        """Test to_dict member."""
        xml = '<root><bar>5</bar><foo>6</foo></root>'
        elt = etree.fromstring(xml)
        node = XMLNode(elt)
        dic = node.to_dict()
        self.assertEqual('5', dic['bar'])
        self.assertEqual('6', dic['foo'])
