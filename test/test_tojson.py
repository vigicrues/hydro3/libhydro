"""Test module of Message method to_json"""
# -- imports ------------------------------------------------------------------
import json
import os
import unittest

from libhydro.conv.xml import Message


class TestToJson(unittest.TestCase):
    """Test class of Message method to_json."""

    def compare(self, unit):
        """Compare output with expecetd"""
        version = '2'
        with open(os.path.join('data', 'json', version, '%s.json' % unit)) as fil:
            contents = fil.read()
        msg = Message.from_json(contents)
        jsonhydro = msg.to_json(bdhydro=True, ordered=True, version=version)
        with open(os.path.join('data', 'json', version, '%s_expected.json' % unit)) as fil:
            expected = json.dumps(json.loads(fil.read()))
        self.assertEqual(expected, jsonhydro)

    def test_intervenants(self):
        """Test export intervenants to json."""
        self.compare('intervenants')

    def test_sitesmeteo(self):
        """Test export sites météo to json."""
        self.compare('sitesmeteo')

    def test_siteshydro(self):
        """Test export sites hdyro to json."""
        self.compare('siteshydro')
