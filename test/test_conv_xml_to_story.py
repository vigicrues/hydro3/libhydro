import unittest
import datetime as _datetime

from libhydro.conv.xml import _to_story


# -- class TestFunctions ------------------------------------------------------
class TestFunctions(unittest.TestCase):

    def test_required(self):
        """Required test."""
        obj_str = 'aaa'
        assert _to_story._required(obj_str, ['lower', 'join', 'split'])
        with self.assertRaises(ValueError):
            _to_story._required(obj_str, ['xxx'])

    def test_datetime2iso(self):
        """datetime2iso test."""
        date = _datetime.datetime(1810, 7, 5, 13, 19, 27)
        self.assertEqual(_to_story.datetime2iso(date), '1810-07-05T13:19:27')
        date = _datetime.datetime(54, 1, 2)
        self.assertEqual(_to_story.datetime2iso(date), '0054-01-02T00:00:00')
        date = _datetime.datetime(2018, 11, 23, 18, 24, 53)
        self.assertEqual(_to_story.datetime2iso(date), '2018-11-23T18:24:53')
        date = None
        self.assertIsNone(_to_story.datetime2iso(date))

    def test_date2iso(self):
        """date2iso test."""
        date = _datetime.datetime(1810, 7, 5, 13, 19, 27)
        self.assertEqual(_to_story.date2iso(date), '1810-07-05')
        date = _datetime.datetime(54, 1, 2)
        self.assertEqual(_to_story.date2iso(date), '0054-01-02')
        date = _datetime.date(1578, 3, 4)
        self.assertEqual(_to_story.date2iso(date), '1578-03-04')
        date = None
        self.assertIsNone(_to_story.date2iso(date))
