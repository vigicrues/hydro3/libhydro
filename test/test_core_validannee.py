# -*- coding: utf-8 -*-
"""Test program for validannee.

To run all tests just type:
    python -m unittest test_core_validannee

To run only a class test:
    python -m unittest test_core_validannee.TestClass

To run only a specific test:
    python -m unittest test_core_validannee.TestClass.test_method

"""

# -- imports ------------------------------------------------------------------
import unittest
import datetime as _datetime

from libhydro.core import (validannee as _validannee, sitehydro as _sitehydro)


class TestValidAnnee(unittest.TestCase):
    """ValidAnnee class tests."""

    def test_base_01(self):
        """Simple ValidAnnee"""
        entite = _sitehydro.Sitehydro(code='A1234567')
        annee = 2005
        vannee = _validannee.ValidAnnee(entite=entite, annee=annee)
        self.assertEqual(
            (vannee.entite, vannee.annee, vannee.qualif, vannee.dispoh,
             vannee.dispoq, vannee.cmnt, vannee.dtmaj),
            (entite, annee, 4, None, None, None, None))

    def test_full_validannee(self):
        """Full ValidAnnee"""
        entite = _sitehydro.Station(code='B123456789')
        annee = 2010
        dtmaj = _datetime.datetime(2016, 8, 14, 10, 20, 30)
        qualif = 12
        dispoh = 16
        dispoq = 8
        cmnt = 'Commentairé'
        vannee = _validannee.ValidAnnee(
            entite=entite, annee=annee, qualif=qualif, dispoh=dispoh,
            dispoq=dispoq, cmnt=cmnt, dtmaj=dtmaj)
        self.assertEqual(
            (vannee.entite, vannee.annee, vannee.qualif, vannee.dispoh,
             vannee.dispoq, vannee.cmnt, vannee.dtmaj),
            (entite, annee, qualif, dispoh, dispoq, cmnt, dtmaj))

    def test_entite(self):
        """test property entite."""
        site = _sitehydro.Sitehydro(code='H1234567')
        station = _sitehydro.Station(code='W987654321')
        capteur = _sitehydro.Capteur(code='C12345678901')
        annee = 2019
        for entite in [site, station]:
            vannee = _validannee.ValidAnnee(entite=entite, annee=annee)
            self.assertEqual(vannee.entite, entite)

        with self.assertRaises(ValueError):
            _validannee.ValidAnnee(entite=None, annee=annee)

        for entite in [capteur, 'H1234567']:
            with self.assertRaises(TypeError):
                _validannee.ValidAnnee(entite=entite, annee=annee)

    def test_annee(self):
        """test property annee."""
        entite = _sitehydro.Station(code='W987654321')
        annee = 2010
        vannee = _validannee.ValidAnnee(entite=entite, annee=annee)
        self.assertEqual(vannee.annee, annee)

        with self.assertRaises(ValueError):
            _validannee.ValidAnnee(entite=entite, annee=None)
        for annee in 'toto', entite:
            with self.assertRaises(Exception):
                _validannee.ValidAnnee(entite=entite, annee=annee)

    def test_qualif(self):
        """test property qualif"""
        entite = _sitehydro.Station(code='W987654321')
        annee = 2010
        qualif = 12
        vannee = _validannee.ValidAnnee(
            entite=entite, annee=annee, qualif=qualif)
        self.assertEqual(vannee.qualif, qualif)
        for qualif in [0, 'toto']:
            with self.assertRaises(ValueError):
                _validannee.ValidAnnee(
                    entite=entite, annee=annee, qualif=qualif)

    def test_dispo(self):
        """test properties dispoh and dispoq"""
        entite = _sitehydro.Station(code='W987654321')
        annee = 2010
        dispoh = 8
        dispoq = 10
        vannee = _validannee.ValidAnnee(
            entite=entite, annee=annee, dispoh=dispoh, dispoq=dispoq)
        self.assertEqual((vannee.dispoh, vannee.dispoq), (dispoh, dispoq))

        for dispo in [4, 'toto']:
            with self.assertRaises(ValueError):
                _validannee.ValidAnnee(
                    entite=entite, annee=annee, dispoh=dispo)
            with self.assertRaises(ValueError):
                _validannee.ValidAnnee(
                    entite=entite, annee=annee, dispoq=dispo)

    def test_str_01(self):
        """test represention of ValidAnne with Station"""
        entite = _sitehydro.Station(code='W987654321')
        annee = 2016
        vannee = _validannee.ValidAnnee(
            entite=entite, annee=annee)
        self.assertEqual(
            vannee.__str__(),
            'Qualification de l\'année 2016 de la station W987654321:'
            ' Année provisoire'
        )

    def test_str_02(self):
        """test represention of ValidAnne with Sitehydro"""
        entite = _sitehydro.Sitehydro(code='A1234567')
        annee = 2010
        qualif = 12
        vannee = _validannee.ValidAnnee(
            entite=entite, annee=annee, qualif=qualif)
        self.assertEqual(
            vannee.__str__(),
            'Qualification de l\'année 2010 du site hydro A1234567:'
            ' Année validée douteuse'
        )


class TestValidsAnnee(unittest.TestCase):
    """ValidAnnee class tests."""

    def test_base_01(self):
        """Simple ValidAnnee"""
        site = _sitehydro.Sitehydro(code='A1234567')
        station = _sitehydro.Station(code='A123456789')
        annee = 2005
        vannee1 = _validannee.ValidAnnee(entite=site, annee=annee, qualif=12)
        vannee2 = _validannee.ValidAnnee(entite=site, annee=annee-1)
        vannee3 = _validannee.ValidAnnee(entite=station, annee=annee)
        valids = _validannee.ValidsAnnee(vannee1, vannee2, vannee3)
        self.assertEqual(len(valids), 3)

        vannees_site = valids.loc['A1234567']
        self.assertEqual(len(vannees_site), 2)
        self.assertEqual(vannees_site.index.tolist(), [2004, 2005])
        self.assertEqual(valids.loc[('A1234567', 2005), 'qualif'], 12)
