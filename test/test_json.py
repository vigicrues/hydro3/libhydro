
import json
import os
import unittest
from libhydro.conv.xml import Message
import test_conv_xml_to_xml


class TestIntervenants(unittest.TestCase):

    def test(self):
        version = '2'
        param = 'intervenants'
        with open(os.path.join('data', 'json', version,
                               '%s_expected.json' % param), 'r') as f:
            expected = f.read()
        expected = json.dumps(json.loads(expected))
        # expeted = json(expected)
        with open(os.path.join('data', 'json', version,
                               '%s.json' % param), 'r') as f:
            contents = f.read()
        message = Message.from_json(contents)
        hydrojson = message.to_json(True, True, '2')
        try:
            test_conv_xml_to_xml.assert_unicode_equal(hydrojson, expected)
        except Exception as exc:
            self.fail(str(exc))
