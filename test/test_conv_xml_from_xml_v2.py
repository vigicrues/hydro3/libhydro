# coding: utf-8
"""Test program for xml.from_xml.

To run all tests just type:
    python -m unittest test_conv_xml_from_xml

To run only a class test:
    python -m unittest test_conv_xml_from_xml.TestClass

To run only a specific test:
    python -m unittest test_conv_xml_from_xml.TestClass.test_method

"""
# -- imports ------------------------------------------------------------------
import os
import unittest
import datetime
import math

import pandas as _pandas

from libhydro.core import _composant_site
from libhydro.conv.xml import Message


# -- strings ------------------------------------------------------------------
# contributor Sébastien ROMON
__version__ = '0.5.3'
__date__ = '2017-09-05'

# HISTORY
# V0.5.3 - SR - 2017-07-18
# Add tests on plages utilisation of Station and Capteur
# V0.5.2 - SR - 2017-07-18
# Add tests on new properties of Station
# V0.5.1 - SR - 2017-07-05
# Add test <Jaugeages>
# V0.5 SR - 2017-06-20
#  Add tests <CourbesCorrection>
# V0.4 SR - 2017-06-20
#  Add tests <CourbesTarage>
# V0.3 - 2017-04-20
#   update tests to new Contact.code type
#   some refactoring
# V0.2 - 2014-08-03
#   add the modelesprevision tests
# V0.1 - 2013-08-24
#   first shot


class CheckConversionIntervenants(unittest.TestCase):
    """Check conversion intervenants."""

    def check_intervenant_0(self, i):
        """Check conversion first intervenant."""
        i_adr = i.adresse
        self.assertEqual(i.code, '11')
        self.assertEqual(i.origine, 'SANDRE')
        self.assertEqual(i.nom, 'Nom')
        self.assertEqual(i.statut, 'Gelé')
        self.assertEqual(i.dtcreation, datetime.datetime(1967, 8, 13, 0, 0, 0))
        self.assertEqual(i.dtmaj, datetime.datetime(2001, 12, 17, 4, 30, 47))
        self.assertEqual(i.auteur, 'Auteur')
        self.assertEqual(i.mnemo, 'Mnemo')
        self.assertEqual(i_adr.boitepostale, 'Boite postale')
        self.assertEqual(i_adr.adresse1_cplt, 'complément')
        self.assertEqual(i_adr.adresse1, '1 rue toto')
        self.assertEqual(i_adr.lieudit, 'Lieu-dit')
        self.assertEqual(i_adr.ville, 'Ville')
        self.assertEqual(i_adr.dep, '31')
        self.assertEqual(i.commentaire, 'Commentaire')
        self.assertEqual(i.activite, 'Activités')
        self.assertEqual(i_adr.codepostal, 'Code postal')
        self.assertEqual(i.nominternational, 'International')
        self.assertEqual(i.siret, '12345678901234')
        self.assertEqual(i.commune.code, '32001')
        self.assertEqual(i.commune.libelle, 'Commune')
        self.assertEqual(i_adr.pays, 'FR')
        self.assertEqual(i_adr.adresse2, 'Adresse étrangère')
        self.assertEqual(i.telephone, '0600')
        self.assertEqual(i.fax, '0000')
        self.assertEqual(i.siteweb, 'http://toto.fr')
        self.assertIsNone(i.pere)
        # contacts
        self.assertEqual(len(i.contacts), 3)
        c = i.contacts[0]
        self.assertEqual(c.code, '1')
        self.assertEqual(c.nom, 'Nom')
        self.assertEqual(c.prenom, 'Prenom')
        self.assertEqual(c.civilite, 1)
        self.assertEqual(c.intervenant, i)
        self.assertEqual(c.profilasstr, '001')
        self.assertIsNotNone(c.adresse)
        adr = c.adresse
        self.assertEqual(adr.adresse1, 'Adresse')
        self.assertEqual(adr.adresse2, 'Adresse étrangère')
        self.assertEqual(adr.codepostal, '31000')
        self.assertEqual(adr.ville, 'Toulouse')
        self.assertEqual(adr.pays, 'FR')
        self.assertEqual(c.fonction, 'Hydromètre')
        self.assertEqual(c.telephone, '0000')
        self.assertEqual(c.portable, '0600')
        self.assertEqual(c.fax, 'Fax')
        self.assertEqual(c.mel, 'Mail')
        self.assertEqual(c.dtmaj, datetime.datetime(2015, 2, 3, 12, 10, 38))
        self.assertEqual(len(c.profilsadmin), 2)
        profil0 = c.profilsadmin[0]
        self.assertEqual(profil0.profil, 'GEST')
        self.assertEqual(len(profil0.zoneshydro), 2)
        self.assertEqual(
            (profil0.zoneshydro[0].code, profil0.zoneshydro[0].libelle,
             profil0.zoneshydro[1].code, profil0.zoneshydro[1].libelle),
            ('A123', 'Libellé zone', 'Z987', None))
        self.assertEqual(profil0.dtactivation,
                         datetime.datetime(2004, 4, 15, 17, 18, 19))
        self.assertEqual(profil0.dtdesactivation,
                         datetime.datetime(2005, 8, 10, 13, 36, 43))
        profil1 = c.profilsadmin[1]
        self.assertEqual(profil1.profil, 'JAU')
        self.assertEqual(len(profil1.zoneshydro), 2)
        self.assertEqual(
            (profil1.zoneshydro[0].code, profil1.zoneshydro[0].libelle,
             profil1.zoneshydro[1].code, profil1.zoneshydro[1].libelle),
            ('L000', None, 'K444', 'Libellé2 zone'))
        self.assertIsNone(profil1.dtactivation)
        self.assertIsNone(profil1.dtdesactivation)
        self.assertEqual(c.alias, 'ALIAS')
        self.assertEqual(c.motdepasse, 'mot de passe')
        self.assertEqual(c.dtactivation,
                         datetime.datetime(2001, 12, 17, 9, 30, 47))
        self.assertEqual(c.dtdesactivation,
                         datetime.datetime(2013, 10, 25, 11, 45, 36))
        c = i.contacts[1]
        self.assertEqual(c.code, '2')
        self.assertEqual(c.nom, 'Nom2')
        self.assertEqual(c.prenom, 'Prenom2')
        self.assertEqual(c.civilite, 2)
        self.assertEqual(c.intervenant, i)
        self.assertEqual(c.profilasstr, '010')
        c = i.contacts[2]
        self.assertEqual(c.code, '999')
        self.assertIsNone(c.nom)
        self.assertIsNone(c.prenom)
        self.assertIsNone(c.civilite)
        self.assertEqual(c.intervenant, i)
        self.assertEqual(c.profilasstr, '000')

    def check_intervenant_1(self, i):
        """Check conversion second intervenant."""
        self.assertEqual(i.code, '12345671234567')
        self.assertEqual(i.origine, 'SIRET')
        self.assertEqual(i.nom, 'Nom Sirét')
        self.assertEqual(i.mnemo, 'Captâîn Mnémo')
        # contacts
        self.assertEqual(len(i.contacts), 1)
        c = i.contacts[0]
        self.assertEqual(c.code, '5')
        self.assertEqual(c.nom, 'Nom Contaçt')
        self.assertEqual(c.prenom, 'Prenom Contaçt')
        self.assertEqual(c.civilite, 3)
        self.assertEqual(c.intervenant, i)
        self.assertEqual(c.profilasstr, '100')


class CheckConversionSeriesHydro(unittest.TestCase):
    """Check conversion séries hydro."""

    def check_seriehydro_0(self, serie):
        """Check conversion first serie hydro."""
        dtdeb = datetime.datetime(2014, 11, 4, 10, 56, 41)
        dtfin = datetime.datetime(2014, 12, 2, 8, 15, 27)
        dtprod = datetime.datetime(2015, 9, 17, 9, 11, 47)

        self.assertEqual((serie.entite.code, serie.grandeur, serie.dtdeb,
                          serie.dtfin, serie.dtprod, serie.contact.code,
                          serie.sysalti, serie.perime),
                         ('A1234567', 'Q', dtdeb, dtfin, dtprod, '247',
                          1, True))
        self.assertEqual(serie.pdt.to_int(), 5)
        self.assertEqual(len(serie.observations), 1)
        self.assertEqual(serie.observations.loc['2014-11-04 11:05:00'].tolist(),
                         [1547.1, 14, 20, 1, 8])

    def check_seriehydro_1(self, serie):
        """Check conversion first serie hydro."""
        self.assertEqual((serie.entite.code, serie.grandeur, serie.dtdeb,
                          serie.dtfin, serie.dtprod, serie.contact,
                          serie.sysalti, serie.perime),
                         ('B234567890', 'H', None, None, None, None,
                          31, None))
        self.assertEqual(serie.pdt, None)
        self.assertEqual(len(serie.observations), 1)
        self.assertEqual(serie.observations.loc['2014-11-04 11:05:00'].tolist(),
                         [1547.1, 0, 16, 0, 0])

    def check_seriehydro_2(self, serie):
        """Check conversion second serie hydro."""
        dtdeb = datetime.datetime(2017, 9, 8, 6, 49, 26)
        dtfin = datetime.datetime(2017, 9, 13, 17, 18, 37)
        dtprod = datetime.datetime(2018, 2, 5, 11, 48, 59)
        self.assertEqual((serie.entite.code, serie.grandeur, serie.dtdeb,
                          serie.dtfin, serie.dtprod, serie.contact,
                          serie.sysalti, serie.perime),
                         ('C98765432101', 'H', dtdeb, dtfin, dtprod, None,
                          31, True))
        self.assertIsNone(serie.observations)

    def check_seriehydro_3(self, serie):
        """Check conversion third serie hydro"""
        dtdeb = datetime.datetime(2010, 3, 4, 15, 20, 30)
        dtfin = datetime.datetime(2010, 3, 5, 2, 0, 10)
        dtprod = datetime.datetime(2010, 3, 5, 2, 10, 15)
        self.assertEqual((serie.entite.code, serie.grandeur, serie.dtdeb,
                          serie.dtfin, serie.dtprod, serie.contact,
                          serie.sysalti, serie.perime),
                         ('V123456789', 'V', dtdeb, dtfin, dtprod, None,
                          31, None))
        self.assertEqual(len(serie.observations), 1)
        self.assertEqual(serie.observations.loc['2010-03-04 18:30:00'].tolist(),
                         [0.156, 0.0, 16.0, 0.0, 0.0])


class CheckConversionSeriesMeteo(unittest.TestCase):
    """Check conversion séries météo."""

    def check_seriemeteo_rr(self, seriesmeteo):
        """Check conversion first serie meteo RR."""
        for serie in seriesmeteo:
            if serie.grandeur.typemesure == 'RR':
                break
        self.assertEqual(serie.grandeur.sitemeteo.code, '987654321')
        self.assertEqual(serie.duree, datetime.timedelta(minutes=10))
        self.assertEqual(serie.dtdeb,
                         datetime.datetime(2015, 1, 2, 14, 18, 19))
        self.assertEqual(serie.dtfin,
                         datetime.datetime(2015, 1, 5, 10, 21, 54))
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2015, 2, 6, 23, 54, 25))
        # (dte) res mth qal qua ctxt statut
        self.assertEqual(serie.observations.iloc[0].tolist(),
                         [15.2, 14, 20, 90, 1, 8])
        self.assertEqual(serie.observations.loc['2015-01-02 16:00'].tolist(),
                         [13.9, 10, 16, 100, 0, 4])
        self.assertEqual(serie.observations['statut'].tolist(),
                         [8, 4, 0, 16])

    def check_seriemeteo_ta(self, seriesmeteo):
        """Check conversion fisrt serie meteo TA."""
        for serie in seriesmeteo:
            if serie.grandeur.typemesure == 'TA':
                break
        self.assertEqual(serie.grandeur.sitemeteo.code, '123456789')
        self.assertEqual(serie.duree, datetime.timedelta(minutes=0))
        self.assertEqual(serie.dtdeb, None)
        self.assertEqual(serie.dtfin, None)
        self.assertEqual(serie.dtprod, None)
        # (dte) res mth qal qua
        self.assertEqual(serie.observations.iloc[0].tolist()[:3],
                         [23.1, 0, 16])
        self.assertTrue(math.isnan(serie.observations.iloc[0]['qua'].item()))
        self.assertEqual(serie.observations.iloc[0]['statut'].item(), 0)
        # self.assertEqual(serie.observations.loc['2010-02-26 13:00'].tolist(),
        #                  [8, 0, 16, 75])


class CheckConversionSeriesObsElab(unittest.TestCase):
    """Check conversion séries obs élab."""

    def check_serie_qmnj(self, serie):
        """Check conversion serie hydro elab QmnJ."""
        self.assertEqual(serie.entite.code, 'K1234567')
        self.assertEqual(serie.pdt.to_int(), 3)
        self.assertEqual(serie.dtdeb,
                         datetime.datetime(2011, 12, 9, 17, 11, 23))
        self.assertEqual(serie.dtfin,
                         datetime.datetime(2012, 2, 4, 15, 9, 37))
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2013, 4, 17, 8, 27, 48))
        self.assertEqual(serie.dtdesactivation,
                         datetime.datetime(2015, 8, 11, 13, 4, 12))
        self.assertEqual(serie.dtactivation,
                         datetime.datetime(2010, 11, 3, 14, 51, 40))

        self.assertEqual(serie.sysalti, 1)
        self.assertEqual(serie.glissante, False)

        self.assertEqual(serie.dtdebrefalti,
                         datetime.datetime(2008, 3, 24, 6, 11, 27))
        self.assertEqual(serie.contact.code, '54')

        # dtres res mth qal qua ctxt statut
        self.assertEqual(
            serie.observations.iloc[0].tolist(),
            [datetime.datetime(2013, 4, 17, 8, 27, 48), 873.1, 4, 20, 1, 8])

    def check_serie_qixnj(self, serie):
        """Check conversion serie hydro elab QIXNJ."""
        self.assertEqual(serie.entite.code, 'A000000201')
        self.assertEqual(serie.pdt.to_int(), 4)
        self.assertEqual(serie.dtdeb,
                         datetime.datetime(2005, 1, 2, 3, 4, 5))
        self.assertEqual(serie.dtfin,
                         datetime.datetime(2005, 1, 4, 18, 10, 25))
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2008, 3, 15, 17, 37, 32))
        self.assertEqual(serie.dtdesactivation,
                         datetime.datetime(2017, 11, 4, 17, 54, 47))
        self.assertEqual(serie.dtactivation,
                         datetime.datetime(2008, 3, 18, 11, 25, 13))

        self.assertEqual(serie.sysalti, 31)
        self.assertEqual(serie.glissante, True)

        self.assertEqual(serie.dtdebrefalti,
                         datetime.datetime(2004, 7, 24, 6, 11, 27))
        self.assertEqual(serie.contact.code, '148')

        self.assertEqual(
            serie.observations.index[0],
            datetime.datetime(2005, 1, 3))
        # dtres res mth qal qua ctxt statut
        self.assertEqual(
            serie.observations.iloc[0].tolist(),
            [datetime.datetime(2005, 1, 2, 23, 38, 4), 1015.3, 10, 16, 0, 12])


class CheckConversionSeriesObsMeteoElab(unittest.TestCase):
    """Check conversion séries obs météo élab."""

    def check_seriesobselabmeteo_ipa(self, serie):
        """Check conversion serie meteo elab IPA."""
        self.assertEqual(serie.site.code, '012345678')
        self.assertEqual(serie.site.ponderation, 0.51)
        self.assertEqual(serie.grandeur, 'RR')
        self.assertEqual(serie.typeserie, 2)
        self.assertEqual(serie.dtdeb,
                         datetime.datetime(2017, 4, 18, 15, 16, 14))
        self.assertEqual(serie.dtfin,
                         datetime.datetime(2017, 5, 2, 8, 51, 43))
        self.assertEqual(serie.duree, datetime.timedelta(seconds=3600))

        self.assertEqual(serie.ipa.coefk, 0.21)
        self.assertEqual(serie.ipa.npdt, 5)
        self.assertEqual(len(serie.observations), 2)
        self.assertEqual(serie.observations.iloc[0].tolist(),
                         [10.4, 12.0, 20.0, 88.4, 16.0])

        dte = '2017-04-20 13:21:08'
        try:
            # Pandas 0.23 loc returns Series
            obs = serie.observations.loc[dte].tolist()
        except Exception:
            # previous version Pandas loc return a dataframe
            obs = serie.observations.loc[dte].iloc[0].tolist()
        self.assertEqual(obs[0:3], [11.9, 0.0, 16.0])
        self.assertTrue(math.isnan(obs[3]))
        self.assertEqual(obs[4], 0.0)

    def check_seriesobselabmeteo_lamedeau(self, serie):
        """Check conversion serie obsmeteoelab lamedeau."""
        self.assertEqual(serie.site.code, 'Z7654321')
        self.assertEqual(serie.grandeur, 'RR')
        self.assertEqual(serie.typeserie, 1)
        self.assertEqual(serie.dtdeb,
                         datetime.datetime(2011, 8, 17, 19, 24, 16))
        self.assertEqual(serie.dtfin,
                         datetime.datetime(2012, 2, 8, 15, 29, 31))
        self.assertEqual(serie.duree, datetime.timedelta(seconds=1800))

        self.assertIsNone(serie.ipa)

        self.assertEqual(len(serie.observations), 2)
        self.assertEqual(serie.observations.iloc[0].tolist(),
                         [28.8, 12.0, 0.0, 33.1, 12.0])

        dte = '2011-12-15 08:09:10'
        try:
            # Pandas 0.23 loc returns Series
            obs = serie.observations.loc[dte].tolist()
        except Exception:
            # previous version Pandas loc return a dataframe
            obs = serie.observations.loc[dte].iloc[0].tolist()
        self.assertEqual(obs[0:3], [38.1, 0.0, 16.0])
        self.assertTrue(math.isnan(obs[3]))
        self.assertEqual(obs[4], 0.0)

    def check_seriesobselabmeteo_2(self, serie):
        """Check conversion second serie meteo elab."""
        self.assertEqual(serie.site.code, 'F3214321')
        self.assertEqual(serie.grandeur, 'RR')
        self.assertEqual(serie.typeserie, 1)
        self.assertIsNone(serie.dtdeb)
        self.assertIsNone(serie.dtfin)
        self.assertIsNone(serie.duree)

        self.assertIsNone(serie.ipa)

        self.assertEqual(len(serie.observations), 1)
        dte = '2013-05-04 13:11:26'
        try:
            # Pandas 0.23 loc returns Series
            obs = serie.observations.loc[dte].tolist()
        except Exception:
            # previous version Pandas loc return a dataframe
            obs = serie.observations.loc[dte].iloc[0].tolist()
        self.assertEqual(obs[0:3], [55.8, 0.0, 16.0])
        self.assertTrue(math.isnan(obs[3]))
        self.assertEqual(obs[4], 0.0)


class CheckConversionCourbesTarage(unittest.TestCase):
    """Check conversion courbes de tarage."""

    def check_courbetarage_0(self, courbe):
        """Check conversion first courbe tarage."""
        self.assertEqual(courbe.code, 1514)
        self.assertEqual(courbe.libelle, 'Libellé courbe')
        self.assertEqual(courbe.typect, 0)
        self.assertEqual(courbe.dtcreation, datetime.datetime(2015, 5, 4,
                                                              11, 16, 49))
        self.assertEqual(courbe.limiteinf, 136.5)
        self.assertEqual(courbe.limitesup, 289.4)
        self.assertEqual(courbe.limiteinfpub, 138.4)
        self.assertEqual(courbe.limitesuppub, 256.7)
        self.assertEqual(courbe.dn, 1.4)
        self.assertEqual(courbe.alpha, 1.1)
        self.assertEqual(courbe.beta, 1.3)
        self.assertEqual(courbe.commentaire, 'Commentaire')
        self.assertEqual(courbe.station.code, 'A123456789')
        self.assertEqual(courbe.contact.code, '144')

        self.assertEqual(len(courbe.pivots), 2)
        pivot1 = courbe.pivots[0]
        self.assertEqual(pivot1.hauteur, 58.4)
        self.assertEqual(pivot1.debit, 1547.1)
        pivot2 = courbe.pivots[1]
        self.assertEqual(pivot2.hauteur, 300.4)
        self.assertEqual(pivot2.debit, 3541.3)

        self.assertEqual(len(courbe.periodes), 2)
        periode = courbe.periodes[0]
        self.assertEqual(periode.dtdeb, datetime.datetime(2010, 9, 23,
                                                          8, 15, 41))
        self.assertEqual(periode.dtfin, datetime.datetime(2018, 4, 14,
                                                          17, 54, 49))
        self.assertEqual(periode.etat, 12)
        self.assertEqual(len(periode.histos), 2)
        histo1 = periode.histos[0]
        self.assertEqual(histo1.dtactivation, datetime.datetime(2015, 2, 22,
                                                                11, 12, 4))
        self.assertEqual(histo1.dtdesactivation, datetime.datetime(2016, 10, 2,
                                                                   16, 50, 11))

        histo2 = periode.histos[1]
        self.assertEqual(histo2.dtactivation, datetime.datetime(2017, 1, 17,
                                                                12, 10, 33))
        self.assertEqual(histo2.dtdesactivation, datetime.datetime(2018, 3, 15,
                                                                   14, 13, 8))

        # période2
        periode2 = courbe.periodes[1]
        self.assertEqual(periode2.dtdeb, datetime.datetime(2018, 5, 1,
                                                           7, 19, 51))
        self.assertIsNone(periode2.dtfin)
        self.assertEqual(periode2.etat, 4)
        self.assertEqual(len(periode2.histos), 1)
        histo = periode2.histos[0]
        self.assertEqual(histo.dtactivation, datetime.datetime(2018, 5, 2,
                                                               17, 18, 37))
        self.assertIsNone(histo.dtdesactivation)

        self.assertEqual(courbe.dtmaj, datetime.datetime(2018, 4, 16,
                                                         7, 9, 3))
        self.assertEqual(courbe.commentaireprive, 'Commentaire privé')

    def check_courbetarage_1(self, courbe):
        """Check conversion second courbe tarage."""
        self.assertEqual(courbe.code, 159874)
        self.assertEqual(courbe.libelle, 'Lb')
        self.assertEqual(courbe.typect, 4)
        self.assertIsNone(courbe.dtcreation)
        self.assertIsNone(courbe.limiteinf)
        self.assertIsNone(courbe.limitesup)
        self.assertIsNone(courbe.limiteinfpub)
        self.assertIsNone(courbe.limitesuppub)
        self.assertIsNone(courbe.dn)
        self.assertIsNone(courbe.alpha)
        self.assertIsNone(courbe.beta)
        self.assertIsNone(courbe.commentaire)
        self.assertEqual(courbe.station.code, 'Z987654321')
        self.assertIsNone(courbe.contact)

        self.assertEqual(len(courbe.pivots), 0)

        self.assertEqual(len(courbe.periodes), 0)

        self.assertIsNone(courbe.dtmaj)
        self.assertIsNone(courbe.commentaireprive)

    def check_courbetarage_2(self, courbe):
        """Check conversion third courbe tarage."""
        self.assertEqual(courbe.code, 4321)
        self.assertEqual(courbe.libelle, 'Courbe puissance')
        self.assertEqual(courbe.typect, 4)
        self.assertIsNone(courbe.dtcreation)
        self.assertIsNone(courbe.limiteinf)
        self.assertIsNone(courbe.limitesup)
        self.assertIsNone(courbe.limiteinfpub)
        self.assertIsNone(courbe.limitesuppub)
        self.assertIsNone(courbe.dn)
        self.assertIsNone(courbe.alpha)
        self.assertIsNone(courbe.beta)
        self.assertIsNone(courbe.commentaire)
        self.assertEqual(courbe.station.code, 'C123454321')
        self.assertEqual(courbe.contact.code, '22')

        self.assertEqual(len(courbe.pivots), 2)
        pivot1 = courbe.pivots[0]
        self.assertEqual(pivot1.hauteur, 76.4)
        self.assertEqual(pivot1.vara, 1)
        self.assertEqual(pivot1.varb, 1)
        self.assertEqual(pivot1.varh, 1)
        pivot2 = courbe.pivots[1]
        self.assertEqual(pivot2.hauteur, 541.3)
        self.assertEqual(pivot2.vara, 1.3)
        self.assertEqual(pivot2.varb, 1.1)
        self.assertEqual(pivot2.varh, 512.9)

        self.assertEqual(len(courbe.periodes), 1)
        periode = courbe.periodes[0]
        self.assertEqual(periode.dtdeb, datetime.datetime(2016, 7, 11,
                                                          15, 8, 34))
        self.assertEqual(periode.dtfin, datetime.datetime(2050, 2, 1,
                                                          10, 0, 0))
        self.assertEqual(periode.etat, 8)
        self.assertEqual(len(periode.histos), 1)
        histo1 = periode.histos[0]
        self.assertEqual(histo1.dtactivation, datetime.datetime(2017, 1, 17,
                                                                12, 10, 33))
        self.assertIsNone(histo1.dtdesactivation)

        self.assertEqual(courbe.dtmaj, datetime.datetime(2017, 12, 9,
                                                         11, 51, 50))
        self.assertIsNone(courbe.commentaireprive)


class CheckConversionCourbesCorrection(unittest.TestCase):
    """Check conversion courbes de correction."""

    def check_courbecorrection_0(self, courbe):
        """Check conversion first courbe correction."""
        self.assertEqual(courbe.station.code, 'A123456789')
        self.assertEqual(courbe.libelle, 'Courbe de correction')
        self.assertEqual(courbe.commentaire, 'cmnt')
        self.assertEqual(len(courbe.pivots), 2)
        pivot1 = courbe.pivots[0]
        pivot2 = courbe.pivots[1]
        self.assertEqual(pivot1.dte, datetime.datetime(2015, 10, 2, 9, 10, 54))
        self.assertEqual(pivot1.deltah, -10)
        self.assertEqual(pivot1.dtactivation,
                         datetime.datetime(2016, 5, 7, 11, 51, 32))
        self.assertEqual(pivot1.dtdesactivation,
                         datetime.datetime(2017, 4, 11, 7, 14, 58))

        self.assertEqual(pivot2.dte, datetime.datetime(2016, 1, 3, 17, 14, 23))
        self.assertEqual(pivot2.deltah, 15.4)
        self.assertIsNone(pivot2.dtactivation)
        self.assertIsNone(pivot2.dtdesactivation)
        self.assertEqual(courbe.dtmaj,
                         datetime.datetime(2018, 5, 14, 12, 23, 31))

    def check_courbecorrection_1(self, courbe):
        """Check conversion second courbe correction."""
        self.assertEqual(courbe.station.code, 'K123412340')
        self.assertEqual(courbe.libelle, 'Courbe')
        self.assertEqual(courbe.commentaire, 'com')
        self.assertEqual(len(courbe.pivots), 1)
        pivot1 = courbe.pivots[0]
        self.assertEqual(pivot1.dte, datetime.datetime(2018, 4, 20, 13, 18, 8))
        self.assertEqual(pivot1.deltah, 0)
        self.assertEqual(pivot1.dtactivation,
                         datetime.datetime(2018, 3, 15, 18, 36, 44))
        self.assertIsNone(pivot1.dtdesactivation)
        self.assertIsNone(courbe.dtmaj)

    def check_courbecorrection_2(self, courbe):
        """Check conversion third courbe correction."""
        self.assertEqual(courbe.station.code, 'K123412340')
        self.assertIsNone(courbe.libelle, 'Courbe')
        self.assertIsNone(courbe.commentaire, 'com')
        self.assertEqual(len(courbe.pivots), 0)
        self.assertIsNone(courbe.dtmaj)


class CheckConversionJaugeages(unittest.TestCase):
    """Check conversion jaugeages."""

    def check_jaug_0(self, jaug):
        """Check conversion fisrt jaugeage."""
        # TODO code integer
        self.assertEqual(jaug.code, 1354)
        self.assertEqual(jaug.debit, 2513.2)
        self.assertEqual(jaug.dtdeb,
                         datetime.datetime(2015, 11, 23, 7, 11, 44))
        self.assertEqual(jaug.dtfin,
                         datetime.datetime(2015, 11, 23, 8, 24, 37))
        self.assertEqual(jaug.section_mouillee, 5436.1)
        self.assertEqual(jaug.perimetre_mouille, 1658.3)
        self.assertEqual(jaug.largeur_miroir, 42.3)
        self.assertEqual(jaug.mode, 7)
        self.assertEqual(jaug.commentaire, 'Commentaire')
        self.assertEqual(jaug.vitessemoy, 15.6)
        self.assertEqual(jaug.vitessemax, 16.2)
        self.assertEqual(jaug.vitessemax_surface, 18.4)
        self.assertEqual(jaug.site.code, 'A7654321')
        self.assertEqual(len(jaug.hauteurs), 2)

        self.assertEqual(jaug.dtmaj,
                         datetime.datetime(2016, 3, 14, 13, 26, 57))
        self.assertEqual(jaug.numero, 'N15')
        self.assertEqual(jaug.incertitude_calculee, 15.4)
        self.assertEqual(jaug.incertitude_retenue, 15.2)
        self.assertEqual(jaug.qualification, 2)
        self.assertEqual(jaug.commentaire_prive, 'Commentaire privé')
        self.assertEqual(len(jaug.courbestarage), 2)
        ct1 = jaug.courbestarage[0]
        ct2 = jaug.courbestarage[1]
        self.assertEqual(ct1.code, 157)
        self.assertEqual(ct1.libelle, 'Libellé CT')
        self.assertEqual(ct2.code, 9831)
        self.assertIsNone(ct2.libelle)

    def check_jaug_1(self, jaug):
        """Check conversion second jaugeage."""
        # TODO code integer
        self.assertIsNone(jaug.code)
        self.assertIsNone(jaug.debit)
        self.assertEqual(jaug.dtdeb,
                         datetime.datetime(2016, 3, 14, 18, 54, 13))

        self.assertIsNone(jaug.dtfin)
        self.assertIsNone(jaug.section_mouillee, 5436.1)
        self.assertIsNone(jaug.perimetre_mouille, 1658.3)
        self.assertIsNone(jaug.largeur_miroir, 42.3)
        self.assertEqual(jaug.mode, 0)
        self.assertIsNone(jaug.commentaire)
        self.assertIsNone(jaug.vitessemoy)
        self.assertIsNone(jaug.vitessemax)
        self.assertIsNone(jaug.vitessemax_surface)

        self.assertEqual(jaug.site.code, 'R1212654')

        self.assertEqual(len(jaug.hauteurs), 0)

        self.assertIsNone(jaug.dtmaj)
        self.assertIsNone(jaug.numero)
        self.assertIsNone(jaug.incertitude_calculee)
        self.assertIsNone(jaug.incertitude_retenue)

        self.assertEqual(jaug.qualification, 0)

        self.assertIsNone(jaug.commentaire_prive)
        self.assertEqual(len(jaug.courbestarage), 0)


class CheckConversionSitesHydro(unittest.TestCase):
    """Check conversion sites hdyro."""

    def check_sitehydro_0(self, sh):
        """Check conversion fisrt site hydro."""
        self.assertEqual(sh.code, 'A1984310')
        self.assertEqual(sh.typesite, 'STANDARD')

    def check_sitehydro_1(self, sh):
        """Check conversion second site hydro."""
        self.assertEqual(sh.code, 'O1984310')
        self.assertEqual(
            sh.libelle, 'Le Touch à Toulouse [Saint-Martin-du-Touch]')
        self.assertEqual(sh.libelleusuel, 'St-Martin-du-Touch')
        self.assertEqual(sh.typesite, 'STANDARD')
        self.assertEqual(sh.precisiontype, 5)
        self.assertEqual(sh.code, 'O1984310')
        self.assertEqual(sh.cdbnbv, None)
        self.assertEqual(sh.communes, [
            _composant_site.Commune(code='11354', libelle='1ère commune'),
            _composant_site.Commune(code='11355'),
            _composant_site.Commune(code='2B021')])
        self.assertEqual(len(sh.stations), 3)
        # check stations
        for i in range(1, 3):
            self.assertEqual(sh.stations[i - 1].code, 'O19843100%i' % i)
            self.assertEqual(sh.stations[i - 1].libelle,
                             '%s - station %i' % (sh.libelle, i))
            self.assertEqual(sh.stations[i - 1].typestation, 'STD')
            self.assertEqual(sh.stations[i - 1].libellecomplement,
                             'station %i' % i)

        # check plages d'utilisation
        self.assertEqual(len(sh.stations[0].plages), 2)
        plage = sh.stations[0].plages[0]
        self.assertEqual(plage.dtdeb,
                         datetime.datetime(2015, 2, 14, 11, 54, 6))
        self.assertEqual(plage.dtfin,
                         datetime.datetime(2016, 9, 21, 6, 19, 31))
        self.assertEqual(plage.dtactivation,
                         datetime.datetime(2017, 3, 17, 17, 38, 21))
        self.assertEqual(plage.dtdesactivation,
                         datetime.datetime(2017, 4, 29, 19, 51, 48))
        self.assertEqual(plage.active, False)

        plage = sh.stations[0].plages[1]
        self.assertEqual(plage.dtdeb,
                         datetime.datetime(2020, 11, 3, 15, 2, 3))
        self.assertIsNone(plage.dtfin)
        self.assertIsNone(plage.dtactivation)
        self.assertIsNone(plage.dtdesactivation)
        self.assertIsNone(plage.active)

        self.assertEqual(sh.stations[0].niveauaffichage, 911)
        self.assertEqual(sh.stations[1].niveauaffichage, 0)

    def check_sitehydro_2(self, sh):
        """Check conversion third site hydro."""
        self.assertEqual(sh.code, 'O2000040')
        self.assertEqual(sh.typesite, 'STANDARD')
        # check station
        station = sh.stations[0]
        self.assertEqual(station.libellecomplement, 'échelle principale')
        self.assertEqual(station.coord.x, 15)
        self.assertEqual(station.coord.y, 16)
        self.assertEqual(station.coord.proj, 26)
        self.assertEqual(sh.zonehydro.code, 'A000')
        self.assertIsNone(sh.zonehydro.libelle)
        self.assertEqual(len(sh.pluiesbassin), 0)

    def check_sitehydro_3(self, site):
        """Check conversion fourth site hydro."""
        self.assertEqual(site.precisiontype, 4)
        self.assertEqual(site.coord.x, 618766)
        self.assertEqual(site.coord.y, 1781803)
        self.assertEqual(site.coord.proj, 26)
        self.assertEqual(site.codeh2, 'O1235401')
        self.assertEqual(len(site.entitesvigicrues), 2)
        self.assertEqual(site.entitesvigicrues[0].code, 'AG3')
        self.assertEqual(site.entitesvigicrues[1].code, 'AG5')
        self.assertEqual(
            site.entitesvigicrues[1].libelle, 'Troncon Adour àvâl')
        self.assertEqual(site.entitehydro.code, 'Y1524018')
        self.assertEqual(site.entitehydro.libelle, 'entitéhydro')
        self.assertEqual(len(site.images), 2)
        image0 = site.images[0]
        self.assertEqual((image0.adresse, image0.typeill,
                          image0.formatimg, image0.commentaire),
                         ('http://image1.jpeg', 1,
                          'image/jpeg', 'Commentaire'))
        image1 = site.images[1]
        self.assertEqual((image1.adresse, image1.typeill,
                          image1.formatimg, image1.commentaire),
                         ('http://image2.bmp', 2,
                          'image/bmp', None))

        self.assertEqual(site.tronconhydro, 'O0011532')
        self.assertEqual(site.zonehydro.code, 'H420')
        self.assertEqual(site.zonehydro.libelle, 'Libellé zone hydro')
        self.assertEqual(site.precisioncoursdeau, 'bras principal')
        self.assertEqual(site.mnemo, 'Mnémo')
        self.assertEqual(site.complementlibelle, 'Complément libellé')
        self.assertEqual(site.pkamont, 990000.3)
        self.assertEqual(site.pkaval, 880000.8)
        self.assertIsNotNone(site.altitude)
        self.assertEqual(site.altitude.altitude, 175.4)
        self.assertEqual(site.altitude.sysalti, 3)
        self.assertEqual(site.dtmaj, datetime.datetime(2017, 1, 17, 11, 6, 29))
        self.assertEqual(site.bvtopo, 69.3)
        self.assertEqual(site.bvhydro, 68.4)
        self.assertEqual(site.fuseau, 2)
        self.assertEqual(site.statut, 1)
        self.assertEqual(site.dtpremieredonnee,
                         datetime.datetime(2007, 3, 1, 9, 16, 44))
        self.assertEqual(site.moisetiage, 9)
        self.assertEqual(site.moisanneehydro, 3)
        self.assertEqual(site.dureecrues, 240)
        self.assertEqual(site.publication, 10)
        self.assertEqual(site.essai, False)
        self.assertEqual(site.influence, 2)
        self.assertEqual(site.influencecommentaire, 'Commentaire influence')
        self.assertEqual(site.commentaire, 'Commentaire du site')
        self.assertIsNotNone(site.siteassocie)
        self.assertEqual(site.siteassocie.code, 'A8742651')

        self.assertEqual(len(site.periodes_shv), 2)
        periode1 = site.periodes_shv[0]
        self.assertEqual(periode1.dtactivation,
                         datetime.datetime(2016, 12, 13, 21, 14, 30))
        self.assertEqual(periode1.dtdesactivation,
                         datetime.datetime(2018, 4, 27, 8, 54, 50))
        self.assertEqual(periode1.dtdeb,
                         datetime.datetime(2015, 3, 17, 11, 26, 34))
        self.assertEqual(periode1.dtfin,
                         datetime.datetime(2017, 9, 11, 17, 38, 53))
        self.assertEqual(len(periode1.sitesattaches), 2)
        siteattache1 = periode1.sitesattaches[0]
        self.assertEqual(siteattache1.code, 'B1256982')
        self.assertEqual(siteattache1.libelle, 'Libellé B1256982')
        self.assertEqual(siteattache1.ponderation, 0.7)
        self.assertEqual(siteattache1.decalage, 15)

        siteattache2 = periode1.sitesattaches[1]
        self.assertEqual(siteattache2.code, 'L3542168')
        self.assertIsNone(siteattache2.libelle)
        self.assertEqual(siteattache2.ponderation, 0.3)
        self.assertEqual(siteattache2.decalage, 0)

        periode2 = site.periodes_shv[1]
        self.assertEqual(periode2.dtactivation,
                         datetime.datetime(2018, 5, 1, 13, 17, 14))
        self.assertIsNone(periode2.dtdesactivation)
        self.assertEqual(periode2.dtdeb,
                         datetime.datetime(2018, 5, 10, 9, 15, 17))
        self.assertIsNone(periode2.dtfin)
        self.assertEqual(len(periode2.sitesattaches), 1)
        siteattache1 = periode2.sitesattaches[0]
        self.assertEqual(siteattache1.code, 'K1256982')
        self.assertIsNone(siteattache1.libelle)
        self.assertEqual(siteattache1.ponderation, 0.1)
        self.assertEqual(siteattache1.decalage, 0)

        self.assertEqual(site.massedeau, 'FRGG111')

        self.assertEqual(len(site.loisstat), 3)
        loi1 = site.loisstat[0]
        self.assertEqual(loi1.contexte, 1)
        self.assertEqual(loi1.loi, 2)
        loi2 = site.loisstat[1]
        self.assertEqual(loi2.contexte, 2)
        self.assertEqual(loi2.loi, 0)
        loi3 = site.loisstat[2]
        self.assertEqual(loi3.contexte, 3)
        self.assertEqual(loi3.loi, 3)

        self.assertEqual(len(site.roles), 2)
        role1 = site.roles[0]
        self.assertEqual(role1.contact.code, '2')
        self.assertEqual(role1.role, 'ADM')
        self.assertIsNone(role1.dtdeb)
        self.assertIsNone(role1.dtfin)
        self.assertIsNone(role1.dtmaj)

        role2 = site.roles[1]
        self.assertEqual(role2.contact.code, '1234')
        self.assertEqual(role2.role, 'REF')
        self.assertEqual(role2.dtdeb,
                         datetime.datetime(2010, 5, 17, 11, 26, 39))
        self.assertEqual(role2.dtfin,
                         datetime.datetime(2038, 1, 19, 20, 55, 30))
        self.assertEqual(role2.dtmaj,
                         datetime.datetime(2017, 11, 4, 9, 23, 31))

        self.assertEqual(len(site.pluiesbassin), 2)
        sitepondere1 = site.pluiesbassin[0]
        sitepondere2 = site.pluiesbassin[1]
        self.assertEqual((sitepondere1.code, sitepondere1.ponderation),
                         ('031200001', 10))
        self.assertEqual((sitepondere2.code, sitepondere2.ponderation),
                         ('031200005', 20))

        self.assertEqual(len(site.sitesamont), 2)
        siteamont1 = site.sitesamont[0]
        self.assertEqual(siteamont1.code, 'A1254895')
        self.assertEqual(siteamont1.libelle, 'Libellé premier site amont')
        siteamont2 = site.sitesamont[1]
        self.assertEqual(siteamont2.code, 'B5423657')
        self.assertIsNone(siteamont2.libelle)

        self.assertEqual(len(site.sitesaval), 2)
        siteaval1 = site.sitesaval[0]
        self.assertEqual(siteaval1.code, 'C1594521')
        self.assertEqual(siteaval1.libelle, 'Libellé premier site aval')
        siteaval2 = site.sitesaval[1]
        self.assertEqual(siteaval2.code, 'D1478541')
        self.assertIsNone(siteaval2.libelle)

        self.assertEqual(site.cdbnbv, 'AA0000')

        # check station
        station = site.stations[0]
        self.assertEqual(station.code, 'O171251001')
        self.assertEqual(station.libelle,
                         'L\'Ariège à Auterive - station de secours')
        self.assertEqual(station.typestation, 'DEB')
        self.assertEqual(station.libellecomplement, 'Complément du libellé')
        self.assertEqual(station.commentaireprive, 'Station située à Auterive')
        self.assertEqual(station.dtmaj,
                         datetime.datetime(2017, 7, 17, 11, 23, 34))
        coord = station.coord
        self.assertEqual((coord.x, coord.y, coord.proj),
                         (15.0, 16.0, 26))
        self.assertEqual(station.pointk, 153.71)
        self.assertEqual(station.dtmiseservice,
                         datetime.datetime(1991, 10, 7, 14, 15, 16))
        self.assertEqual(station.dtfermeture,
                         datetime.datetime(2012, 4, 21, 19, 58, 3))
        self.assertEqual(station.surveillance, True)
        self.assertEqual(station.niveauaffichage, 1)
        self.assertEqual(station.droitpublication, 20)
        self.assertEqual(station.delaidiscontinuite, 13)
        self.assertEqual(station.delaiabsence, 27)
        self.assertEqual(station.essai, False)
        self.assertEqual(station.influence, 2)
        self.assertEqual(station.influencecommentaire, 'Libellé influence')
        self.assertEqual(station.commentaire,
                         'commentaire1 création station hydro')
        self.assertEqual(len(station.stationsanterieures), 2)
        stationanterieure0 = station.stationsanterieures[0]
        self.assertEqual(stationanterieure0.code, 'R875653542')
        self.assertEqual(stationanterieure0.libelle,
                         'première station antérieure')
        stationanterieure1 = station.stationsanterieures[1]
        self.assertEqual(stationanterieure1.code, 'P856475215')
        self.assertIsNone(stationanterieure1.libelle)

        self.assertEqual(len(station.stationsposterieures), 2)
        stationposterieure0 = station.stationsposterieures[0]
        self.assertEqual(stationposterieure0.code, 'Q854695321')
        self.assertEqual(stationposterieure0.libelle,
                         'station postérieure')
        stationposterieure1 = station.stationsposterieures[1]
        self.assertEqual(stationposterieure1.code, 'K777569884')
        self.assertIsNone(stationposterieure1.libelle)

        self.assertEqual(len(station.qualifsdonnees), 2)
        qualif0 = station.qualifsdonnees[0]
        self.assertEqual(qualif0.coderegime, 1)
        self.assertEqual(qualif0.qualification, 12)
        self.assertEqual(qualif0.commentaire, 'Commentaire de la qualif')

        qualif1 = station.qualifsdonnees[1]
        self.assertEqual(qualif1.coderegime, 2)
        self.assertEqual(qualif1.qualification, 16)
        self.assertIsNone(qualif1.commentaire)

        self.assertEqual(station.finalites, [1, 2])
        self.assertEqual(len(station.loisstat), 3)
        loi0 = station.loisstat[0]
        self.assertEqual(loi0.contexte, 1)
        self.assertEqual(loi0.loi, 1)
        loi1 = station.loisstat[1]
        self.assertEqual(loi1.contexte, 3)
        self.assertEqual(loi1.loi, 2)
        loi2 = station.loisstat[2]
        self.assertEqual(loi2.contexte, 2)
        self.assertEqual(loi2.loi, 3)

        self.assertEqual(len(station.images), 2)
        image0 = station.images[0]
        self.assertEqual((image0.adresse, image0.typeill, image0.formatimg,
                          image0.commentaire),
                         ('http://toto.fr/station.png', 2, 'png',
                          'Image de la station'))
        image1 = station.images[1]
        self.assertEqual((image1.adresse, image1.typeill, image1.formatimg,
                          image1.commentaire),
                         ('http://tata.fr/station2.bmp', None, None, None))

        self.assertEqual(len(station.roles), 2)
        rol0 = station.roles[0]
        self.assertEqual(rol0.contact.code, '2')
        self.assertEqual(rol0.role, 'ADM')
        self.assertEqual(rol0.dtdeb,
                         datetime.datetime(2005, 7, 11, 15, 37, 43))
        self.assertEqual(rol0.dtfin,
                         datetime.datetime(2007, 3, 29, 13, 14, 54))
        self.assertEqual(rol0.dtmaj,
                         datetime.datetime(2009, 10, 3, 8, 27, 11))

        rol1 = station.roles[1]
        self.assertEqual(rol1.contact.code, '954')
        self.assertEqual(rol1.role, 'REF')
        self.assertIsNone(rol1.dtdeb)
        self.assertIsNone(rol1.dtfin)
        self.assertIsNone(rol1.dtmaj)

        self.assertEqual(len(station.plages), 2)
        plage0 = station.plages[0]
        self.assertEqual(plage0.dtdeb,
                         datetime.datetime(2006, 4, 25, 16, 0, 0))
        self.assertEqual(plage0.dtfin,
                         datetime.datetime(2006, 4, 30, 17, 0, 0))
        self.assertEqual(plage0.dtactivation,
                         datetime.datetime(2007, 11, 19, 17, 11, 6))
        self.assertEqual(plage0.dtdesactivation,
                         datetime.datetime(2012, 5, 4, 9, 15, 24))
        self.assertTrue(plage0.active)

        plage1 = station.plages[1]
        self.assertEqual(plage1.dtdeb,
                         datetime.datetime(2008, 4, 13, 19, 15, 13))
        self.assertEqual(plage1.dtfin,
                         datetime.datetime(2009, 5, 26, 9, 10, 54))
        self.assertIsNone(plage1.dtactivation)
        self.assertIsNone(plage1.dtdesactivation)
        self.assertFalse(plage1.active)

        # TODO add Reseau
        self.assertEqual(len(station.reseaux), 2)
        reseau0 = station.reseaux[0]
        self.assertEqual((reseau0.code, reseau0.libelle),
                         ('10', 'premier réseau'))
        reseau1 = station.reseaux[1]
        self.assertEqual((reseau1.code, reseau1.libelle),
                         ('1000000001', None))

        self.assertEqual(len(station.refsalti), 2)
        ref0 = station.refsalti[0]
        self.assertEqual(ref0.dtdeb, datetime.datetime(2006, 1, 1, 8, 0, 0))
        self.assertEqual(ref0.dtfin, datetime.datetime(2006, 1, 31, 10, 0, 0))
        self.assertEqual(ref0.dtactivation,
                         datetime.datetime(2007, 12, 20, 6, 7, 44))
        self.assertEqual(ref0.dtdesactivation,
                         datetime.datetime(2009, 9, 14, 10, 50, 24))
        self.assertEqual(ref0.altitude.altitude, 999.0)
        self.assertEqual(ref0.altitude.sysalti, 4)
        self.assertEqual(ref0.dtmaj,
                         datetime.datetime(2015, 3, 24, 12, 14, 39))
        ref1 = station.refsalti[1]
        self.assertEqual(ref1.dtdeb, datetime.datetime(2006, 2, 1, 8, 0, 0))
        self.assertEqual(ref1.dtfin, datetime.datetime(2006, 2, 28, 10, 0, 0))
        self.assertIsNone(ref1.dtactivation)
        self.assertIsNone(ref1.dtdesactivation)
        self.assertEqual(ref1.altitude.altitude, 777.0)
        self.assertEqual(ref1.altitude.sysalti, 7)
        self.assertIsNone(ref1.dtmaj)

        self.assertEqual(station.codeh2, 'O1712510')
        self.assertEqual(station.commune.code, '11354')
        self.assertEqual(station.commune.libelle, 'Libellé commune')

        self.assertEqual(len(station.stationsamont), 2)
        stationamont0 = station.stationsamont[0]
        self.assertEqual(stationamont0.code, 'A985475492')
        self.assertEqual(stationamont0.libelle, 'Station amont')

        stationamont1 = station.stationsamont[1]
        self.assertEqual(stationamont1.code, 'C542549853')
        self.assertIsNone(stationamont1.libelle)

        self.assertEqual(len(station.stationsaval), 2)
        stationaval0 = station.stationsaval[0]
        self.assertEqual(stationaval0.code, 'L784747486')
        self.assertEqual(stationaval0.libelle, 'Station Aval')

        stationaval1 = station.stationsaval[1]
        self.assertEqual(stationaval1.code, 'M125412010')
        self.assertIsNone(stationaval1.libelle)

        self.assertEqual(len(station.plagesstationsfille), 2)
        plagestationfille0 = station.plagesstationsfille[0]
        self.assertEqual(plagestationfille0.code, 'W845621543')
        self.assertEqual(plagestationfille0.libelle, 'Station fille')
        self.assertEqual(plagestationfille0.dtdeb,
                         datetime.datetime(2016, 1, 17, 6, 14, 29))
        self.assertEqual(plagestationfille0.dtfin,
                         datetime.datetime(2017, 12, 20, 18, 25, 45))

        plagestationfille1 = station.plagesstationsfille[1]
        self.assertEqual(plagestationfille1.code, 'S123456781')
        self.assertIsNone(plagestationfille1.libelle)
        self.assertEqual(plagestationfille1.dtdeb,
                         datetime.datetime(2016, 1, 17, 6, 14, 29))
        self.assertIsNone(plagestationfille1.dtfin)

        self.assertEqual(len(station.plagesstationsmere), 2)
        plagestationmere0 = station.plagesstationsmere[0]
        self.assertEqual(plagestationmere0.code, 'G123212312')
        self.assertEqual(plagestationmere0.libelle, 'Station mère')
        self.assertEqual(plagestationmere0.dtdeb,
                         datetime.datetime(2003, 10, 4, 9, 53, 11))
        self.assertEqual(plagestationmere0.dtfin,
                         datetime.datetime(2006, 3, 18, 17, 19, 2))

        plagestationmere1 = station.plagesstationsmere[1]
        self.assertEqual(plagestationmere1.code, 'Z001254921')
        self.assertIsNone(plagestationmere1.libelle)
        self.assertEqual(plagestationmere1.dtdeb,
                         datetime.datetime(2008, 4, 1, 13, 25, 18))
        self.assertIsNone(plagestationmere1.dtfin)

        # checkcapteurs
        capteurs = station.capteurs
        self.assertEqual(len(capteurs), 2)
        self.assertEqual(capteurs[0].code, 'O17125100102')
        self.assertEqual(capteurs[0].typemesure, 'H')
        self.assertEqual(capteurs[0].typecapteur, 0)  # default type

        self.assertEqual(capteurs[1].code, 'O17125100101')
        self.assertEqual(capteurs[1].libelle, 'Ultrasons principal')
        self.assertEqual(capteurs[1].mnemo, 'UP')
        self.assertEqual(capteurs[1].typemesure, 'H')
        self.assertEqual(capteurs[1].codeh2, 'O1712510')
        self.assertEqual(capteurs[1].typecapteur, 16)
        self.assertEqual(capteurs[1].surveillance, False)
        self.assertEqual(capteurs[1].dtmaj,
                         datetime.datetime(2016, 5, 18, 14, 5, 35))
        self.assertEqual(capteurs[1].pdt, 6)
        self.assertEqual(capteurs[1].essai, True)
        self.assertEqual(capteurs[1].commentaire, 'Capteur jaune')
        self.assertEqual(capteurs[1].observateur.code, '3')

        # check plages utilisatino capteurs
        self.assertEqual(len(capteurs[0].plages), 0)
        self.assertEqual(len(capteurs[1].plages), 2)
        plage = capteurs[1].plages[0]
        self.assertEqual(plage.dtdeb,
                         datetime.datetime(2009, 11, 3, 15, 19, 18))
        self.assertEqual(plage.dtfin,
                         datetime.datetime(2015, 3, 21, 11, 14, 47))
        self.assertEqual(plage.dtactivation,
                         datetime.datetime(2014, 12, 14, 18, 27, 32))
        self.assertEqual(plage.dtdesactivation,
                         datetime.datetime(2015, 10, 25, 19, 13, 4))
        self.assertEqual(plage.active, True)

        plage = capteurs[1].plages[1]
        self.assertEqual(plage.dtdeb,
                         datetime.datetime(2016, 1, 15, 12, 14, 13))
        self.assertIsNone(plage.dtfin)
        self.assertIsNone(plage.dtactivation)
        self.assertIsNone(plage.dtdesactivation)
        self.assertIsNone(plage.active)


class CheckConversionSeuilsHydro(unittest.TestCase):
    """Check conversion seuils hydro."""

    def check_seuilhydro_0(self, seuil):
        """Check conversion fisrt seuil hydro."""
        self.assertEqual(seuil.code, 2214)
        self.assertEqual(seuil.sitehydro.code, 'U2655010')
        self.assertEqual(seuil.sitehydro.libelle, 'Libellé du site')
        # check the seuil
        # self.assertEqual(seuil.sh, sh)  # FIXME
        self.assertEqual(seuil.typeseuil, 1)
        self.assertEqual(seuil.duree, 0)
        self.assertEqual(seuil.nature, 32)
        self.assertEqual(seuil.libelle, 'Crue du 24/11/2003')
        self.assertEqual(seuil.mnemo, 'Mnemonique')
        self.assertEqual(seuil.gravite, 65)
        self.assertEqual(seuil.commentaire, 'Commentaire du seuil')
        self.assertEqual(seuil.publication, 12)
        self.assertEqual(seuil.valeurforcee, True)
        self.assertEqual(seuil.dtmaj, datetime.datetime(2012, 2, 19, 8, 25))
        self.assertEqual(seuil._strict, True)

        # check the values
        self.assertEqual(len(seuil.valeurs), 1)
        self.assertEqual(seuil.valeurs[0].valeur, 7000465)
        # self.assertEqual(seuil.valeurs[0].seuil, seuil)  # FIXME
        self.assertEqual(seuil.valeurs[0].entite, seuil.sitehydro)
        self.assertEqual(seuil.valeurs[0].entite.libelle, 'Libellé du site')

        self.assertEqual(seuil.valeurs[0].tolerance, 100)
        self.assertEqual(seuil.valeurs[0].dtactivation,
                         datetime.datetime(2010, 5, 17, 13, 40, 2))
        self.assertEqual(seuil.valeurs[0].dtdesactivation,
                         datetime.datetime(2012, 2, 19, 9, 28))
        self.assertEqual(seuil.valeurs[0]._strict, True)

    def check_seuilhydro_1(self, seuil):
        """Check conversion second seuil hydro."""
        self.assertEqual(seuil.code, 82)
        self.assertEqual(seuil.sitehydro.code, 'O2000040')
        self.assertEqual(seuil.sitehydro.libelle, 'Libellé du site')

        # check the seuil
        # self.assertEqual(seuil.sitehydro, sitehydro)  # FIXME
        self.assertEqual(seuil.typeseuil, 2)
        self.assertEqual(seuil.duree, 60)
        self.assertEqual(seuil.nature, 32)
        self.assertEqual(seuil.libelle, 'Gradient durée 60')
        self.assertEqual(seuil.mnemo, None)
        self.assertEqual(seuil.gravite, None)
        self.assertEqual(seuil.commentaire, None)
        self.assertEqual(seuil.publication, 22)
        self.assertEqual(seuil.valeurforcee, None)
        self.assertEqual(seuil.dtmaj,
                         datetime.datetime(2014, 3, 23, 9, 51, 56))

        # check the values
        self.assertEqual(len(seuil.valeurs), 4)
        self.assertEqual(seuil.valeurs[0].valeur, 85)
        self.assertEqual(seuil.valeurs[1].valeur, 4380)
        self.assertEqual(seuil.valeurs[2].valeur, 3520)
        self.assertEqual(seuil.valeurs[3].valeur, 8320)
        self.assertEqual(seuil.valeurs[0].seuil, seuil)
        self.assertEqual(seuil.valeurs[0].entite.code, 'O2000040')
        self.assertEqual(seuil.valeurs[1].entite.code, 'O200004001')
        self.assertEqual(seuil.valeurs[2].entite.code, 'O200004002')
        self.assertEqual(seuil.valeurs[3].entite.code, 'O200004003')
        self.assertEqual(seuil.valeurs[0].tolerance, 5)
        self.assertEqual(seuil.valeurs[1].tolerance, 20)
        self.assertEqual(seuil.valeurs[2].tolerance, None)
        self.assertEqual(seuil.valeurs[3].tolerance, 10)
        self.assertEqual(seuil.valeurs[0].dtactivation, None)
        self.assertEqual(seuil.valeurs[1].dtactivation,
                         datetime.datetime(2010, 6, 10, 10, 52, 57))
        self.assertEqual(seuil.valeurs[2].dtactivation,
                         datetime.datetime(2010, 6, 10, 11, 32, 57))
        self.assertEqual(seuil.valeurs[3].dtactivation,
                         datetime.datetime(2010, 6, 10, 11, 52, 57))
        self.assertEqual(seuil.valeurs[0].dtdesactivation, None)
        self.assertEqual(seuil.valeurs[1].dtdesactivation, None)
        self.assertEqual(seuil.valeurs[2].dtdesactivation,
                         datetime.datetime(2013, 10, 5, 5, 59, 29))
        self.assertEqual(seuil.valeurs[3].dtdesactivation, None)

    def check_seuilhydro_5(self, seuil):
        """Check conversion sixth site hydro."""
        self.assertEqual(seuil.code, 338)
        self.assertEqual(seuil.sitehydro.code, 'O6793330')
        self.assertIsNone(seuil.sitehydro.libelle)

        self.assertEqual(seuil.typeseuil, 1)
        self.assertEqual(seuil.duree, 0)
        self.assertEqual(seuil.nature, 22)
        self.assertIsNone(seuil.libelle)
        self.assertEqual(seuil.mnemo, 'Seuil de vigilance JAUNE')
        self.assertEqual(seuil.gravite, 25)
        self.assertEqual(seuil.publication, 32)
        self.assertEqual(seuil.valeurforcee, True)
        self.assertEqual(seuil.commentaire, 'Commentaire du seuil')
        self.assertEqual(seuil.dtmaj, datetime.datetime(2012, 2, 19, 8, 25, 0))

        self.assertEqual(len(seuil.valeurs), 4)
        valeur0 = seuil.valeurs[0]
        self.assertEqual(valeur0.valeur, 100)
        self.assertEqual(valeur0.entite.code, 'O6793330')
        self.assertIsNone(valeur0.entite.libelle)

        valeur1 = seuil.valeurs[1]
        self.assertEqual(valeur1.valeur, 200)
        self.assertEqual(valeur1.entite.code, 'O679333001')
        self.assertEqual(valeur1.entite.libelle, 'Libellé de la station')

        valeur2 = seuil.valeurs[2]
        self.assertEqual(valeur2.valeur, 300)
        self.assertEqual(valeur2.entite.code, 'O67933300101')
        self.assertEqual(valeur2.entite.libelle, 'Libellé du capteur')

        valeur3 = seuil.valeurs[3]
        self.assertEqual(valeur3.valeur, 400)
        self.assertEqual(valeur3.entite.code, 'O67933300102')
        self.assertIsNone(valeur3.entite.libelle)


class CheckConversionSeuilsMeteo(unittest.TestCase):
    """Check conversion seuils météo."""

    def check_seuilmeteo_0(self, seuil):
        """Check conversion fisrt seuil meteo."""
        self.assertEqual(seuil.code, 2214)
        self.assertEqual(seuil.grandeurmeteo.sitemeteo.code, '012345678')
        self.assertEqual(seuil.grandeurmeteo.sitemeteo.libelle, 'Libéllé du site météo')
        self.assertEqual(seuil.grandeurmeteo.typemesure, 'RR')
        self.assertEqual(seuil.grandeurmeteo.dtmiseservice,
                         datetime.datetime(1990, 1, 17, 14, 36, 39))

        self.assertEqual(seuil.typeseuil, 2)
        self.assertEqual(seuil.nature, 32)
        self.assertEqual(seuil.duree, 60)

        self.assertEqual(seuil.libelle, 'Libellé usuel')
        self.assertEqual(seuil.mnemo, 'Mnémo')
        self.assertEqual(seuil.gravite, 85)
        self.assertEqual(seuil.dtmaj, datetime.datetime(2015, 4, 2, 11, 29, 1))
        self.assertEqual(seuil.commentaire, 'Commentaire')

        self.assertEqual(seuil._strict, True)

        # check the values
        self.assertEqual(len(seuil.valeurs), 2)
        self.assertEqual(seuil.valeurs[0].valeur, 50.5)
        # self.assertEqual(seuil.valeurs[0].seuil, seuil)  # FIXME
        self.assertEqual(seuil.valeurs[0].entite, seuil.grandeurmeteo)
        self.assertEqual(seuil.valeurs[0].tolerance, 1.5)
        self.assertEqual(seuil.valeurs[0].dtactivation,
                         datetime.datetime(2000, 8, 14, 15, 10, 2))
        self.assertEqual(seuil.valeurs[0].dtdesactivation,
                         datetime.datetime(2010, 11, 10, 17, 23, 45))
        self.assertEqual(seuil.valeurs[0]._strict, True)

        self.assertEqual(seuil.valeurs[1].valeur, 52)
        # self.assertEqual(seuil.valeurs[0].seuil, seuil)  # FIXME
        self.assertEqual(seuil.valeurs[1].entite, seuil.grandeurmeteo)
        self.assertIsNone(seuil.valeurs[1].tolerance)
        self.assertEqual(seuil.valeurs[1].dtactivation,
                         datetime.datetime(2011, 3, 13, 7, 10, 14))
        self.assertIsNone(seuil.valeurs[1].dtdesactivation)
        self.assertEqual(seuil.valeurs[1]._strict, True)

    def check_seuilmeteo_1(self, seuil):
        """Check conversion second seuil meteo."""
        self.assertEqual(seuil.code, 3505)
        self.assertEqual(seuil.grandeurmeteo.sitemeteo.code, '012345678')
        self.assertIsNone(seuil.grandeurmeteo.sitemeteo.libelle)
        self.assertEqual(seuil.grandeurmeteo.typemesure, 'TA')
        self.assertIsNone(seuil.grandeurmeteo.dtmiseservice)

        self.assertIsNone(seuil.typeseuil)
        self.assertIsNone(seuil.nature)
        self.assertIsNone(seuil.duree)

        self.assertIsNone(seuil.libelle)
        self.assertIsNone(seuil.mnemo)
        self.assertIsNone(seuil.gravite)
        self.assertIsNone(seuil.dtmaj)
        self.assertIsNone(seuil.commentaire, 'Commentaire')

        self.assertEqual(seuil._strict, True)

        # check the values
        self.assertEqual(len(seuil.valeurs), 0)


class CheckConversionModelesPrevision(unittest.TestCase):
    """Check conversion modèles de prévision."""

    def check_modele_0(self, modele):
        """Check conversion fisrt modele prevision."""
        self.assertEqual(modele.contact.code, '1234')
        self.assertEqual(modele.code, '9876543210')
        self.assertEqual(modele.libelle, 'Libellé du modèle')
        self.assertEqual(modele.typemodele, 1)
        self.assertEqual(modele.description, 'Description du modèle')
        self.assertEqual(modele.dtmaj,
                         datetime.datetime(2001, 12, 17, 4, 30, 47))
        self.assertEqual(len(modele.siteshydro), 2)
        self.assertEqual(modele.siteshydro[0].code, 'A1234567')
        self.assertEqual(modele.siteshydro[1].code, 'Z7654321')

    def check_modele_1(self, modele):
        """Check conversion second modele prevision."""
        self.assertIsNone(modele.contact)
        self.assertEqual(modele.code, '0123456789')
        self.assertIsNone(modele.libelle)
        self.assertEqual(modele.typemodele, 0)
        self.assertIsNone(modele.description)
        self.assertIsNone(modele.dtmaj)
        self.assertEqual(modele.siteshydro, [])


class CheckConversionEvenements(unittest.TestCase):
    """Check conversion événements."""

    def check_evt_0(self, evt):
        """Check conversion fisrt evenement."""
        self.assertEqual(evt.entite.code, 'A123456789')
        self.assertEqual(evt.contact.code, '897')
        self.assertEqual(evt.dt, datetime.datetime(2015, 6, 21, 9, 54, 32))
        self.assertEqual(evt.typeevt, 4)
        self.assertEqual(evt.descriptif, 'Evènement')
        self.assertEqual(evt.publication, 32)
        self.assertEqual(evt.dtmaj, datetime.datetime(2017, 7, 23, 11, 12, 3))
        self.assertEqual(len(evt.ressources), 2)
        ressource1 = evt.ressources[0]
        self.assertEqual(ressource1.url, 'www.toto.fr')
        self.assertEqual(ressource1.libelle, 'Libellé1')
        ressource2 = evt.ressources[1]
        self.assertEqual(ressource2.url, 'www.tata.fr')
        self.assertIsNone(ressource2.libelle)
        self.assertEqual(evt.dtfin, datetime.datetime(2016, 10, 9, 16, 14, 38))

    def check_evt_1(self, evt):
        """Check conversion second evenement."""
        self.assertEqual(evt.entite.code, 'Z7654321')
        self.assertEqual(evt.contact.code, '1503')
        self.assertEqual(evt.dt, datetime.datetime(2010, 1, 15, 11, 23, 43))
        self.assertEqual(evt.typeevt, 0)
        self.assertEqual(evt.descriptif, 'Absence de données')
        self.assertEqual(evt.publication, 0)
        self.assertIsNone(evt.dtmaj)
        self.assertEqual(len(evt.ressources), 0)
        self.assertIsNone(evt.dtfin)

    def check_evt_2(self, evt):
        """Check conversion third evenement."""
        self.assertEqual(evt.entite.code, '001234567')
        self.assertEqual(evt.contact.code, '43')
        self.assertEqual(evt.dt, datetime.datetime(2018, 6, 25, 10, 4, 37))
        self.assertEqual(evt.typeevt, 3)
        self.assertEqual(evt.descriptif, 'Site météo vandalisé')
        self.assertEqual(evt.publication, 12)
        self.assertIsNone(evt.dtmaj)
        self.assertEqual(len(evt.ressources), 0)
        self.assertIsNone(evt.dtfin)


class CheckConversionGradients(unittest.TestCase):
    """Check conversion gradients."""

    def check_gradients_0(self, serie):
        """Check conversion fisrt first serie of gradients."""
        self.assertEqual(serie.duree, 60)
        self.assertEqual(serie.entite.code, 'A1234567')
        self.assertEqual(serie.grd, 'Q')
        self.assertEqual(serie.contact.code, '158')
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2008, 12, 15, 11, 7, 54))
        self.assertEqual(len(serie.gradients), 2)
        self.assertEqual(serie.gradients['res'].to_list(), [150.8, 130.2])
        self.assertEqual(serie.gradients['mth'].to_list(), [8, 4])
        self.assertEqual(serie.gradients['qal'].to_list(), [16, 12])
        self.assertEqual(serie.gradients['statut'].to_list(), [4, 8])
        self.assertEqual(serie.gradients.index.to_list(),
                         [datetime.datetime(2008, 12, 15, 9, 0, 0),
                          datetime.datetime(2008, 12, 15, 10, 0, 0)])

    def check_gradients_1(self, serie):
        """Check conversion second serie of gradients."""
        self.assertEqual(serie.duree, 30)
        self.assertEqual(serie.entite.code, 'A123456789')
        self.assertEqual(serie.grd, 'H')
        self.assertEqual(serie.contact.code, '130')
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2009, 12, 15, 11, 7, 54))
        self.assertEqual(len(serie.gradients), 1)
        self.assertEqual(serie.gradients['res'].to_list(), [15.8])
        self.assertEqual(serie.gradients['mth'].to_list(), [0])
        self.assertEqual(serie.gradients['qal'].to_list(), [20])
        self.assertEqual(serie.gradients['statut'].to_list(), [12])
        self.assertEqual(serie.gradients.index.to_list(),
                         [datetime.datetime(2009, 12, 15, 9, 0, 0)])

    def check_gradients_2(self, serie):
        """Check conversion third serie of gradients."""
        self.assertEqual(serie.duree, 30)
        self.assertEqual(serie.entite.code, 'A12345678901')
        self.assertEqual(serie.grd, 'H')
        self.assertEqual(serie.contact.code, '130')
        self.assertEqual(serie.dtprod,
                         datetime.datetime(2009, 12, 15, 11, 7, 54))
        self.assertEqual(len(serie.gradients), 1)
        self.assertEqual(serie.gradients['res'].to_list(), [13.2])
        self.assertEqual(serie.gradients['mth'].to_list(), [14])
        self.assertEqual(serie.gradients['qal'].to_list(), [16])
        self.assertEqual(serie.gradients['statut'].to_list(), [16])
        self.assertEqual(serie.gradients.index.to_list(),
                         [datetime.datetime(2009, 12, 15, 10, 30, 0)])


class CheckConversionSitesMeteo(unittest.TestCase):
    """Check conversion sites météo."""

    def check_sitemeteo_0(self, sm):
        """Check conversion first site meteo."""
        self.assertEqual(sm.code, '001072001')
        self.assertEqual(sm.libelle, 'CEYZERIAT_PTC')
        self.assertEqual(sm.libelleusuel, 'CEYZERIAT')
        self.assertEqual(sm.mnemo, 'Mnémo')
        self.assertEqual(sm.lieudit, 'Aérodrome de bourg Ceyzeriat')
        self.assertEqual(sm.coord.x, 827652)
        self.assertEqual(sm.coord.y, 2112880)
        self.assertEqual(sm.coord.proj, 26)
        self.assertEqual(sm.altitude.altitude, 53.0)
        self.assertEqual(sm.altitude.sysalti, 7)
        self.assertEqual(sm.fuseau, 2)
        self.assertEqual(sm.dtmaj, datetime.datetime(2015, 3, 17, 11, 54, 47))
        self.assertEqual(sm.dtouverture,
                         datetime.datetime(1950, 1, 1, 0, 0, 0))
        self.assertEqual(sm.dtfermeture,
                         datetime.datetime(2007, 10, 24, 9, 8, 1))
        self.assertTrue(sm.droitpublication)
        self.assertFalse(sm.essai)
        self.assertEqual(sm.commentaire, 'Commentaire')

        self.assertEqual(len(sm.images), 2)
        image0 = sm.images[0]
        self.assertEqual((image0.adresse, image0.typeill, image0.formatimg,
                          image0.commentaire),
                         ('http://xxxxxxx', 3, 'image/jpeg',
                          'Photo d\'ensemble depuis le nord'))
        image1 = sm.images[1]
        self.assertEqual((image1.adresse, image1.typeill, image1.formatimg,
                          image1.commentaire),
                         ('http://toto.fr/img.png', None, None, None))

        self.assertEqual(len(sm.reseaux), 2)
        self.assertEqual(sm.reseaux[0].code, '10')
        self.assertEqual(sm.reseaux[1].code, '100000003')
        self.assertEqual(len(sm.roles), 1)
        role = sm.roles[0]
        self.assertEqual(role.contact.code, '2')
        self.assertEqual(role.role, 'ADM')
        self.assertEqual(role.dtdeb, datetime.datetime(2008, 4, 19, 11, 10, 9))
        self.assertEqual(role.dtfin, datetime.datetime(2015, 10, 4, 7, 20, 30))
        self.assertEqual(role.dtmaj,
                         datetime.datetime(2016, 12, 9, 17, 58, 16))

        self.assertEqual(sm.zonehydro.code, 'A123')
        self.assertEqual(sm.zonehydro.libelle, 'Libellé zone')

        self.assertEqual(sm.commune.code, '35281')
        self.assertEqual(sm.commune.libelle, 'Libellé commune')
        self.assertEqual(sm._strict, True)
        self.assertEqual(len(sm.grandeurs), 2)
        for grandeur in sm.grandeurs:
            self.assertEqual(grandeur.sitemeteo, sm)

        grd0 = sm.grandeurs[0]
        self.assertEqual(grd0.typemesure, 'RR')
        self.assertEqual(grd0.dtmiseservice,
                         datetime.datetime(1994, 4, 5, 16, 0, 0))
        self.assertEqual(grd0.dtfermeture,
                         datetime.datetime(2011, 4, 5, 16, 0, 0))
        self.assertEqual(grd0.essai, True)
        self.assertEqual(grd0.surveillance, True)
        self.assertEqual(grd0.delaiabsence, 15)
        self.assertEqual(grd0.pdt, 4)
        self.assertEqual(len(grd0.classesqualite), 1)
        cl0 = grd0.classesqualite[0]
        self.assertEqual(cl0.classe, '3')
        self.assertEqual(cl0.visite.dtvisite,
                         datetime.datetime(1994, 4, 5, 8, 23, 0))
        self.assertEqual(cl0.dtdeb, datetime.datetime(1994, 4, 5, 8, 21, 0))
        self.assertEqual(cl0.dtfin, datetime.datetime(2010, 4, 5, 8, 28, 0))
        self.assertEqual(grd0.dtmaj, datetime.datetime(2012, 9, 4, 12, 54, 17))

        grd1 = sm.grandeurs[1]
        self.assertEqual(grd1.typemesure, 'VV')
        self.assertEqual(grd1.dtmiseservice,
                         datetime.datetime(1998, 4, 5, 16, 0, 0))
        self.assertEqual(grd1.dtfermeture,
                         datetime.datetime(2008, 4, 5, 16, 0, 0))
        self.assertEqual(grd1.essai, False)
        self.assertEqual(grd1.surveillance, None)
        self.assertEqual(grd1.delaiabsence, None)
        self.assertEqual(grd1.pdt, None)
        self.assertEqual(len(grd1.classesqualite), 1)
        cl1 = grd1.classesqualite[0]
        self.assertEqual(cl1.classe, '4S')
        self.assertEqual(cl1.visite.dtvisite,
                         datetime.datetime(1994, 4, 5, 8, 54, 0))
        self.assertEqual(cl1.dtdeb, datetime.datetime(1994, 4, 5, 8, 52, 0))
        self.assertEqual(cl1.dtfin, datetime.datetime(2015, 4, 5, 8, 55, 0))
        self.assertEqual(grd1.dtmaj, None)

        self.assertEqual(len(sm.visites), 2)
        visite = sm.visites[0]
        self.assertEqual(visite.dtvisite,
                         datetime.datetime(2003, 10, 15, 11, 28, 34))
        self.assertIsNone(visite.contact)
        self.assertIsNone(visite.methode)
        self.assertIsNone(visite.modeop)

        visite = sm.visites[1]
        self.assertEqual(visite.dtvisite,
                         datetime.datetime(2004, 4, 5, 19, 36, 0))
        self.assertEqual(visite.contact.code, '4')
        self.assertEqual(visite.methode, 'Méthode à préciser')
        self.assertEqual(visite.modeop, 'Libellé libre')


class CheckConversionValidsAnnee(unittest.TestCase):
    """Check conversion années hydrométriques."""

    def check_validsannee(self, validsannee):
        """Check conversion validsannee."""
        vannees = [vannee for vannee in validsannee.itertuples()]
        vanneesite = vannees[0]
        self.assertEqual(vanneesite.Index, ('A1234567', 2017))
        self.assertEqual(vanneesite.qualif, 12)
        self.assertTrue(_pandas.isnull(vanneesite.dispoh))
        self.assertTrue(_pandas.isnull(vanneesite.dispoq))
        self.assertIsNone(vanneesite.cmnt)
        self.assertTrue(_pandas.isnull(vanneesite.dtmaj))

        vanneestation = vannees[1]
        self.assertEqual(vanneestation.Index, ('Z987654321', 2016))
        self.assertEqual(vanneestation.qualif, 16)
        self.assertEqual(vanneestation.dispoh, 14)
        self.assertEqual(vanneestation.dispoq, 8)
        self.assertEqual(vanneestation.cmnt, 'Année station')
        self.assertEqual(vanneestation.dtmaj,
                         datetime.datetime(2018, 4, 7, 11, 8, 7))


class CheckConversionSimulations(unittest.TestCase):
    """Check conversion simulations."""

    def check_sim_0(self, simulation):
        """Check conversion first simulation."""
        self.assertEqual(simulation.entite.code, 'Y1612020')
        self.assertEqual(simulation.dtprod,
                         datetime.datetime(2010, 2, 26, 14, 45))
        self.assertEqual(simulation.grandeur, 'Q')
        self.assertEqual(simulation.code, 157)
        self.assertEqual(simulation.qualite, 36)
        self.assertEqual(simulation.dtfinvalidite,
                         datetime.datetime(2010, 2, 27, 9, 10, 11))
        self.assertEqual(simulation.dtdeb,
                         datetime.datetime(2010, 2, 26, 15, 30, 10))
        self.assertEqual(simulation.dtfin,
                         datetime.datetime(2010, 2, 26, 18, 45, 30))
        self.assertEqual(simulation.dtbase,
                         datetime.datetime(2010, 2, 26, 14, 30, 0))
        self.assertEqual(simulation.dtderobs,
                         datetime.datetime(2010, 2, 26, 18, 33, 34))
        self.assertEqual(simulation.modecalcul, 3)
        self.assertEqual(simulation.statut, 4)
        self.assertEqual(simulation.publication, 10)
        self.assertEqual(simulation.sysalti, 0)
        self.assertEqual(simulation.contexte, 'Contexte')
        self.assertEqual(simulation.commentaire, 'Biais=-14.91 Précision=36.00')
        self.assertEqual(simulation.cmntprive, 'Commentaire privé')
        self.assertEqual(simulation.mode, 1)
        self.assertEqual(simulation.modeleprevision.code, '13_08')
        self.assertEqual(simulation.contact.code, '41')
        self.assertEqual(simulation.intervenant.code, '1537')

        # check previsions => res
        # FIXME  #1 - restore the full list when the duplicate pandas index is
        #   fixed. Restore also lines 52-55 in test/data/xml/1.1/simulation.xml
        self.assertEqual(set(simulation.previsions_tend['res'].tolist()),
                         {30, 10, 50, 23, 25})

        self.assertEqual(simulation.previsions_det['res'].tolist(), [178, 179])
        self.assertEqual(simulation.previsions_det['incertdte'].tolist(), [8, 0])
        self.assertEqual(
            simulation.previsions_det.index.tolist(),
            [datetime.datetime(2010, 2, 26, 16),
             datetime.datetime(2010, 2, 26, 17)]
        )
        self.assertEqual(set(simulation.previsions_prb.tolist()),
                         {25, 75, 90, 95})
        self.assertEqual(simulation.previsions_prb.iloc[0], 25)
        self.assertEqual(
            simulation.previsions_tend.loc['2010-02-26 15:00']['res'].tolist(), [23, 25])

        self.assertEqual(
            # FIXME #1
            # simulation.previsions.swaplevel(0, 1)[50].tolist(), [30, 95, 23])
            simulation.previsions_prb.swaplevel(0, 1)[50].tolist(), [95])
        self.assertEqual(
            simulation.previsions_prb.swaplevel(0, 1)[40].tolist(), [75])
        # check previsions => index
        # FIXME #1
        # self.assertEqual(len(simulation.previsions.index), 9)
        self.assertEqual(len(simulation.previsions_prb.index), 4)
        self.assertEqual(len(simulation.previsions_tend.index), 5)
        self.assertEqual(
            set([x[0] for x in simulation.previsions_prb.swaplevel(0, 1).index]),
            {20, 40, 49, 50})
        self.assertEqual(
            set([x[0] for x in simulation.previsions_tend.swaplevel(0, 1).index]),
            {'moy', 'min', 'max', 'moy', 'max'})
        # check previsions_tend et previsions_prb
        self.assertEqual(
            simulation.previsions_prb.swaplevel(0, 1)[40].tolist(), [75])
        self.assertEqual(
            simulation.previsions_tend.swaplevel(0, 1).loc['moy']['res'].tolist(),
            [30, 23])
        self.assertEqual(
            simulation.previsions_tend.swaplevel(0, 1).loc['min']['res'].tolist(), [10])
        self.assertEqual(
            simulation.previsions_tend.swaplevel(0, 1).loc['max']['res'].tolist(),
            [50, 25])

        self.assertEqual(len(simulation.prevs_ensemble), 4)
        self.assertEqual(
            simulation.prevs_ensemble.index.tolist(),
            [(datetime.datetime(2010, 2, 27, 1, 0, 0), 'Libellé membre'),
             (datetime.datetime(2010, 2, 27, 1, 0, 0), 'Libellé 2'),
             (datetime.datetime(2010, 2, 27, 2, 0, 0), 'Membre'),
             (datetime.datetime(2010, 2, 27, 2, 0, 0), 'Membre 2')
             ])
        self.assertEqual(
            simulation.prevs_ensemble['res'].tolist(),
            [145.4, 247.54, 15.1, 18.7])
        self.assertEqual(
            simulation.prevs_ensemble['poids'].tolist(),
            [5, 8, 1, 1])

        self.assertEqual(len(simulation.prevs_evol), 2)
        prevevol1 = simulation.prevs_evol[0]
        self.assertEqual(
            (prevevol1.evol, prevevol1.dtdeb, prevevol1.incertdte),
            (1, datetime.datetime(2010, 3, 1, 9, 10, 15), 5)
        )
        prevevol2 = simulation.prevs_evol[1]
        self.assertEqual(
            (prevevol2.evol, prevevol2.dtdeb, prevevol2.incertdte),
            (2, datetime.datetime(2010, 3, 1, 10, 20, 30), None)
        )

        self.assertIsNotNone(simulation.scenario)
        scn = simulation.scenario
        self.assertEqual(
            (scn.libelle, scn.descriptif),
            ('Libellé du scénario de simulation',
             'Descriptif du scénario de simulation')
        )

    def check_sim_1(self, simulation):
        """Check conversion second simulation."""
        self.assertEqual(simulation.entite.code, 'Y161202001')
        self.assertEqual(simulation.dtprod,
                         datetime.datetime(2010, 2, 26, 14, 45))
        self.assertEqual(simulation.code, 999)
        self.assertEqual(simulation.modeleprevision.code, 'ScMerSHOM')
        self.assertEqual(simulation.grandeur, 'H')
        self.assertEqual(simulation.statut, 16)
        self.assertEqual(simulation.qualite, 21)
        self.assertEqual(simulation.publication, 11)

        # check previsions => res
        self.assertEqual(len(simulation.previsions_tend), 8)
        self.assertEqual(simulation.previsions_tend['res'].tolist()[0], 371.774)
        self.assertEqual(simulation.previsions_tend['res'].tolist()[3], 422.280)
        self.assertEqual(simulation.previsions_tend['res'].tolist()[7], 358.71)
        # check previsions => index
        self.assertEqual(len(simulation.previsions_tend.index), 8)
        self.assertEqual(len(simulation.previsions_tend.swaplevel(0, 1).loc['moy']), 8)
        self.assertIsNone(simulation.previsions_det)
        self.assertIsNone(simulation.previsions_prb)

        self.assertIsNotNone(simulation.scenario)
        scn = simulation.scenario
        self.assertEqual(
            (scn.libelle, scn.descriptif),
            ('Libellé', None)
        )

    def check_sim_2(self, simulation):
        """Check conversion third simulation."""
        self.assertEqual(simulation.entite.code, 'Y1612020')
        self.assertEqual(simulation.code, 0)
        self.assertEqual(simulation.modeleprevision.code, '13_09')
        self.assertEqual(simulation.grandeur, 'Q')
        self.assertEqual(simulation.statut, 4)
        self.assertEqual(simulation.qualite, None)
        self.assertEqual(simulation.publication, 22)
        self.assertEqual(simulation.dtprod,
                         datetime.datetime(2010, 2, 26, 9, 45))
        # check previsions => res
        self.assertEqual(len(simulation.previsions_tend), 4)
        self.assertEqual(simulation.previsions_tend['res'].tolist(), [22, 33, 44, 55])
        # check previsions => index
        self.assertEqual(len(simulation.previsions_tend.index), 4)
        self.assertEqual(len(simulation.previsions_tend.swaplevel(0, 1).loc['min']), 2)
        self.assertEqual(len(simulation.previsions_tend.swaplevel(0, 1).loc['max']), 2)
        self.assertIsNone(simulation.previsions_det)
        self.assertIsNone(simulation.previsions_prb)

    def check_sim_3(self, simulation):
        """Check conversion fourth simulation."""
        self.assertEqual(simulation.entite.code, 'Y1612020')
        self.assertEqual(simulation.code, 777)
        self.assertEqual(simulation.modeleprevision.code, '13_09')
        self.assertEqual(simulation.grandeur, 'Q')
        self.assertEqual(simulation.statut, 4)
        self.assertEqual(simulation.qualite, None)
        self.assertEqual(simulation.publication, 22)
        self.assertEqual(simulation.dtprod,
                         datetime.datetime(2010, 2, 27, 11, 45))
        # check previsions
        self.assertIsNone(simulation.previsions_tend)
        self.assertIsNone(simulation.previsions_det)
        self.assertIsNone(simulation.previsions_prb)


# -- class TestFromXmlIntervenants --------------------------------------------
class TestFromXmlIntervenants(CheckConversionIntervenants):

    """FromXmlIntervenants class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'intervenants.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertNotEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(
            scenario.dtprod, datetime.datetime(2001, 12, 17, 4, 30, 47))
        self.assertEqual(
            (scenario.reference, scenario.envoi),
            ('modele.xml', 'envoi.xml'))
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.emetteur.contact.code, '525')
        self.assertEqual(
            scenario.destinataire.intervenant.code, '12345671234567')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SIRET')
        self.assertEqual(scenario.destinataire.contact.code, '2')

    def test_intervenant_0(self):
        """intervenant 0 test."""
        # intervenant
        i = self.data.intervenants[0]
        self.check_intervenant_0(i)

    def test_intervenant_1(self):
        """intervenant 1 test."""
        # intervenant
        i = self.data.intervenants[1]
        self.check_intervenant_1(i)


# -- class TestFromXmlSeriesHydro ---------------------------------------------
class TestFromXmlSeriesHydro(CheckConversionSeriesHydro):
    """FromXmlSeriesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'serieshydro.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertNotEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

    def test_full_serie(self):
        """Test serie with all tags"""
        serie = self.data.serieshydro[0]
        self.check_seriehydro_0(serie)

    def test_minimal_serie(self):
        """Serie and obs with only mandatory tags"""
        serie = self.data.serieshydro[1]
        self.check_seriehydro_1(serie)

    def test_serie_without_obs(self):
        """Serie without obs"""
        serie = self.data.serieshydro[2]
        self.check_seriehydro_2(serie)

    def test_serie_v(self):
        """Serie V"""
        serie = self.data.serieshydro[3]
        self.check_seriehydro_3(serie)


# -- class TestFromXmlSeriesMeteo ---------------------------------------------
class TestFromXmlSeriesMeteo(CheckConversionSeriesMeteo):

    """FromXmlSeriesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'seriesmeteo.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertNotEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        # self.assertEqual(len(self.data.sitesmeteo), 1)
        self.assertEqual(len(self.data.seriesmeteo), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_rr(self):
        """Serie RR test."""
        self.check_seriemeteo_rr(self.data.seriesmeteo)

    def test_serie_ta(self):
        """Serie TA test."""
        self.check_seriemeteo_ta(self.data.seriesmeteo)

    def test_without_obs(self):
        """Serie without obs test."""
        serie = self.data.seriesmeteo[2]
        self.assertIsNone(serie.observations)

    @unittest.skip("todo: obs without res")
    def test_without_res(self):
        """Serie with one obs but without res ."""
        serie = self.data.seriesmeteo[3]
        self.assertIsNotNone(serie.observations)


# -- class TestFromXmlSeriesMeteo ---------------------------------------------
class TestFromXmlSeriesObsElab(CheckConversionSeriesObsElab):

    """FromXmlSeriesObsElab( class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'obsselab.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertNotEqual(self.data.seriesobselab, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_qmnj(self):
        """Serie QMNJ test."""
        serie = self.data.seriesobselab[0]
        self.check_serie_qmnj(serie)

    def test_serie_qixnj(self):
        """Serie QIXnJ with tag DtResObsElaborHydro test."""
        serie = self.data.seriesobselab[1]
        self.check_serie_qixnj(serie)

    @unittest.skip("todo: obs without res")
    def test_without_obs(self):
        """Serie without obs test."""
        serie = self.data.seriesmeteo[2]
        self.assertIsNone(serie.observations)

    @unittest.skip("todo: obs without res")
    def test_without_res(self):
        """Serie with one obs but without res ."""
        serie = self.data.seriesmeteo[3]
        self.assertIsNotNone(serie.observations)


# -- class TestFromXmlSeriesObsElabMeteo --------------------------------------
class TestFromXmlSeriesObsElabMeteo(CheckConversionSeriesObsMeteoElab):

    """XmlSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'obsselabmeteo.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertNotEqual(self.data.seriesobselabmeteo, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_serie_ipa(self):
        """Test série with ipa."""
        serie = self.data.seriesobselabmeteo[0]
        self.check_seriesobselabmeteo_ipa(serie)

    def test_serie_lamedeau(self):
        """Test série lamedeau."""
        serie = self.data.seriesobselabmeteo[1]
        self.check_seriesobselabmeteo_lamedeau(serie)

    def test_minimal_serie(self):
        """Test minima serie."""
        serie = self.data.seriesobselabmeteo[2]
        self.check_seriesobselabmeteo_2(serie)


# -- class TestFromXmlCourbesTarage --------------------------------------
class TestFromXmlCourbesTarage(CheckConversionCourbesTarage):
    """XmlSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'courbestarage.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertNotEqual(self.data.courbestarage, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_full_courbetarage(self):
        """Test full courbe de tarage."""
        courbe = self.data.courbestarage[0]
        self.check_courbetarage_0(courbe)

    def test_min_courbetarage(self):
        """Test minimal courbe tarage."""
        courbe = self.data.courbestarage[1]
        self.check_courbetarage_1(courbe)

    def test_courbetarage_puissance(self):
        """Test courbe de tarage puissance"""
        courbe = self.data.courbestarage[2]
        self.check_courbetarage_2(courbe)


# -- class TestFromXmlCourbesCorrection --------------------------------------
class TestFromXmlCourbesCorrection(CheckConversionCourbesCorrection):
    """XmlSeriesObsElabMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'courbescorrection.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertEqual(self.data.courbestarage, [])
        self.assertEqual(len(self.data.courbescorrection), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_full_courbecorrection(self):
        """test full courbecorrection"""
        courbe = self.data.courbescorrection[0]
        self.check_courbecorrection_0(courbe)

    def test_courbecorrection_1point(self):
        """test courbecorrection 1 point"""
        courbe = self.data.courbescorrection[1]
        self.check_courbecorrection_1(courbe)

    def test_courbecorrection_0point(self):
        """test courbecorrection 0 point"""
        courbe = self.data.courbescorrection[2]
        self.check_courbecorrection_2(courbe)


# -- class TestFromXmlJaugeages --------------------------------------
class TestFromXmlJaugeages(CheckConversionJaugeages):
    """Xml Jaugeages class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'jaugeages.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertEqual(self.data.courbestarage, [])
        self.assertEqual(self.data.courbescorrection, [])
        self.assertNotEqual(self.data.jaugeages, [])

    def test_full_jaugeage(self):
        """test full courbecorrection"""
        jaug = self.data.jaugeages[0]
        self.check_jaug_0(jaug)

    def test_min_jaugeage(self):
        """test minimal jaugeage"""
        jaug = self.data.jaugeages[1]
        self.check_jaug_1(jaug)


# -- class TestFromXmlEvenements --------------------------------------
class TestFromXmlEvenements(CheckConversionEvenements):
    """Xml Evenements class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'evenements.xml'))

    def test_base(self):
        """Check keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertNotEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(self.data.seriesobselab, [])
        self.assertEqual(self.data.seriesobselabmeteo, [])
        self.assertEqual(self.data.courbestarage, [])
        self.assertEqual(self.data.courbescorrection, [])
        self.assertEqual(self.data.jaugeages, [])

    def test_full_evenement(self):
        """test full evenement"""
        evt = self.data.evenements[0]
        self.check_evt_0(evt)

    def test_min_evenement(self):
        """test minimal evenement"""
        evt = self.data.evenements[1]
        self.check_evt_1(evt)

    def test_meteo_evenement(self):
        """test evenement on site meteo"""
        evt = self.data.evenements[2]
        self.check_evt_2(evt)


# -- class TestFromXmlSitesHydro ----------------------------------------------
class TestFromXmlSitesHydros(CheckConversionSitesHydro):

    """FromXmlSitesHydro class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'siteshydro.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertNotEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(
            scenario.dtprod, datetime.datetime(2010, 2, 26, 12, 53, 10))
        self.assertEqual(scenario.emetteur.contact.code, '1069')
        self.assertEqual(scenario.emetteur.intervenant.code, '25')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_sitehydro_0(self):
        """Sitehydro 0 test."""
        site = self.data.siteshydro[0]
        self.check_sitehydro_0(site)
        self.assertEqual(site.code, 'A1984310')
        self.assertEqual(site.typesite, 'STANDARD')

    def test_sitehydro_1(self):
        """Sitehydro 1 test."""
        # check site
        site = self.data.siteshydro[1]
        self.check_sitehydro_1(site)

    def test_sitehydro_2(self):
        """Sitehydro 2 test."""
        # check site
        site = self.data.siteshydro[2]
        self.check_sitehydro_2(site)

    def test_sitehydro_3(self):
        """Sitehydro 3 test."""
        # check site
        site = self.data.siteshydro[3]
        self.check_sitehydro_3(site)

#    def test_error_1(self):
#        """Xml file with namespace error test."""
#        with self.assertRaises(ValueError):
#            # *([os.path.join('data', 'xml', '1.1', 'siteshydro.xml')])
#            Message.from_file(*([os.path.join(
#                'data', 'xml', '1.1', 'siteshydro_with_namespace.xml')]))


# -- class TestFromXmlSitesMeteo ----------------------------------------------
class TestFromXmlSitesMeteo(CheckConversionSitesMeteo):

    """FromXmlSitesMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'sitesmeteo.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertNotEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        # len
        self.assertEqual(len(self.data.sitesmeteo), 1)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 8, 5, 56))
        self.assertEqual(scenario.emetteur.contact.code, '26')
        self.assertEqual(scenario.emetteur.intervenant.code, '1520')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_sitemeteo_0(self):
        """Sitemeteo 0 test."""
        site = self.data.sitesmeteo[0]
        self.check_sitemeteo_0(site)


# -- class TestFromXmlSeuilsHydro ---------------------------------------------
class TestFromXmlSeuilsHydros(CheckConversionSeuilsHydro):

    """FromXmlSeuilsHydro class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'seuilshydro.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertNotEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(len(self.data.siteshydro), 0)
        self.assertEqual(len(self.data.seuilshydro), 6)

    def test_seuils_sitehydro_0(self):
        """Test seuils sitehydro 0."""
        # check the sitehydro
        seuil = self.data.seuilshydro[0]
        self.check_seuilhydro_0(seuil)

    def test_seuils_sitehydro_1(self):
        """Test seuils sitehydro 1."""
        # check the sitehydro

        seuil = self.data.seuilshydro[1]
        self.check_seuilhydro_1(seuil)

    def test_seuils_sitehydro_2(self):
        """Test seuils sitehydro 2."""
        seuils = [self.data.seuilshydro[2], self.data.seuilshydro[3]]
        # check the seuils
        for seuil in seuils:
            self.assertEqual(seuil.sitehydro.code, 'O0144020')
            self.assertEqual(len(seuil.valeurs), 0)

    def test_seuils_sitehydro_3(self):
        """Test seuils sitehydro 3."""
        seuil = self.data.seuilshydro[4]
        self.assertEqual(seuil.code, 999)
        self.assertIsNone(seuil.sitehydro)

    def test_seuils_sitehydro_4(self):
        """Seuil with values associated with sitehydro, station and capteurs"""
        seuil = self.data.seuilshydro[5]
        self.check_seuilhydro_5(seuil)


# -- class TestFromXmlSeuilsMeteo ---------------------------------------------
class TestFromXmlSeuilsMeteos(CheckConversionSeuilsMeteo):

    """FromXmlSeuilsMeteo class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'seuilsmeteo.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.simulations, [])
        self.assertEqual(len(self.data.siteshydro), 0)
        self.assertEqual(len(self.data.seuilshydro), 0)
        self.assertEqual(len(self.data.seuilsmeteo), 2)

    def test_seuils_sitehydro_0(self):
        """Test seuils sitehydro 0."""
        # check the sitehydro
        seuil = self.data.seuilsmeteo[0]
        self.check_seuilmeteo_0(seuil)

    def test_seuils_sitehydro_1(self):
        """Test seuils sitehydro 1."""
        seuil = self.data.seuilsmeteo[1]
        self.check_seuilmeteo_1(seuil)


# -- class TestFromXmlModelesPrevision ----------------------------------------
class TestFromXmlModelesPrevision(CheckConversionModelesPrevision):

    """FromXmlModelesPrevision class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'modelesprevision.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertNotEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])
        # len
        self.assertEqual(len(self.data.modelesprevision), 2)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2012, 6, 4, 9, 22, 44))
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.contact.code, '7')

    def test_modeleprevision_0(self):
        """Modeleprevision 0 test."""
        modele = self.data.modelesprevision[0]
        self.check_modele_0(modele)

    def test_modeleprevision_1(self):
        """Modeleprevision 1 test."""
        modele = self.data.modelesprevision[1]
        self.check_modele_1(modele)


# -- class TestFromXmlGradients ----------------------------------------
class TestFromXmlGradients(CheckConversionGradients):

    """FromXmlGradients class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'gradients.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

        self.assertEqual(len(self.data.seriesgradients), 3)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_gradients_0(self):
        """Gradients 0 test."""
        serie = self.data.seriesgradients[0]
        self.check_gradients_0(serie)

    def test_gradients_1(self):
        """Gradients 1 test."""
        serie = self.data.seriesgradients[1]
        self.check_gradients_1(serie)

    def test_gradients_2(self):
        """Gradients 2 test."""
        serie = self.data.seriesgradients[2]
        self.check_gradients_2(serie)


# -- class TestFromXmlValidsAnnee ----------------------------------------
class TestFromXmlValidsAnnee(CheckConversionValidsAnnee):

    """FromXmlValidsAnnee class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'validsanneehydro.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(self.data.simulations, [])

        self.assertEqual(self.data.seriesgradients, [])
        self.assertEqual(len(self.data.validsannee), 2)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 23, 55, 30))
        self.assertEqual(scenario.emetteur.contact.code, '1')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '1537')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_validsannee(self):
        """ValidsAnnee test."""
        self.check_validsannee(self.data.validsannee)


# -- class TestFromXmlSimulationss ----------------------------------------
class TestFromXmlSimulations(CheckConversionSimulations):

    """FromXmlSimulations class tests."""

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it."""
        self.data = Message.from_file(
            os.path.join('data', 'xml', '2', 'simulations.xml'))

    def test_base(self):
        """Check Keys test."""
        self.assertNotEqual(self.data.scenario, [])
        self.assertEqual(self.data.intervenants, [])
        self.assertEqual(self.data.siteshydro, [])
        self.assertEqual(self.data.sitesmeteo, [])
        self.assertEqual(self.data.seuilshydro, [])
        self.assertEqual(self.data.modelesprevision, [])
        self.assertEqual(self.data.evenements, [])
        self.assertEqual(self.data.serieshydro, [])
        self.assertEqual(self.data.seriesmeteo, [])
        self.assertEqual(len(self.data.simulations), 4)

        self.assertEqual(self.data.seriesgradients, [])
        self.assertIsNone(self.data.validsannee)

    def test_scenario(self):
        """Scenario test."""
        scenario = self.data.scenario
        self.assertEqual(scenario.code, 'hydrometrie')
        self.assertEqual(scenario.version, '2')
        self.assertEqual(scenario.nom, 'Echange de données hydrométriques')
        self.assertEqual(scenario.dtprod,
                         datetime.datetime(2010, 2, 26, 9, 30))
        self.assertEqual(scenario.emetteur.contact.code, '41')
        self.assertEqual(scenario.emetteur.intervenant.code, '1537')
        self.assertEqual(scenario.emetteur.intervenant.origine, 'SANDRE')
        self.assertEqual(scenario.destinataire.intervenant.code, '14')
        self.assertEqual(scenario.destinataire.intervenant.origine, 'SANDRE')

    def test_simulation_0(self):
        """Simulation 0 test."""
        simulation = self.data.simulations[0]
        # check simulation
        self.check_sim_0(simulation)

    def test_simulation_1(self):
        """Simulation 1 test."""
        simulation = self.data.simulations[1]
        # check simulation
        self.check_sim_1(simulation)

    def test_simulation_2(self):
        """Simulation 2 test."""
        simulation = self.data.simulations[2]
        # check simulation
        self.check_sim_2(simulation)

    def test_simulation_3(self):
        """Simulation 3 test."""
        simulation = self.data.simulations[3]
        # check simulation
        self.check_sim_3(simulation)
