# -*- coding: utf-8 -*-
"""Test program for xml.from_json.

To run all tests just type:
    python -m unittest test_conv_json_from_json

To run only a class test:
    python -m unittest test_conv_json_from_json.TestClass

To run only a specific test:
    python -m unittest test_conv_json_from_json.TestClass.test_method

"""

# -- imports ------------------------------------------------------------------
import json
import os
import unittest

from libhydro.conv.xml import Message


class TestToJson(unittest.TestCase):

    def compare(self, unit, version="2"):
        """Compare output with expecetd"""

        with open(os.path.join('data', 'json', version, '%s.json' % unit)) as fil:
            contents = fil.read()
        msg = Message.from_json(contents)
        jsonhydro = msg.to_json(bdhydro=True, ordered=True, version=version)
        with open(os.path.join('data', 'json', version, '%s_expected.json' % unit)) as fil:
            expected = json.dumps(json.loads(fil.read()))
        self.assertEqual(expected, jsonhydro)

    def compare_with_none(self, unit, version="2"):
        """Compare output with expecetd"""
        with open(os.path.join('data', 'json', version, '%s.json' % unit)) as fil:
            contents = fil.read()
        msg = Message.from_json(contents)
        jsonhydro = msg.to_json(bdhydro=True, ordered=True, version=version, with_none=True)
        filename = os.path.join('data', 'json', version, '%s_expected_with_none.json' % unit)
        with open(filename) as fil:
            expected = json.dumps(json.loads(fil.read()))
        self.assertEqual(expected, jsonhydro)

    def test_intervenants(self):
        """Intervenants to json test."""
        self.compare('intervenants')

    def test_sitesmeteo(self):
        """Sites météo to json test."""
        self.compare('sitesmeteo')

    def test_siteshydro(self):
        """Sites hydro to json test."""
        self.compare('siteshydro')

    def test_siteshydro_v21(self):
        """Sites hydro to json test Sandre V21."""
        self.compare('siteshydro', "2.1")

    def test_modelesprevision(self):
        """Modèles prévision to json test."""
        self.compare('modelesprevision')

    def test_seuilshydro(self):
        """Seuils hydro to json test."""
        self.compare('seuilshydro')

    def test_seuilsmeteo(self):
        """Seuils météo to json test."""
        self.compare('seuilsmeteo')

    def test_evenements(self):
        """Evenements to json test."""
        self.compare('evenements')

    def test_courbestarage(self):
        """Courbes de tarage to json test."""
        self.compare('courbestarage')

    def test_courbestarage_with_none(self):
        """Courbes de tarage to json test."""
        self.compare_with_none('courbestarage')

    def test_jaugeages(self):
        """Jaugeages to json test."""
        self.compare('jaugeages')

    def test_jaugeages_with_none(self):
        """Jaugeages to json test."""
        self.compare_with_none('jaugeages')

    def test_courbescorrection(self):
        """Courbes correction to json test."""
        self.compare('courbescorrection')

    def test_courbescorrection_with_none(self):
        """Courbes correction to json test."""
        self.compare_with_none('courbescorrection')

    def test_courbescorrection_paths_to_ignore(self):
        """Test paths to ignore."""
        with open(os.path.join('data', 'json', '2', 'courbescorrection.json')) as fil:
            contents = fil.read()
        msg = Message.from_json(contents)
        jsonhydro = msg.to_json(bdhydro=True, ordered=True, version='2', with_none=True)
        count = jsonhydro.count('DtMajCourbeCorrH')
        self.assertEqual(3, count)
        paths_to_ignore = [('Donnees', 'CourbesCorrH', 'DtMajCourbeCorrH')]
        json2 = msg.to_json(bdhydro=True, ordered=True, version='2', with_none=True,
                            paths_to_ignore=paths_to_ignore)

        count = json2.count('DtMajCourbeCorrH')
        self.assertEqual(0, count)

        # chemins inexistants
        paths_to_ignore = [
            ('Donnees', 'CourbesCorrH', 'DtMajCourbeCorrH2'),
            ('foo',),
            ('Donnees', 'CourbesTarage', 'DtMajCourbeCorrH2'),
        ]
        json3 = msg.to_json(bdhydro=True, ordered=True, version='2', with_none=True,
                            paths_to_ignore=paths_to_ignore)

        count = json3.count('DtMajCourbeCorrH')
        self.assertEqual(3, count)

    def test_serieshydro(self):
        """Hydro series to json test."""
        self.compare('serieshydro')

    def test_seriesmeteo(self):
        """Weather series to json test."""
        self.compare('seriesmeteo')

    def test_serieshydroelab(self):
        """Series obshydro elab to json test."""
        self.compare('obsselab')

    def test_seriesmeteoelab(self):
        """Series obsmeteo elab to json test."""
        self.compare('obsselabmeteo')

    def test_gradients(self):
        """Gradients to json test."""
        self.compare('gradients')

    def test_validsannee(self):
        """Gradients to json test."""
        self.compare('validsannee')

    def test_simulations(self):
        """Simulations to json test."""
        self.compare('simulations')
