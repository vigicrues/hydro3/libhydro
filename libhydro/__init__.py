# coding: utf-8
"""Meta-package libhydro.

Le meta-package libhydro est constitue des packages:
    # core qui contient les classes permettant de manipuler les objets
        hydrometriques
    # conv qui propose des convertisseurs de et vers differents formats
    # processing qui propose des traitements sur les donnéeshydro

"""

__version__ = '0.9.5'
__all__ = ['core', 'conv', 'processing']

# from .bdhydro import bdhydro
