# coding: utf-8
"""Module composant.

Ce module contient des elements de base de la librairie.

Il integre:
    # un gestionnaire d'erreurs (ERROR_HANDLERS)

la classe:
    # Rlist

les descripteurs:
    # Rlistproperty
    # Datefromeverything
    # Nomenclatureitem

les fonctions:
    # is_code_hydro()
    # is_code_insee()
    # __str__()

les fonctions speciales:
    # __eq__
    # __ne__

"""
# -- imports ------------------------------------------------------------------
import warnings as _warnings

import weakref as _weakref
import numpy as _numpy
import datetime as _datetime

from .nomenclature import NOMENCLATURE as _NOMENCLATURE


# -- strings ------------------------------------------------------------------
__version__ = '1.0.2'
__date__ = '2015-05-03'

# HISTORY
# V1.0 - 2014-12-17
#   move here full __eq__ and __ne__ methods
# V0.9 - 2014-07-16
#   add the error_handler, the Rlist and the Rlistproperty
#   split the module in 3 parts
# V0.8 - 2014-02-01
#   add and use descriptors
# V0.1 - 2013-11-06
#   first shot

# -- todos --------------------------------------------------------------------
# TODO - use regex for codes matching functions


# -- a basic errors handler ---------------------------------------------------
def _warn_handler(msg, *args, **kwargs):
    """Print msg on stderr."""
    _warnings.warn(msg)


def _strict_handler(msg, error):
    """Raise error(msg)."""
    raise error(msg)


ERROR_HANDLERS = {
    "ignore": lambda *args, **kwargs: None,  # 'ignore' returns None
    "warn": _warn_handler,  # 'warn' emit 'warn(msg)'
    "strict": _strict_handler}  # 'strict' raises 'error(msg)'


# -- class Rlist --------------------------------------------------------------
class Rlist(list):

    """Class Rlist.

    A class of restricted lists that can only contains items of type 'cls'.

    Read only property:
        cls (type)

    Methods:
        all list methods
        check()

    """

    def __init__(self, cls, iterable=None):
        """Initialisation.

        Arguments:
            cls (type) = the class of authorized items
            iterable (iterable, default None) = the list elements

        """
        # a read only property
        self._cls = cls

        # check and init
        if iterable is None:
            super(Rlist, self).__init__()
        else:
            self.checkiterable(iterable)
            super(Rlist, self).__init__(iterable)

    # -- read only property cls --
    @property
    def cls(self):
        """Get cls."""
        return self._cls

    # -- list overwritten methods --
    def append(self, y):
        """Append method."""
        self.checkiterable([y])
        super(Rlist, self).append(y)

    def extend(self, iterable):
        """Extend method."""
        self.checkiterable(iterable)
        super(Rlist, self).extend(iterable)

    def insert(self, i, y):
        """Insert method."""
        self.checkiterable([y])
        super(Rlist, self).insert(i, y)

    def __setitem__(self, i, y):
        """Setitem method."""
        # Used for slicing in python 3
        if isinstance(i, slice):
            # check type of each element in y
            self.checkiterable(y)
        else:
            # check type of y
            self.checkiterable([y])
        super(Rlist, self).__setitem__(i, y)

#    def __setslice__(self, i, j, y):
#        """Setitem method."""
#        self.checkiterable(y)
#        super(Rlist, self).__setslice__(i, j, y)

    # -- other methods --
    def checkiterable(self, iterable, errors='strict'):
        """Check iterable items type.

        Arguments:
            iterable (iterable)
            errors (str in 'strict' (default), 'ignore', 'warn')

        """
        # get the error handler
        try:
            error_handler = ERROR_HANDLERS[errors]
        except Exception:
            raise ValueError("unknown error handler name '%s'" % errors)
        # check
        if iterable is not None:
            for obj in iterable:
                if not isinstance(obj, self.cls):
                    error_handler(
                        msg="the object '%s' is not of %s" % (obj, self.cls),
                        error=TypeError)
                    return False
        # return
        return True


# reset docstrings to their original 'list' values
# python2 Rlist.append <unbound method Rlist.append>
# python2 Rlist.append __func__ <function append at 0x7fd89991a2a8>
# python3 Rlist.append <function __main__.Rlist.append>
# Rlist.append.__func__.__doc__ = list.append.__doc__
# Rlist.extend.__func__.__doc__ = list.extend.__doc__
# Rlist.insert.__func__.__doc__ = list.insert.__doc__
# Rlist.__setitem__.__func__.__doc__ = list.__setitem__.__doc__
# Rlist.__setslice__.__func__.__doc__ = list.__setslice__.__doc__
Rlist.append.__doc__ = list.append.__doc__
Rlist.extend.__doc__ = list.extend.__doc__
Rlist.insert.__doc__ = list.insert.__doc__
Rlist.__setitem__.__doc__ = list.__setitem__.__doc__
# __set_slice depretacted python2.6 use __setitem__ isntead
# Rlist.__setslice__.__doc__ = list.__setslice__.__doc__


# -- class  Rlistproperty -----------------------------------------------------
class Rlistproperty:

    """Class Rlistproperty

    A descriptor to deal with a list of restricted items.

    Raises a TypeError when value is not allowed.

    Properties:
        cls (class) = the type of the list items
        required (bool, defaut True) = wether or not instance's value can
            be None
        default =  a defautl value returned if the instance's value is not
            in the dictionnary. Should be unused if the property has been
            initialized.
        data (weakref.WeakKeyDictionary)

    """

    def __init__(self, cls, required=True, default=None):
        """Initialization.

        Args:
            required (bool, defaut True): wether or not instance's value can
                be None
            default: a default value returned if the instance's value is not
                in the dictionnary. Should be unused if the property has been
                initialized.

        """
        self.cls = cls
        self.required = bool(required)
        self.default = default
        self.data = _weakref.WeakKeyDictionary()

    def __get__(self, instance, owner):
        """Return instance value."""
        return self.data.get(instance, default=self.default)

    def __set__(self, instance, items):
        """Set the instance list."""
        # None case
        if items is None:
            if self.required:
                raise ValueError('a value other than None is required')
            rlist = Rlist(self.cls)

        # other cases
        else:
            rlist = Rlist(self.cls, items)

        # all is well
        self.data[instance] = rlist


# -- class Datefromeverything -------------------------------------------------
class Datefromeverything:

    """Class Datefromeverything.

    A descriptor to store a datetime.datetime property that can be initiated
    in different manners using numpy.datetime64 facilities.

    """

    def __init__(self, required=True, default=None):
        """Initialization.

        Args:
            required (bool, defaut True) = wether instance's value can
                be None or not

        """
        self.required = bool(required)
        self.default = default
        self.data = _weakref.WeakKeyDictionary()

    def __get__(self, instance, owner):
        """Return instance value."""
        return self.data.get(instance, default=self.default)

    def __set__(self, instance, value):
        """Set the datetime.datetime property.

        Args:
            value (numpy.datetime64 or string to make one
                datetime.datetime or iterable or dict to make it)

        String format: look at
          [http://docs.scipy.org/doc/numpy-dev/reference/arrays.datetime.html]

        """
        if self.required and (value is None):
            raise TypeError('a value other than None is required')
        if (value is not None) and not isinstance(value, _datetime.datetime) \
                and not isinstance(value, _numpy.datetime64):
            try:
                if isinstance(value, dict):
                    value = _datetime.datetime(**value)
                elif isinstance(value, str):
                    value = _numpy.datetime64(value, 's')
                else:  # can be an iterable for datetime.datetime
                    value = _datetime.datetime(*value)
            except (ValueError, TypeError, AttributeError):
                raise ValueError(
                    'could not convert object to datetime.datetime')

        # all is well
        if isinstance(value, _numpy.datetime64):
            value = value.item()
        self.data[instance] = value


# -- class Nomenclatureitem ---------------------------------------------------
class Nomenclatureitem:

    """Class Nomenclatureitem.

    A descriptor to deal with 'in nomenclature.NOMENCLATURES' properties.

    Should raise only a ValueError when value is not allowed (even with
    the None case).

    Properties:
        nomenclature (int) = the nomenclature ref
        valuetype (type) = a function to cast values to the nomenclature's
            items type
        strict (bool, default True) = wether or not the instance value has
            to be in the nomenclature items
        required (bool, defaut True) = wether or not instance's value can
            be None
        default =  a defautl value returned if the instance's value is not
            in the dictionnary. Should be unused if the property has been
            initialized.
        data (weakref.WeakKeyDictionary)

    """

    def __init__(self, nomenclature, strict=True, required=True, default=None):
        """Initialization.

        Args:
            nomenclature (int) = the nomenclature ref
            strict (bool, default True) = wether or not the instance value has
                to be in the nomenclature items
            required (bool, defaut True) = wether or not instance's value can
                be None
            default =  a defautl value returned if the instance's value is not
                in the dictionnary. Should be unused if the property has been
                initialized.

        """
        self.nomenclature = int(nomenclature)
        if self.nomenclature not in _NOMENCLATURE:
            raise ValueError('unknown nomenclature')
        self.valuetype = type(list(_NOMENCLATURE[self.nomenclature].keys())[0])
        self.strict = bool(strict)
        self.required = bool(required)
        self.default = default
        self.data = _weakref.WeakKeyDictionary()

    def __get__(self, instance, owner):
        """Return instance value."""
        return self.data.get(instance, default=self.default)

    def __set__(self, instance, value):
        """Set the 'in nomenclature' property."""
        # None case
        if value is None:
            if self.required:
                raise ValueError('a value other than None is required')

        # other cases
        else:
            value = self.valuetype(value)
            if self.strict and value not in _NOMENCLATURE[self.nomenclature]:
                raise ValueError(
                    'value should be in nomenclature %i' % self.nomenclature)

        # all is well
        self.data[instance] = value


# -- functions ----------------------------------------------------------------
def is_code_hydro(code, length=8, errors='ignore'):
    """Return wether or not code is a valid code hydro as a bool.

    Arguments:
       code (string)
       length (int, default 8): code size
       errors ('ignore' (default), 'strict'): 'strict' raises an exception

    """
    try:
        # (length) chars length
        if len(code) != length:
            raise ValueError(
                'code hydro {0} must be {1} chars long'.format(code, length))

        # first char must be in [A-Z] or in [0-9]
        if not (code[0].isupper() or code[0].isdigit()):
            raise ValueError('code hydro first char must be upper or a digit')

        # [1:-1] digits
        if not code[1:-1].isdigit():
            raise ValueError(
                'code hydro chars except first and last must be digits')

        # digit or upper last char
        if not (code[-1].isdigit() or code[-1].isupper()):
            raise ValueError('code hydro last char must be digit or upper')

        # all is well
        return True

    except Exception as err:
        if errors not in ('ignore', 'strict'):
            raise ValueError("unknown error handler name '%s'" % errors)
        # ERROR_HANDLERS[errors](msg=err.message, error=type(err))
        ERROR_HANDLERS[errors](msg=str(err), error=type(err))


def is_code_insee(code, length=5, errors='ignore'):
    """Return whether or not code is a valid INSEE code as a bool.

    Un code INSEE de commune est construit sur 5 caracteres. Pour les
    communes de metropole, les deux premiers caracteres correspondent
    au numero du departement de la commune. Pour les DOM, les trois
    premiers caracteres correspondent au numero du departement de la
    commune. Il est a noter que ce code est au format caractere afin de
    gerer les communes de la Corse (2A et 2B).

    Un code INSEE de site meteorologique est construit sur 9 caracteres.
    Les 6 premiers forment le code INSEE de la commune de localisation,
    prefixe d'un zero significatif, ou bien un code de pays pour les
    postes hors du territoire national. Les 3 derniers caracteres sont
    un numero d'ordre du site meteorologique.

    Arguments:
       code (str): code
       length (int): expected length
       errors ('ignore' (default), 'strict'): 'strict' raises an exception

    """
    # pre-condition
    if length not in (5, 9):
        raise ValueError('length must be 5 or 9')

    try:
        # prepare
        code = str(code)

        # chars length
        if len(code) != length:
            raise ValueError('INSEE code must be %i chars long' % length)

        # code commune must be all digit or 2(A|B)xxx
        if not code.isdigit():
            if length == 9:
                start = 1
            else:
                start = 0
            if not (code[start:start + 2] in ('2A', '2B') and
                    code[start + 2:].isdigit()):
                raise ValueError('illegal char in INSEE code')

        # all is well
        return True

    except Exception as err:
        if errors not in ('ignore', 'strict'):
            raise ValueError("unknown error handler name '%s'" % errors)
        # ERROR_HANDLERS[errors](msg=err.message, error=type(err))
        ERROR_HANDLERS[errors](msg=str(err), error=type(err))


def __eq__(self, other, attrs=None, ignore=None, lazzy=False):
    """Equal elaborate function.

    Arguments:
        self, other
        attrs (iterable of strings, default to self.__class__.__all__attrs__ or
            __self.__dict__.keys() = the attrs to compare
        ignore (iterable of strings, default None): attrs to ignore
        lazzy (bool, default False): if True does not test an attribute
            whose counterpart is None

    NB: functool.partial could be smarter than a private class variable to
    fix the default attrs list, but it doesn't work with 'self'.

    """
    # quick identity test
    if self is other:
        return True
    # set the final attrs list
    if not attrs:
        attrs = getattr(
            self.__class__, '__all__attrs__', list(self.__dict__.keys()))
    if ignore:
        for attr in ignore:
            # we make attrs mutable or a copy before removing elements
            attrs = list(attrs)
            try:
                attrs.remove(attr)
            except ValueError:
                raise AttributeError(
                    "'{}' object has no attribute '{}'".format(
                        attr, self.__class__.__name__))
    # action !
    for attr in attrs:
        first = getattr(self, attr)
        second = getattr(other, attr, False)  # never raise
        if lazzy and (first is None or second is None):
            continue
        try:
            # base case - works with numpy.ndarrays of size 1
            if not bool(first == second):
                # if not first == second:
                return False
        except Exception:
            # # comparison for ndarrays of size > 1 and pandas objects
            try:
                if not bool((first == second).all().all()):
                    return False
            except BaseException:
                # here everything failed
                return False
    return True


def __ne__(self, other, attrs=None, ignore=None, lazzy=False):
    if attrs is None:
        attrs = []
    if ignore is None:
        ignore = []
    return not self.__eq__(
        other=other, attrs=attrs, ignore=ignore, lazzy=lazzy)


def __hash__(self):
    return id(self)


class PasDeTemps:
    """Class PasDeTemps réprsentant un pas de temps.

    Le pas de temps peut être exprimé en minutes, heures ou jours.

    Properties:
        duree (datetime.timedelta) duree du pas de temps
        unite (int): unite du pas de temps
                     parmi [PasDeTemps.SECONDES, PAsDeTemps.MINUTES,
                     PasDeTemps.HEURES, PasDeTemps.JOURS]

    """

    SECONDES = 0
    MINUTES = 1
    HEURES = 2
    JOURS = 3

    def __init__(self, duree=None, unite=1):
        """Initialisation.

        Arguments:
            duree (int or datetime.timedelta): duree du pas de temps
            unite (int): unite du pas de temps (défaut PasDeTemps.MINUTES)
                parmi [PasDeTemps.SECONDES, PasDeTemps.MINUTES,
                       PasDeTemps.HEURES, PasDeTemps.JOURS]
        """
        self._unite = None
        self.unite = unite
        self._duree = None
        self.duree = duree

    # -- property duree --
    @property
    def duree(self):
        """Return duree."""
        return self._duree

    @duree.setter
    def duree(self, duree):
        """Set duree."""
        if isinstance(duree, _datetime.timedelta):
            self._duree = duree
            return
        try:
            duree = int(duree)
            if duree < 0:
                raise ValueError('duree must be positive')
            if self.unite == PasDeTemps.MINUTES:
                self._duree = _datetime.timedelta(minutes=duree)
            elif self.unite == PasDeTemps.HEURES:
                self._duree = _datetime.timedelta(hours=duree)
            elif self.unite == PasDeTemps.JOURS:
                self._duree = _datetime.timedelta(days=duree)
            else:
                self._duree = _datetime.timedelta(seconds=duree)
        except Exception:
            raise

    # -- property entite --
    @property
    def unite(self):
        """Return unite."""
        return self._unite

    @unite.setter
    def unite(self, unite):
        """Set unite."""
        try:
            unite = int(unite)
            if unite not in [PasDeTemps.SECONDES,
                             PasDeTemps.MINUTES,
                             PasDeTemps.HEURES,
                             PasDeTemps.JOURS]:
                raise ValueError('incorrect unite')
            self._unite = unite
        except Exception:
            raise

    def to_int(self):
        """conversion in integer with good units"""
        if self.duree is None:
            return None
        if self.unite == PasDeTemps.MINUTES:
            return int(self.duree.total_seconds() / 60)
        elif self.unite == PasDeTemps.HEURES:
            return int(self.duree.total_seconds() / 3600)
        elif self.unite == PasDeTemps.JOURS:
            return self.duree.days
        else:
            return int(self.duree.total_seconds())

    def __str__(self):
        """Return str representation."""
        if self.unite == PasDeTemps.MINUTES:
            unite = 'm'
        elif self.unite == PasDeTemps.HEURES:
            unite = 'h'
        elif self.unite == PasDeTemps.JOURS:
            unite = 'j'
        else:
            unite = 's'
        duree = self.to_int()
        return '{} {}'.format(duree, unite)
