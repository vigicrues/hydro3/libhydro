# coding: utf-8
"""Module simulation.

Ce module contient les classes:
   # PrevisionDeterministe et PrevisionsDeterministes
   # PrevisionPrb et PrevisionsPrb
   # PrevEnsemble et PrevsEnsemble
   # PrevisionTendance et PrevisionsTendance
   # PrevEvol
   # ScenarioSimul
   # Simulation

La Simulation est le conteneur de reference pour les previsions hydrometriques.
Les previsions y sont contenues dans differents attributs suivant leur type,
sous la forme de pandas.Series, a simple ou double index.

"""

# On peut aussi utiliser directement les classes de la librairie Pandas, les
# Series ou les DataFrame.
#
# Exemple pour instancier une Series:
#     datas = pandas.Series(
#         data = [100, 110, 120],
#         index = [
#             numpy.datetime64('2012-05 01:00', 's'),
#             numpy.datetime64('2012-05 02:00', 's'),
#             numpy.datetime64('2012-05 03:00', 's')
#         ]
#         dtype = None,
#         name='previsions de debit'
# )
#
# Exemple pour instancier un DataFrame:
#     hauteurs = pandas.DataFrame({
#         'H2354310': Series_de_hauteurs_1,
#         'H4238907': Series_de_hauteurs_2,
#         ...
#     })

# -- imports ------------------------------------------------------------------
import numpy as _numpy
import pandas as _pandas

from . import _composant
from . import sitehydro as _sitehydro, modeleprevision as _modeleprevision
from .nomenclature import NOMENCLATURE as _NOMENCLATURE
from libhydro.core import intervenant as _intervenant


# -- strings ------------------------------------------------------------------
__version__ = '0.8.1'
__date__ = '2022-10-04'

# HISTORY
# V0.8 - 2017-05-17
#  implement classes from the new model
# V0.7 - 2014-03-02
#   use descriptors
# V0.1 - 2013-08-07
#   first shot

# -- todos --------------------------------------------------------------------
# FIXME - integrity checks entite / grandeur
#     grandeur is a descriptor, it needs a callback
#     ADMIT_SIMULATION = {
#         Sitehydro: {'H': False, 'Q': True},
#         Station: {'H': True, 'Q': False},
#         Capteur: {'H': False, 'Q':False}
#     }
# def _admit_simulation(self, grandeur):
#      # be careful with  self._class_ and inheritance
#      return ADMIT_SIMULATION[self.__class][grandeur]

# TODO - add a sort argument/method ?


# -- class PrevisionDeterministe ----------------------------------------------
class PrevisionDeterministe(_numpy.ndarray):

    """Classe PrevisionEnsemble.

    Classe pour manipuler une prevision déterministe.

    Subclasse de numpy.array('dte', 'res', 'incertdt').
    Les elements étant du type DTYPE.

    Date et resultat sont obligatoires, l'incertitude temporelle vaut par défaut 0.

    Proprietes:
        dte (numpy.datetime64) = date UTC de la prevision au format
            ISO 8601, arrondie a la seconde. A l'initialisation par une string,
            si le fuseau horaire n'est pas precise, la date est consideree en
            heure locale. Pour forcer la sasie d'une date UTC utiliser
            le fuseau +00:
                numpy.datetime64('2000-01-01T09:28:00+00')
        res (numpy.float64) = resultat
        incertdte (numpy.int) = incertitude temporelle en minutes

    """

    DTYPE = _numpy.dtype([
        (str('dte'), _numpy.datetime64(None, str('s'))),
        (str('res'), _numpy.float64),
        (str('incertdte'), _numpy.int16)])

    def __new__(cls, dte, res, incertdte=0):
        if not isinstance(dte, _numpy.datetime64):
            dte = _numpy.datetime64(dte, 's')
        obj = _numpy.array((dte, res, incertdte),
                           dtype=PrevisionDeterministe.DTYPE).view(cls)
        return obj

    def __str__(self):
        """Return str representation."""
        return '{0} pour le {1} à {2} UTC (incertitude de {3} minutes)'.format(
            self['res'].item(),
            *self['dte'].item().isoformat().split('T'),
            self['incertdte']
        )


# -- class PrevisionsDeterministes --------------------------------------------
class PrevisionsDeterministes(_pandas.Series):

    """Classe PrevisionsDeterministes.

    Classe pour manipuler un jeux de previsions déterministes,
    sous la forme d'une Series pandas avec comme index la date de la prévision

    Illustration d'une Series pandas de previsions pour 3 dates et avec 2 jeux
    de probabilite:
        dte                  res incertdte
        1972-10-01 10:00:00  33  4
        1972-10-01 11:00:00  44  -5
        1972-10-01 12:00:00  50  1


    Pour filtrer la serie de resultats de meme probabilite, par exemple 50%,
    entre 2 dates:
        previsions[:,50]['2013-01':'2013-01-23 01:00']
    ou a une date precise:
        previsions['2013-01-23 01:00'][50]

    On peut slicer une serie mais il faut que l'index soit ordonne par la
    colonne utilisee:
        # trier par la date
        ordered_prev = previsions.sortlevel(0)
        # slicer
        ordered_prev['2013-01-23 00:00':'2013-01-23 10:00']
        # si l'index n'est pas correctement trie on leve une exception...
        ordered_prev = previsions.sortlevel(1)
        ordered_prev['2013-01-23 00:00':'2013-01-23- 10:00']
        >> KeyError: 'MultiIndex lexsort depth 0, key was length 1'

    Pour agreger 2 series de previsions:
        previsions.append(other_previsions)

    """

    def __new__(cls, *previsions):
        """Constructeur.

        Arguments:
            previsions (un nombre quelconque de PrevisionDeterministe)

        Exemples:
            prv = Previsions(prv1)  # une seule Prevision
            prv = Previsions(prv1, prv2, ..., prvn)  # n Prevision
            prv = Previsions(*previsions)  #  une liste de Prevision

        """

        # prepare a list of previsions
        prvs = []
        try:
            for prv in previsions:
                if not isinstance(prv, PrevisionDeterministe):
                    raise TypeError(
                        '{} is not a deterministic Prevision'.format(prv))
                # python 3 avois error provide tuple instead array
                # dtype is specified after
                # prvs.append(prv)
                prvs.append(prv.tolist())

        except Exception:
            raise

        # prepare a tmp numpy.array
        array = _numpy.array(object=prvs, dtype=PrevisionDeterministe.DTYPE)

        # make index
        index = _pandas.Index(array['dte'], name='dte')

        # get the pandas.Series
        obj = _pandas.DataFrame(
            data=array[list(array.dtype.names[1:])],
            index=index
        )

        # return
        # TODO - we can't subclass the DataFrame object
        # return obj.view(cls)
        return obj


# -- class PrevisionPrb -------------------------------------------------------
class PrevisionPrb(_numpy.ndarray):

    """Classe PrevisionPrb.

    Classe pour manipuler une prevision probabiliste.

    Subclasse de numpy.array('dte', 'res', 'prb'), les elements etant du
    type DTYPE.

    Date et resultat sont obligatoires, la probabilite vaut 50 par defaut.

    Proprietes:
        dte (numpy.datetime64) = date UTC de la prevision au format
            ISO 8601, arrondie a la seconde. A l'initialisation par une string,
            si le fuseau horaire n'est pas precise, la date est consideree en
            heure locale. Pour forcer la sasie d'une date UTC utiliser
            le fuseau +00:
                numpy.datetime64('2000-01-01T09:28:00+00')
        res (numpy.float64) = resultat
        prb (numpy.int entre 0 et 100, defaut 50) = probabilite du resultat

    """

    DTYPE = _numpy.dtype([
        (str('dte'), _numpy.datetime64(None, str('s'))),
        (str('res'), _numpy.float64), (str('prb'), _numpy.int8)])

    def __new__(cls, dte, res, prb=50):
        if not isinstance(dte, _numpy.datetime64):
            dte = _numpy.datetime64(dte, 's')
        try:
            prb = int(prb)
            if (prb < 0) or (prb > 100):
                raise ValueError('probabilite incorrect')
        except Exception:
            raise
        obj = _numpy.array((dte, res, prb), dtype=PrevisionPrb.DTYPE).view(cls)
        return obj

    def __str__(self):
        """Return str representation."""
        return '{0} avec une probabilite de {1}% pour le {2} a {3} UTC'.format(
            self['res'].item(), self['prb'].item(),
            *self['dte'].item().isoformat().split('T'))


# -- class PrevisionsPrb ------------------------------------------------------
class PrevisionsPrb(_pandas.Series):

    """Classe PrevisionsPrb.

    Classe pour manipuler un jeux de previsions probabilistes, sous la forme d'une Series
    pandas avec un double index, le premier etant la date du resultat, le
    second sa probabilite.

    Illustration d'une Series pandas de previsions pour 3 dates et avec 2 jeux
    de probabilite:
        dte                  prb
        1972-10-01 10:00:00  50     33
                             40     44
        1972-10-01 11:00:00  50     35
                             40     45
        1972-10-01 12:00:00  50     55
                             40     60
        Name: res, dtype: float64

    Se reporter a la documentation de la classe PrevisionPrb pour l'utilisation
    du parametre prb.

    Pour filtrer la serie de resultats de meme probabilite, par exemple 50%,
    entre 2 dates:
        previsions[:,50]['2013-01':'2013-01-23 01:00']
    ou a une date precise:
        previsions['2013-01-23 01:00'][50]

    On peut slicer une serie mais il faut que l'index soit ordonne par la
    colonne utilisee:
        # trier par la date
        ordered_prev = previsions.sortlevel(0)
        # slicer
        ordered_prev['2013-01-23 00:00':'2013-01-23 10:00']
        # si l'index n'est pas correctement trie on leve une exception...
        ordered_prev = previsions.sortlevel(1)
        ordered_prev['2013-01-23 00:00':'2013-01-23- 10:00']
        >> KeyError: 'MultiIndex lexsort depth 0, key was length 1'

    Pour agreger 2 series de previsions:
        previsions.append(other_previsions)

    """

    def __new__(cls, *previsions):
        """Constructeur.

        Arguments:
            previsions (un nombre quelconque de PrevisionPrb)

        Exemples:
            prv = Previsions(prv1)  # une seule Prevision
            prv = Previsions(prv1, prv2, ..., prvn)  # n Prevision
            prv = Previsions(*previsions)  #  une liste de Prevision

        """
        # prepare a list of previsions
        prvs = []
        try:
            for prv in previsions:
                if not isinstance(prv, PrevisionPrb):
                    raise TypeError(
                        '{} is not a probabilist Prevision'.format(prv))
                # TODO - inefficient, need a refactoring
                # python 3 avoid error provide tuple instead array
                # dtype is specified after
                # prvs.append(prv)
                prvs.append(prv.tolist())

        except Exception:
            raise

        # prepare a tmp numpy.array
        # python 3 error: ValueError: cannot include dtype 'M' in a buffer
        # to avoid this error provide tuple instead of _numpy.array
        # and dtype
        # array = _numpy.array(object=prvs)
        array = _numpy.array(object=prvs, dtype=PrevisionPrb.DTYPE)

        # make index
        index = _pandas.MultiIndex.from_tuples(
            list(zip(array['dte'], array['prb'])), names=['dte', 'prb'])

        # get the pandas.Series
        obj = _pandas.Series(data=array['res'], index=index, name='res')

        # return
        # TODO - we can't subclass the DataFrame object
        # return obj.view(cls)
        return obj


# -- class PrevEnsemble -------------------------------------------------------
class PrevEnsemble(_numpy.ndarray):

    """Classe PrevEnsemble.

    Classe pour manipuler une prevision d'ensemble.

    Proprietes:
        dte (numpy.datetime64) = date UTC de la prevision au format
            ISO 8601, arrondie a la seconde. A l'initialisation par une string,
            si le fuseau horaire n'est pas precise, la date est consideree en
            heure locale. Pour forcer la sasie d'une date UTC utiliser
            le fuseau +00:
                numpy.datetime64('2000-01-01T09:28:00+00')
        lb = membre
        res (numpy.float64) = resultat
        poids

    """

    DTYPE = _numpy.dtype([
        ('dte', _numpy.datetime64(None, str('s'))),
        ('lb', object),
        ('res', _numpy.float64),
        ('poids', _numpy.int32),
        ])

    def __new__(cls, dte, lb, res, poids=1):
        if not isinstance(dte, _numpy.datetime64):
            dte = _numpy.datetime64(dte, 's')
        obj = _numpy.array(
            (dte, lb, res, poids),
            dtype=PrevEnsemble.DTYPE
        ).view(cls)
        if int(poids) < 1:
            raise ValueError('poids must be greater than or equal to 1')
        return obj

    def __str__(self):
        """Return str representation."""
        return '{0} du membre {1} de poids {2} pour le {3} à {4} UTC'.format(
            self['res'].item(), self['lb'].item(), self['poids'].item(),
            *self['dte'].item().isoformat().split('T'))


# -- class PrevsEnsemble ------------------------------------------------------
class PrevsEnsemble(_pandas.DataFrame):

    """Classe PrevsEnsemble.

    Classe pour manipuler un jeux de previsions d'ensemble.

    """

    def __new__(cls, *previsions):
        """Constructeur.

        Arguments:
            previsions (un nombre quelconque de PrevEnsemble)

        """

        # prepare a list of previsions
        prvs = []
        try:
            for prv in previsions:
                if not isinstance(prv, PrevEnsemble):
                    raise TypeError(
                        '{} is not an ensemble Prevision'.format(prv))
                # TODO - inefficient, need a refactoring
                # python 3 avois error provide tuple instead array
                # dtype is specified after
                # prvs.append(prv)
                prvs.append(prv.tolist())

        except Exception:
            raise

        # prepare a tmp numpy.array
        # python 3 error: ValueError: cannot include dtype 'M' in a buffer
        # to avoid this error provide tuple instead of _numpy.array
        # and dtype
        # array = _numpy.array(object=prvs)
        array = _numpy.array(object=prvs, dtype=PrevEnsemble.DTYPE)

        # make index
        index = _pandas.MultiIndex.from_tuples(
            list(zip(array['dte'], array['lb'])), names=['dte', 'lb'])

        # get the pandas.DataFrame
        obj = _pandas.DataFrame(
            data=array[list(array.dtype.names[2:])],
            index=index
        )

        # return
        # TODO - we can't subclass the DataFrame object
        # return obj.view(cls)
        return obj


# -- class PrevisionTendance --------------------------------------------------
class PrevisionTendance(_numpy.ndarray):

    """Classe PrevisionTendance.

    Classe pour manipuler une prevision de tendance elementaire.

    Subclasse de numpy.array('dte', 'res', 'tend'), les elements etant du
    type DTYPE.

    Date et resultat sont obligatoires. La tendance peut prendre la valeur
    'min', 'moy' (valeur par defaut) ou 'max'

    Proprietes:
        dte (numpy.datetime64) = date UTC de la prevision au format
            ISO 8601, arrondie a la seconde. A l'initialisation par une string,
            si le fuseau horaire n'est pas precise, la date est consideree en
            heure locale. Pour forcer la sasie d'une date UTC utiliser
            le fuseau +00:
                numpy.datetime64('2000-01-01T09:28:00+00')
        res (numpy.float64) = resultat
        tend (numpy.unicode 3 caractères parmi 'min', 'moy' ou 'max') =
            tendance de la prevision
        incertdte

    """

    DTYPE = _numpy.dtype([
        (str('dte'), _numpy.datetime64(None, str('s'))),
        (str('tend'), _numpy.unicode_, 3),
        (str('res'), _numpy.float64),
        (str('incertdte'), _numpy.int16)
        ])

    def __new__(cls, dte, res, tend='moy', incertdte=0):
        if not isinstance(dte, _numpy.datetime64):
            dte = _numpy.datetime64(dte, 's')
        try:
            tend = str(tend)
            if tend not in ('min', 'max', 'moy'):
                raise ValueError('tendance incorrect {}'.format(tend))
        except Exception:
            raise
        obj = _numpy.array(
            (dte, tend, res, incertdte),
            dtype=PrevisionTendance.DTYPE
        ).view(cls)
        return obj

    def __str__(self):
        """Return str representation."""
        return '{0} de tendance {1} pour le {2} a {3} UTC'.format(
            self['res'].item(), self['tend'].item(),
            *self['dte'].item().isoformat().split('T'))


# -- class PrevisionsTendance -------------------------------------------------
class PrevisionsTendance(_pandas.Series):

    """Classe PrevisionsTendance.

    Classe pour manipuler un jeux de previsions de tendande, sous la forme
    d'une Series pandas avec un double index, le premier etant la date du
    resultat, le second la tendance ('moy', 'min' ou 'max').

    Illustration d'une Series pandas de previsions pour 3 dates et avec 2 jeux
    de tendances:
        dte                  tend
        1972-10-01 10:00:00  moy     33
                             max     44
        1972-10-01 11:00:00  min     35
                             moy     45
                             max     55
        1972-10-01 12:00:00  min     55
                             moy     60
        Name: res, dtype: float64

    Se reporter a la documentation de la classe PrevisionTendance pour
    l'utilisation du parametre tend.

    Pour filtrer la serie de resultats de meme tendance, par exemple 'moy',
    entre 2 dates:
        previsions[:,'moy']['2013-01':'2013-01-23 01:00']
    ou a une date precise:
        previsions['2013-01-23 01:00']['moy']

    On peut slicer une serie mais il faut que l'index soit ordonne par la
    colonne utilisee:
        # trier par la date
        ordered_prev = previsions.sortlevel(0)
        # slicer
        ordered_prev['2013-01-23 00:00':'2013-01-23 10:00']
        # si l'index n'est pas correctement trie on leve une exception...
        ordered_prev = previsions.sortlevel(1)
        ordered_prev['2013-01-23 00:00':'2013-01-23- 10:00']
        >> KeyError: 'MultiIndex lexsort depth 0, key was length 1'

    Pour agreger 2 series de previsions:
        previsions.append(other_previsions)

    """

    def __new__(cls, *previsions):
        """Constructeur.

        Arguments:
            previsions (un nombre quelconque de PrevisionTendance)

        Exemples:
            prv = Previsions(prv1)  # une seule Prevision
            prv = Previsions(prv1, prv2, ..., prvn)  # n PrevisionTendance
            prv = Previsions(*previsions)  #  une liste de PrevisionTendance

        """

        # prepare a list of previsions
        prvs = []
        try:
            for prv in previsions:
                if not isinstance(prv, PrevisionTendance):
                    raise TypeError(
                        '{} is not a tendency Prevision'.format(prv))
                # TODO - inefficient, need a refactoring
                # python 3 avois error provide tuple instead array
                # dtype is specified after
                # prvs.append(prv)
                prvs.append(prv.tolist())

        except Exception:
            raise

        # prepare a tmp numpy.array
        # python 3 error: ValueError: cannot include dtype 'M' in a buffer
        # to avoid this error provide tuple instead of _numpy.array
        # and dtype
        # array = _numpy.array(object=prvs)
        array = _numpy.array(object=prvs, dtype=PrevisionTendance.DTYPE)
        # make index
        index = _pandas.MultiIndex.from_tuples(
            list(zip(array['dte'], array['tend'])), names=['dte', 'tend'])

        # get the pandas.Series
        obj = _pandas.DataFrame(
            data=array[list(array.dtype.names[2:])],
            index=index
        )

        # obj = _pandas.Series(data=array['res'], index=index, name='res')

        # return
        # TODO - can't subclass the DataFRame object
        # return obj.view(cls)
        return obj


# -- class PrevEvol -----------------------------------------------------------
class PrevEvol:
    """Classe PrevEvol

    Proprietes:
        evol (int): type de prévision d'évolution
        dtdeb (datetime.datetime): date de début de la prévision d'évolution
        incertdte (int | None): incertitude sur la date en minutes

    """

    evol = _composant.Nomenclatureitem(nomenclature=927)
    dtdeb = _composant.Datefromeverything(required=True)

    def __init__(self, evol, dtdeb, incertdte):
        """Initialisation.

        Arguments:
            evol (int): type de prévision d'évolution
            dtdeb (datetime.datetime): date de début de la prévision d'évolution
            incertdte (int | None): incertitude sur la date en minutes

        """
        self.evol = evol
        self.dtdeb = dtdeb
        self._incertdte = None
        self.incertdte = incertdte

    @property
    def incertdte(self):
        """Return incertdte."""
        return self._incertdte

    @incertdte.setter
    def incertdte(self, incertdte):
        """Set incertdte."""
        if incertdte is None:
            self._incertdte = None
            return
        try:
            incertdte = int(incertdte)
        except BaseException:
            raise TypeError('incertdte must be an integer')
        self._incertdte = incertdte

    def __str__(self):
        if self.incertdte is None:
            incertdte = '<sans incertitude de date>'
        else:
            incertdte = f'incertitude de {self.incertdte} minute(s)'
        return '{} à partir de {} ({})'.format(
            _NOMENCLATURE[927][self.evol].lower(),
            self.dtdeb,
            incertdte
        )


# -- class ScenarioSimul ------------------------------------------------------
class ScenarioSimul:
    """Classe ScenarioSimul

    Classe pour manipuler les scénarios de simulation

    Propriétés:
        libelle (str): Libellé du scénario
        descriptif (str | None): Descriptif du scénario

    """

    def __init__(self, libelle, descriptif=None):
        """Initialisation

        Arguments:
            libelle (str): libellé du scénario
            descriptif (str | None): descriptif du scénario

        """
        self.libelle = str(libelle)
        self.descriptif = str(descriptif) if descriptif is not None else None

    def __str__(self):
        """Return str representation."""
        return 'Scénario {}\n{}'.format(
            self.libelle,
            self.descriptif if self.descriptif is not None else '<sans descriptif>'
        )


# -- class Simulation ---------------------------------------------------------
class Simulation:

    """Classe Simulation.

    Classe pour manipuler les simulations hydrauliques ou hydrologiques.

    Proprietes:
        entite (Sitehydro ou Station)
        dtprod (datetime.datetime) = date de production
        grandeur (char in NOMENCLATURE[509]) = H ou Q
        code (int): code de la simulation
        qualite (0 < int < 100) = indice de qualite
        dtfinvalidite (datetime.datetime): date de fin de validite
        dtdeb (datetime.datetime): date de début
        dtfin (datetime.datetime): date de fin
        dtbase (datetime.datetime): temps de base de la simulation
        dtderobs (datetime.datetime): date de la dernière observation
        modecalcul (int): mode de calcul (nomenclature 924)
        statut (int in NOMENCLATURE[516]) = brute ou critiquee
        sysalti (int): système altimétrique (nomenclature 76)
        contexte (str): contexte
        commentaire (texte)
        cmntprive (str): commentaire privé
        mode: mode de la simulation (NOMENCLATURE[925])
        modeleprevision (Modeleprevision): modèle de prévision
        contact (intervenant.Contact) : contact
        intervenant (intervenant.Intervenant): intervenant
        publication (int): type de publication (nomenclature 874)
        previsions_tend (PrevisionsTendance): prévisions de tendance
        previsions_det (PrevisionsDeterministes): prévisions déterministes
        previsions_prb (PrevisionsPrb) : prévisions probabilistes
        prevs_ensemble (PrevsEnsemble): Prévisions d'ensemble
        prevs_evol (list[PrevEvol]): prévisions d'évolution
        scenario (ScenarioSimul | None): scénario de simulation

    """

    # FIXME - add a callback to grandeur to check inconsistency with entite
    sysalti = _composant.Nomenclatureitem(nomenclature=76, required=False)
    grandeur = _composant.Nomenclatureitem(nomenclature=509, required=False)
    statut = _composant.Nomenclatureitem(nomenclature=516)
    publication = _composant.Nomenclatureitem(nomenclature=874, required=False)
    modecalcul = _composant.Nomenclatureitem(nomenclature=924, required=False)
    mode = _composant.Nomenclatureitem(nomenclature=925, required=False)
    dtprod = _composant.Datefromeverything(required=False)
    dtfinvalidite = _composant.Datefromeverything(required=False)
    dtdeb = _composant.Datefromeverything(required=False)
    dtfin = _composant.Datefromeverything(required=False)
    dtbase = _composant.Datefromeverything(required=False)
    dtderobs = _composant.Datefromeverything(required=False)

    # Simulation others attributes
    # refalti
    # courbetarage

    def __init__(self, entite=None, dtprod=None,  grandeur=None,
                 code=None,  qualite=None, dtfinvalidite=None,
                 dtdeb=None, dtfin=None, dtbase=None,
                 dtderobs=None, modecalcul=0, statut=4,
                 sysalti=None, contexte=None, commentaire=None,
                 cmntprive=None, mode=0, modeleprevision=None,
                 contact=None, intervenant=None,
                 publication=22,
                 previsions_tend=None, previsions_det=None,
                 previsions_prb=None, prevs_ensemble=None, prevs_evol=None,
                 scenario=None, strict=True):
        """Initialisation.

        Arguments:
            entite (Sitehydro ou Station)
            dtprod (numpy.datetime64 string, datetime.datetime...): date de production
            grandeur (char in NOMENCLATURE[509]) = H ou Q
            code (int): code de la simulation
            qualite (0 < int < 100) = indice de qualite
            dtfinvalidite (datetime.datetime): date de fin de validite
            dtdeb (datetime.datetime): date de début
            dtfin (datetime.datetime): date de fin
            dtbase (datetime.datetime): temps de base de la simulation
            dtderobs (datetime.datetime): date de la dernière observation
            modecalcul (int): mode de calcul (nomenclature 924)
            statut (int in NOMENCLATURE[516], defaut 4) = brute ou critiquee
            sysalti (int): système altimétrique (nomenclature 76)
            contexte (str): contexte
            commentaire (str): commentaire
            cmntprive (str): commentaire privé
            mode: mode de la simulation (NOMENCLATURE[925])
            modeleprevision (Modeleprevision): modèle de prévision
            contact (intervenant.Contact) : contact
            intervenant (intervenant.Intervenant): intervenant
            publication (int): type de publication (nomenclature 874)
            previsions_tend (PrevisionsTendance): prévisions de tendance
            previsions_det (PrevisionsDeterministes): prévisions déterministes
            previsions_prb (PrevisionsPrb) : prévisions probabilistes
            prevs_ensemble (PrevsEnsemble): Prévisions d'ensemble
            prevs_evol (list[PrevEvol]): prévisions d'évolution
            scenario (ScenarioSimul | None): scénario de simulation
            strict (bool, defaut True) = en mode permissif il n'y a pas de
                controles de validite des parametres
        """

        # -- simple properties --
        self._strict = bool(strict)

        self.commentaire = str(commentaire) if commentaire else None
        self.cmntprive = str(cmntprive) if cmntprive else None
        self.contexte = str(contexte) if contexte else None

        # -- adjust the descriptors --
        vars(Simulation)['grandeur'].strict = self._strict
        vars(Simulation)['statut'].strict = self._strict

        # -- descriptors --
        self.sysalti = sysalti
        self.publication = publication
        self.grandeur = grandeur
        self.statut = statut
        self.modecalcul = modecalcul
        self.mode = mode
        self.dtprod = dtprod
        self.dtfinvalidite = dtfinvalidite
        self.dtdeb = dtdeb
        self.dtfin = dtfin
        self.dtbase = dtbase
        self.dtderobs = dtderobs

        # -- full properties --
        self._entite = self._modeleprevision = \
            self._qualite = self._previsions_tend = \
            self._previsions_prb = None
        self.entite = entite
        self._code = None
        self.code = code
        self._intervenant = None
        self.intervenant = intervenant
        self._contact = None
        self.contact = contact
        self.modeleprevision = modeleprevision
        self.qualite = qualite
        self.previsions_tend = previsions_tend
        self.previsions_prb = previsions_prb
        self._previsions_det = None
        self.previsions_det = previsions_det
        self._prevs_ensemble = None
        self.prevs_ensemble = prevs_ensemble
        self._prevs_evols = []
        self.prevs_evol = prevs_evol
        self._scenario = None
        self.scenario = scenario

    # -- property entite --
    @property
    def entite(self):
        """Return entite hydro."""
        return self._entite

    @entite.setter
    def entite(self, entite):
        """Set entite hydro."""
        try:
            if self._strict and (entite is not None):

                # entite must be a site or a station
                if not isinstance(
                        entite, (_sitehydro.Sitehydro, _sitehydro.Station)):
                    raise TypeError('entite must be a Sitehydro or a Station')

                # Q prevs on Sitehydro only, H prevs on Station
                if self.grandeur is not None:
                    if (self.grandeur == 'Q') and \
                            not isinstance(entite, _sitehydro.Sitehydro):
                        raise TypeError(
                            'Q previsions, entite must be a Sitehydro')
                    if self.grandeur == 'H' and \
                            not isinstance(entite, _sitehydro.Station):
                        raise TypeError(
                            'H previsions, entite must be a Station')

            # all is well
            self._entite = entite

        except Exception:
            raise

    # -- property code --
    @property
    def code(self):
        """Return code de simularion."""
        return self._code

    @code.setter
    def code(self, code):
        """Set code de simulation."""
        if code is None:
            self._code = None
            return
        try:
            code = int(code)
        except BaseException:
            if self._strict and code != '--':
                raise TypeError('simulation code must be an integer')
        self._code = code

    # -- property modeleprevision --
    @property
    def modeleprevision(self):
        """Return modele de prevision."""
        return self._modeleprevision

    @modeleprevision.setter
    def modeleprevision(self, modeleprevision):
        """Set modele de prevision."""
        try:
            if self._strict and (modeleprevision is not None):
                if not isinstance(
                        modeleprevision, _modeleprevision.Modeleprevision):
                    raise TypeError(
                        'modeleprevision must be a Modeleprevision')
            self._modeleprevision = modeleprevision

        except BaseException:
            raise

    # -- property qualite --
    @property
    def qualite(self):
        """Return indice de qualite."""
        return self._qualite

    @qualite.setter
    def qualite(self, qualite):
        """Set indice de qualite."""
        try:
            if qualite is not None:
                qualite = int(qualite)
                if qualite < 0 or qualite > 100:
                    raise ValueError('qualite is not in 0-100 range')
            self._qualite = qualite

        except BaseException:
            raise

    # -- property contact --
    @property
    def contact(self):
        """Return contact."""
        return self._contact

    @contact.setter
    def contact(self, contact):
        """Set contact."""
        try:
            if self._strict and (contact is not None):
                if not isinstance(
                        contact, _intervenant.Contact):
                    raise TypeError(
                        'contact must be an instance of Contact')
            self._contact = contact

        except BaseException:
            raise

    # -- property intervenant --
    @property
    def intervenant(self):
        """Return intervenant."""
        return self._intervenant

    @intervenant.setter
    def intervenant(self, intervenant):
        """Set intervenant."""
        try:
            if self._strict and (intervenant is not None):
                if not isinstance(
                        intervenant, _intervenant.Intervenant):
                    raise TypeError(
                        'intervenant must be an instance of Intervenant')
            self._intervenant = intervenant

        except BaseException:
            raise

    # -- property previsions_tend --
    @property
    def previsions_tend(self):
        """Return previsions_tend."""
        return self._previsions_tend

    @previsions_tend.setter
    def previsions_tend(self, previsions_tend):
        """Set previsions_tend."""
        try:
            if previsions_tend is not None:
                # we check we have a Series...
                # ... and that index contains datetimes
                if self._strict:
                    if not isinstance(previsions_tend, _pandas.DataFrame) or \
                            previsions_tend.index.names != ['dte', 'tend']:
                        raise TypeError('previsions incorrect')
                    previsions_tend.index[0][0].isoformat()
            # all seeem's ok :-)
            self._previsions_tend = previsions_tend
        except BaseException:
            raise

    # -- property previsions --
    @property
    def previsions_prb(self):
        """Return previsions_prb."""
        return self._previsions_prb

    @previsions_prb.setter
    def previsions_prb(self, previsions_prb):
        """Set previsions_prb."""
        try:
            if previsions_prb is not None:
                # we check we have a Series...
                # ... and that index contains datetimes
                if self._strict:
                    if not isinstance(previsions_prb, _pandas.Series) or \
                            previsions_prb.index.names != ['dte', 'prb']:
                        raise TypeError('probabilistic previsions incorrect')
                    previsions_prb.index[0][0].isoformat()
            # all seeem's ok :-)
            self._previsions_prb = previsions_prb
        except BaseException:
            raise

    # -- property previsions_det --
    @property
    def previsions_det(self):
        """Return previsions_det."""
        return self._previsions_det

    @previsions_det.setter
    def previsions_det(self, previsions_det):
        """Set previsions_det."""

        try:
            if previsions_det is not None:
                # we check we have a Series...
                # ... and that index contains datetimes
                if self._strict:
                    if not isinstance(previsions_det, _pandas.DataFrame) \
                            and previsions_det.index.name != 'dte':
                        raise TypeError('deterministic previsions incorrect')
                    if not hasattr(previsions_det.index[0], 'isoformat'):
                        raise TypeError('deterministic previsions incorrect')
            # all seeem's ok :-)
            self._previsions_det = previsions_det
        except BaseException:
            raise

    # -- property prevs_ensemble --
    @property
    def prevs_ensemble(self):
        """Return prevs_ensemble."""
        return self._prevs_ensemble

    @prevs_ensemble.setter
    def prevs_ensemble(self, prevs_ensemble):
        """Set prevs_ensemble."""

        try:
            if prevs_ensemble is not None:
                # we check we have a Series...
                # ... and that index contains datetimes
                if self._strict:
                    if not isinstance(prevs_ensemble, _pandas.DataFrame) \
                            and prevs_ensemble.index.names != ['dte', 'lb']:
                        raise TypeError('ensemble previsions incorrect')
                    if not hasattr(prevs_ensemble.index[0][0], 'isoformat'):
                        raise TypeError('deterministic previsions incorrect')
            # all seeem's ok :-)
            self._prevs_ensemble = prevs_ensemble
        except BaseException:
            raise

    # -- property prevs_evol --
    @property
    def prevs_evol(self):
        """Return prevs_evol."""
        return self._prevs_evol

    @prevs_evol.setter
    def prevs_evol(self, prevs_evol):
        """Set prevs_evol."""
        if prevs_evol is None:
            self._prevs_evol = []
            return
        if isinstance(prevs_evol, PrevEvol):
            self._prevs_evol = [prevs_evol]
            return
        for prv in prevs_evol:
            if not isinstance(prv, PrevEvol):
                raise TypeError(
                    'prevs_evol must be a PrevEvol or an iterable of PrevEvol')
        # all seeem's ok :-)
        self._prevs_evol = prevs_evol

    # -- property prevs_evol --
    @property
    def scenario(self):
        """Return scenario."""
        return self._scenario

    @scenario.setter
    def scenario(self, scenario):
        """Set scenario."""
        if scenario is None:
            self._scenario = None
            return
        if not isinstance(scenario, ScenarioSimul):
            raise TypeError('scenario must be an instance of ScenarioSimul')
        self._scenario = scenario

    # -- special methods --
    __all__attrs__ = ('entite', 'grandeur', 'qualite',
                      'dtdeb', 'dtfin', 'dtbase', 'dtderobs',
                      'modecalcul', 'statut', 'sysalti',
                      'mode', 'modeleprevision', 'publication',
                      'previsions_det', 'previsions_tend', 'previsions_prb',
                      'prevs_ensemble', 'prevs_evol',
                      'scenario')

    __eq__ = _composant.__eq__
    __ne__ = _composant.__ne__
    __hash__ = _composant.__hash__

    def __str__(self):
        """Return str representation."""
        # init
        try:
            statut = _NOMENCLATURE[516][self.statut].lower()
        except Exception:
            statut = '<sans statut>'
        try:
            entite = '{} {}'.format(
                _sitehydro._ARTICLE[self.entite.__class__],
                self.entite.__str__())
        except (AttributeError, KeyError):
            entite = '<une entite inconnue>'
        if self.previsions_tend is not None:
            tend = f'Prévisions de tendance:\n{self.previsions_tend}'
        else:
            tend = '<sans prévisions de tendance>'
        if self.previsions_det is not None:
            det = f'Prévisions déterministes:\n{self.previsions_det}'
        else:
            det = '<sans prévisions déterministes>'
        if self.previsions_prb is not None:
            prob = f'Prévisions probabilistes:\n{self.previsions_prb}'
        else:
            prob = '<sans prévisions probabilistes>'
        if self.prevs_ensemble is not None:
            ens = f'Prévisions d\'ensemble:\n{self.prevs_ensemble}'
        else:
            ens = '<sans prévisions d\'ensemble>'

        if self.prevs_evol:
            evol = 'Prévisions d\'évolution:\n{}'.format(
                '\n'.join([str(prv) for prv in self.prevs_evol]))
        else:
            evol = '<sans prévisions d\'évolution>'
        try:
            dtprod = self.dtprod.isoformat()
        except Exception:
            dtprod = '<inconnue>'
        try:
            qualite = '%i%%' % self.qualite
        except Exception:
            qualite = '<inconnue>'

        scenario = self.scenario if self.scenario is not None else '<sans scénario>'

        # action !
        return '''Simulation {0} de {1} sur {2}\n''' \
               '''Date de production: {3} - Qualite {4}\n''' \
               '''Commentaire: {5}\n''' \
               '''{6}\n''' \
               '''{7}\n''' \
               '''{8}\n''' \
               '''{9}\n''' \
               '''{10}\n''' \
               '''Previsions:\n {11}\n {12}\n {13}\n {14}\n {15}'''.format(
                   statut,
                   self.grandeur or '<sans grandeur>',
                   entite,
                   dtprod,
                   qualite,
                   self.commentaire or '<sans>',
                   '-' * 72,
                   self.modeleprevision or '<modele inconnu>',
                   '-' * 72,
                   scenario,
                   '-' * 72,
                   tend, det, prob, ens, evol)
