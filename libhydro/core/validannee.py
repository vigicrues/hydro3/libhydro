# -*- coding: utf-8 -*-
"""
Module validannee

Ce module contient les classes:
    # ValidAnnee
"""

# -- imports ------------------------------------------------------------------
import pandas as _pandas

from libhydro.core import (sitehydro as _sitehydro, _composant)
from libhydro.core.nomenclature import NOMENCLATURE as _NOMENCLATURE

__author__ = """Sebastien ROMON"""
__version__ = """0.1"""
__date__ = """2020-02-27"""


class ValidAnnee:
    """Class ValidAnnee"""

    qualif = _composant.Nomenclatureitem(nomenclature=576, required=True)
    dispoh = _composant.Nomenclatureitem(nomenclature=518, required=False)
    dispoq = _composant.Nomenclatureitem(nomenclature=518, required=False)
    dtmaj = _composant.Datefromeverything(required=False)

    def __init__(self, entite=None, annee=None, qualif=4, dispoh=None,
                 dispoq=None, cmnt=None, dtmaj=None):

        # -- simple properties --
        self.cmnt = str(cmnt) if cmnt is not None else None

        # -- descriptors --
        self.dispoh = dispoh
        self.dispoq = dispoq
        self.dtmaj = dtmaj

        # -- full properties --
        self._entite = None
        self.entite = entite

        self._annee = None
        self.annee = annee

        self.qualif = qualif

    @property
    def annee(self):
        """Return annee."""
        return self._annee

    @annee.setter
    def annee(self, annee):
        """Set annee."""
        if annee is None:
            raise ValueError('annee is required')

        self._annee = int(annee)

    @property
    def entite(self):
        """Return entite."""
        return self._entite

    @entite.setter
    def entite(self, entite):
        """Set entite."""
        if entite is None:
            raise ValueError('entite is required')

        if not isinstance(entite, (_sitehydro.Sitehydro, _sitehydro.Station)):
            raise TypeError('entite must be a Sitehydro or a Station')

        self._entite = entite

    def __str__(self):
        """Return str representation."""
        if isinstance(self.entite, _sitehydro.Sitehydro):
            typeentite = "du site hydro"
        else:
            typeentite = "de la station"
        return 'Qualification de l\'année {} {} {}: {}'.format(
            self.annee, typeentite, self.entite.code,
            _NOMENCLATURE[576][self.qualif])


class ValidsAnnee(_pandas.DataFrame):

    def __new__(cls, *validsannee):
        tuples = []
        index = []
        for vannee in validsannee:
            index.append((vannee.entite.code, vannee.annee))
            tuples.append(
                (vannee.qualif, vannee.dispoh, vannee.dispoq,
                 vannee.cmnt, vannee.dtmaj))

        # make index
        index = _pandas.MultiIndex.from_tuples(
            index, names=['cdentite', 'annee'])
#         obj = _pandas.DataFrame(
#             data=array[list(array.dtype.names[2:])], index=index)
        obj = _pandas.DataFrame(
            data=tuples, index=index,
            columns=['qualif', 'dispoh', 'dispoq', 'cmnt', 'dtmaj'])

        return obj.sort_index(axis='index')
