"""Module xml.xml_parser.

Module permettant de convertir des éléments xml Sandre V2 en objets.

"""
import lxml.etree as _etree

from libhydro.conv.xml.node import XMLNode
import libhydro.conv.xml.parser as _parser
from libhydro.conv.xml import _xml


def parse_xml(xml):
    """Parse xml string."""
    if isinstance(xml, str):
        root = _etree.fromstring(xml.encode('utf-8'))
    else:
        root = _etree.fromstring(xml)
    tree = _etree.ElementTree(root)
    return _parse_tree(tree)


def parse_xml_file(src):
    """Parse xml file."""
    parser = _etree.XMLParser(
        remove_blank_text=True, remove_comments=True, ns_clean=True)
    tree = _etree.parse(src, parser=parser)
    return _parse_tree(tree)


def _parse_tree(tree):
    """Parse ElementTree."""
    _xml.remove_namespaces(tree)
    node = XMLNode(tree)
    return _parser.parse(node)
