"""Module permettant de convertir les objets hydrométriques en tableaux asscoaitifs

Cette conversion est commune à la production de XML et de JSON.
"""
import collections as _collections
import math as _math

import numpy as _numpy
import pandas as _pandas

from libhydro.conv.xml import sandre_tags as _sandre_tags
from libhydro.core import (intervenant as _intervenant, sitehydro as _sitehydro,
                           sitemeteo as _sitemeteo, courbetarage as _courbetarage)

# some tags mappings
CLS_MAPPINGS = {
    _sitehydro.Sitehydro: 'SiteHydro',
    _sitehydro.Station: 'StationHydro',
    _sitehydro.Capteur: 'Capteur',
    _sitemeteo.Sitemeteo: 'SiteMeteo',
    _sitemeteo.Grandeur: 'Grandeur'}


# -- atomic functions ---------------------------------------------------------
def scenario_to_story(scenario, bdhydro=False, strict=True):
    """Return a story from a xml.Scenario."""

    # prerequisites
    _required(scenario, ['dtprod', 'emetteur', 'destinataire'])
    if strict:
        _required(scenario, ['code', 'version'])
    if bdhydro:
        _required(scenario.emetteur, ['contact'])
        _required(scenario.emetteur.contact, ['code'])

    version = '2' if scenario.version < '2' else scenario.version

    # template for scenario simple element
    story = _collections.OrderedDict((
        ('CodeScenario', {'value': scenario.code}),
        ('VersionScenario', {'value': version}),
        ('NomScenario', {'value': scenario.nom}),
        ('DateHeureCreationFichier',
         {'value': datetime2iso(scenario.dtprod)}),
        ('RefFichier', {'value': scenario.reference}),
        ('RefFichierEnvoi', {'value': scenario.envoi})
    ))
    # template for scenario sub-elements <Emetteur> and <Destinataire>
    for tag in ('Emetteur', 'Destinataire'):
        item = getattr(scenario, tag.lower())
        story[tag] = {
            'sub': _collections.OrderedDict((
                ('CdIntervenant', {
                    'value': str(item.intervenant.code),
                    'attr': {'schemeAgencyID': item.intervenant.origine}}),
                ('NomIntervenant', {
                    'value': str(item.intervenant.nom)
                    if item.intervenant.nom is not None else None}),
                ('CdContact', {
                    'value': str(item.contact.code)
                    if (
                        (item.contact is not None) and
                        (item.contact.code is not None)
                    ) else None})))}

    return story


def intervenant_to_story(intervenant, bdhydro=False, strict=True):
    """Return a story from a intervenant.Intervenant."""

    # prerequisites
    if strict:
        _required(intervenant, ['code'])

    cdcommune = intervenant.commune.code \
        if intervenant.commune is not None else None
    adresse = intervenant.adresse \
        if intervenant.adresse is not None else None

    contacts = []
    for contact in intervenant.contacts:
        contacts.append(
            _contact_to_story(
                contact, bdhydro=bdhydro, strict=strict))

    # template for intervenant simple elements
    story = _collections.OrderedDict((
        ('CdIntervenant', {
            'value': str(intervenant.code),
            'attr': {'schemeAgencyID': intervenant.origine}}),
        ('NomIntervenant', {'value': intervenant.nom}),
        ('StIntervenant', {'value': intervenant.statut}),
        ('DateCreationIntervenant', {
            'value': date2iso(intervenant.dtcreation)}),
        ('DateMajIntervenant', {'value': datetime2iso(intervenant.dtmaj)}),
        ('AuteurIntervenant', {'value': intervenant.auteur}),
        ('MnIntervenant', {'value': intervenant.mnemo}),
        ('BpIntervenant', {'value': adresse.boitepostale}),
        ('ImmoIntervenant', {'value': adresse.adresse1_cplt}),
        ('RueIntervenant', {'value': adresse.adresse1}),
        ('LieuIntervenant', {'value': adresse.lieudit}),
        ('VilleIntervenant', {'value': adresse.ville}),
        ('DepIntervenant', {'value': adresse.dep}),
        ('CommentairesIntervenant', {'value': intervenant.commentaire}),
        ('ActivitesIntervenant', {'value': intervenant.activite}),
        ('CPIntervenant', {'value': adresse.codepostal}),
        ('NomInternationalIntervenant', {
            'value': intervenant.nominternational}),
        ('CdSIRETRattacheIntervenant', {'value': intervenant.siret})))

    if cdcommune is not None:
        story['Commune'] = {
            'value': None,
            'sub': _collections.OrderedDict((
                    ('CdCommune', {'value': cdcommune}),
                    ('LbCommune', {
                        'value': intervenant.commune.libelle})))}
    story['Contacts'] = {
        'value': None,
        'list': contacts,
        'subtag': 'Contact'
    }
    story['PaysComplementIntervenant'] = {'value': adresse.pays}
    story['AdEtrangereComplementIntervenant'] = {'value': adresse.adresse2}
    story['TelephoneComplementIntervenant'] = {
        'value': intervenant.telephone}
    story['FaxComplementIntervenant'] = {'value': intervenant.fax}
    story['SiteWebComplementIntervenant'] = {'value': intervenant.siteweb}

    return story


def _contact_to_story(contact, bdhydro=False, strict=True):
    """Return a story from a intervenant.Contact."""

    # prerequisite
    if strict:
        _required(contact, ['code'])

    adresse = contact.adresse if contact.adresse is not None \
        else _intervenant.Adresse()

    profils = [_contactprofiladmin_to_story(profiladmin)
               for profiladmin in contact.profilsadmin]

    # template for contact simple elements
    story = _collections.OrderedDict((
        # FIXME - this tag can be factorize
        ('CdContact', {
            'value': contact.code
            if (
                (contact is not None) and
                (contact.code is not None)
            ) else None,
            # bdhydro requires a junk attr for the contacts
            'attr': None}),
        ('NomContact', {'value': contact.nom}),
        ('PrenomContact', {'value': contact.prenom}),
        ('CiviliteContact', {'value': contact.civilite}),
        ('ProfilContact', {'value': contact.profilasstr}),
        ('AdContact', {'value': adresse.adresse1}),
        ('AdEtrangereContact', {'value': adresse.adresse2}),
        ('CpContact', {'value': adresse.codepostal}),
        ('VilleContact', {'value': adresse.ville}),
        ('FonctionContact', {'value': contact.fonction}),
        ('TelephoneContact', {'value': contact.telephone}),
        ('PortContact', {'value': contact.portable}),
        ('FaxContact', {'value': contact.fax}),
        ('MelContact', {'value': contact.mel}),
        ('PaysContact', {'value': adresse.pays}),
        ('DateMajContact', {'value': datetime2iso(contact.dtmaj)}),
        ('ProfilsAdminLocal', {
            'value': None,
            'list': profils,
            'subtag': 'ProfilAdminLocal'
        }),
        ('AliasContact', {'value': contact.alias}),
        ('MotPassContact', {'value': contact.motdepasse}),
        ('DtActivationContact', {
            'value': datetime2iso(contact.dtactivation)}),
        ('DtDesactivationContact', {
            'value': datetime2iso(contact.dtdesactivation)})))
    return story


def _contactprofiladmin_to_story(profil):
    """Return a story from _intervenant.ProfilAdminLocal"""
    if profil is None:
        return
    zones = [_zonehydro_to_story(zone) for zone in profil.zoneshydro]
    story = _collections.OrderedDict((
        ('CdProfilAdminLocal', {'value': profil.profil}),
        ('ZonesHydro', {
            'value': None,
            'list': zones,
            'subtag': 'ZoneHydro'
        }),
        ('DtActivationProfilAdminLocal', {
            'value': datetime2iso(profil.dtactivation)}),
        ('DtDesactivationProfilAdminLocal', {
            'value': datetime2iso(profil.dtdesactivation)})
        ))
    return story


def _zonehydro_to_story(zonehydro):
    story = _collections.OrderedDict((
        ('CdZoneHydro', {'value': zonehydro.code}),
        ('LbZoneHydro', {'value': zonehydro.libelle})))
    return story


def sitehydro_to_story(sitehydro, bdhydro=False, strict=True):
    """Return a story from a sitehydro.Sitehydro.

    Args:
        sitehydro (sitehydro.Sitehydro)

    """

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    if strict:
        _required(sitehydro, ['code'])

    dtmaj = datetime2iso(sitehydro.dtmaj)
    dtpremdonnee = None
    if sitehydro.dtpremieredonnee is not None:
        dtpremdonnee = datetime2iso(sitehydro.dtpremieredonnee)
    essai = sitehydro.essai

    typesite = sitehydro.typesite
    precisiontype = sitehydro.precisiontype

    communes = [_commune_to_story(commune) for commune in sitehydro.communes]
    periodes = [_periode_shv_to_story(periode) for periode in sitehydro.periodes_shv]

    # template for sitehydro simple elements
    story = _collections.OrderedDict((
        ('CdSiteHydro', {'value': sitehydro.code}),
        ('LbSiteHydro', {'value': sitehydro.libelle}),
        ('LbUsuelSiteHydro', {'value': sitehydro.libelleusuel}),
        ('TypSiteHydro', {'value': typesite}),
        ('PrecisionTypSiteHydro', {'value': precisiontype}),
        ('MnSiteHydro', {'value': sitehydro.mnemo}),
        (tags.comtlbsitehydro, {'value': sitehydro.complementlibelle}),
        ('CoordSiteHydro', {
            'value': None,
            'force': True if sitehydro.coord is not None else False}),
        ('PkAmontSiteHydro', {'value': sitehydro.pkamont}),
        ('PkAvalSiteHydro', {'value': sitehydro.pkaval}),
        ('AltiSiteHydro', {
            'value': None,
            'force': True if sitehydro.altitude is not None else False}),
        (tags.dtmajsitehydro, {'value': dtmaj})))
    story['BassinVersantSiteHydro'] = {'value': sitehydro.bvtopo}

    story['BassinVersantHydroSiteHydro'] = {'value': sitehydro.bvhydro}
    story['FuseauHoraireSiteHydro'] = {'value': sitehydro.fuseau}
    story[tags.stsitehydro] = {'value': sitehydro.statut}
    story['DtPremDonSiteHydro'] = {'value': dtpremdonnee}
    story['PremMoisEtiageSiteHydro'] = {'value': sitehydro.moisetiage}
    story['PremMoisAnHydSiteHydro'] = {'value': sitehydro.moisanneehydro}

    story['DureeCarCruSiteHydro'] = {'value': sitehydro.dureecrues}
    story['DroitPublicationSiteHydro'] = {'value': sitehydro.publication}
    story['EssaiSiteHydro'] = {'value': essai}
    story['InfluGeneSiteHydro'] = {'value': sitehydro.influence}
    story['ComInfluGeneSiteHydro'] = {
            'value': sitehydro.influencecommentaire}
    story['ComSiteHydro'] = {'value': sitehydro.commentaire}
    if sitehydro.siteassocie is not None:
        story['SiteHydroAssocie'] = {
            'sub': _collections.OrderedDict((
                ('CdSiteHydro', {'value': sitehydro.siteassocie.code}), ))}
    if len(sitehydro.periodes_shv) > 0:
        tag_shv = 'PeriodesActiviteSiteHydroVirtuel'
        story[tag_shv] = {'value': None, 'list': periodes,
                          'subtag': 'PeriodeActiviteSiteHydroVirtuel'}
    story['CdEuMasseDEau'] = {'value': sitehydro.massedeau}

    if sitehydro.entitehydro is not None:
        story['EntiteHydrographique'] = {
            'sub': _collections.OrderedDict((
                ('CdEntiteHydrographique', {
                    'value': sitehydro.entitehydro.code}),
                ('NomEntiteHydrographique', {
                    'value': sitehydro.entitehydro.libelle})))}
    lois = [_loistat_to_story(loi, 'SiteHydro') for loi in sitehydro.loisstat]
    if len(sitehydro.loisstat) > 0:
        story['LoisStatContexteSiteHydro'] = {'value': None, 'list': lois,
                                              'subtag': 'LoiStatContexteSiteHydro'}
    images = [_image_to_story(image=image, entite='SiteHydro') for image in sitehydro.images]
    if len(sitehydro.images) > 0:
        story['ImagesSiteHydro'] = {'value': None, 'list': images, 'subtag': 'ImageSiteHydro'}

    roles = [_role_to_story(role=role, tags=tags, entite='SiteHydro')
             for role in sitehydro.roles]
    if len(sitehydro.roles) > 0:
        story[tags.rolscontactsitehydro] = {'value': None, 'list': roles,
                                            'subtag': 'RolContactSiteHydro'}

    story['CdTronconHydrographique'] = {'value': sitehydro.tronconhydro}

    troncons = [_tronconvigilance_to_story(troncon, strict=strict)
                for troncon in sitehydro.entitesvigicrues]
    if len(sitehydro.entitesvigicrues) > 0:
        story[tags.entsvigicru] = {'value': None, 'list': troncons, 'subtag': 'EntVigiCru'}

    story['Communes'] = {'value': None, 'list': communes, 'subtag': 'Commune'}
    story['CdSiteHydroAncienRef'] = {'value': sitehydro.codeh2}

    stations = [_station_to_story(station, bdhydro=bdhydro, strict=strict)
                for station in sitehydro.stations]
    story['StationsHydro'] = {
        'value': None,
        'list': stations,
        'subtag': 'StationHydro'
    }

    # story['ValeursSeuilsSiteHydro'] = {
    #         'value': None,
    #         'force': True if (len(seuilshydro) > 0) else False}
    pluies = [_pluiebassin_to_story(sitemeteopondere=sitemeteopondere, bdhydro=bdhydro,
                                    tags=tags)
              for sitemeteopondere in sitehydro.pluiesbassin]
    story[tags.pluiesdebassin] = {'value': None, 'list': pluies, 'subtag': 'PluieDeBassin'}

    if sitehydro.zonehydro is not None:
        story['ZoneHydro'] = {
            'sub': _collections.OrderedDict((
                ('CdZoneHydro', {
                    'value': sitehydro.zonehydro.code}),
                ('LbZoneHydro', {
                    'value': sitehydro.zonehydro.libelle})))}
    story['PrecisionCoursDEauSiteHydro'] = {
        'value': sitehydro.precisioncoursdeau}

    sitesamont = [_siteamontaval_to_element(site=siteamont, amont=True)
                  for siteamont in sitehydro.sitesamont]
    sitesaval = [_siteamontaval_to_element(site=siteaval, amont=False)
                 for siteaval in sitehydro.sitesaval]

    if len(sitehydro.sitesamont) > 0:
        story['SitesHydroAmont'] = {'value': None, 'list': sitesamont, 'subtag': 'SiteHydroAmont'}
    if len(sitehydro.sitesaval) > 0:
        story['SitesHydroAval'] = {'value': None, 'list': sitesaval, 'subtag': 'SiteHydroAval'}
    story['CdBNBV'] = {'value': sitehydro.cdbnbv}

    # update the coord if necessary
    if sitehydro.coord is not None:
        story['CoordSiteHydro'] = {
            'sub': _collections.OrderedDict((
                ('CoordXSiteHydro', {'value': sitehydro.coord.x}),
                ('CoordYSiteHydro', {'value': sitehydro.coord.y}),
                ('ProjCoordSiteHydro', {'value': sitehydro.coord.proj})))}

    # update the coord if necessary
    if sitehydro.altitude is not None:
        story['AltiSiteHydro'] = {
            'sub': _collections.OrderedDict((
                ('AltitudeSiteHydro',
                 {'value': sitehydro.altitude.altitude}),
                ('SysAltimetriqueSiteHydro',
                 {'value': sitehydro.altitude.sysalti}),
                ))}
    return story


def _periode_shv_to_story(periode_shv):
    """Return a story from a _sitehydro.PeriodeSitehydrovirtuel."""
    if periode_shv is None:
        return
    dtactivation = datetime2iso(periode_shv.dtactivation)
    dtdesactivation = datetime2iso(periode_shv.dtdesactivation)
    dtdeb = datetime2iso(periode_shv.dtdeb)
    dtfin = datetime2iso(periode_shv.dtfin)
    story = _collections.OrderedDict((
        ('DtDebActivationPeriodeActiviteSiteHydroVirtuel',
         {'value': dtactivation}),
        ('DtFinActivationPeriodeActiviteSiteHydroVirtuel',
         {'value': dtdesactivation}),
        ('DtDebPeriodeActiviteSiteHydroVirtuel', {'value': dtdeb}),
        ('DtFinPeriodeActiviteSiteHydroVirtuel', {'value': dtfin}),
        ))
    sitesattaches = [_siteattache_to_dict(site) for site in periode_shv.sitesattaches]
    story['SitesHydroAttaches'] = {'value': None, 'list': sitesattaches,
                                   'subtag': 'SiteHydroAttache'}
    return story


def _pluiebassin_to_story(sitemeteopondere, bdhydro, tags):
    """Return a story from an _sitemeteo.SitemeteoPondere"""
    if sitemeteopondere is None:
        return
    code = _codesitemeteo_to_value(sitemeteo=sitemeteopondere, bdhydro=bdhydro)
    story = _collections.OrderedDict((
        ('CdSiteMeteo', {'value': code}),
        (tags.ponderationpluiedebassin,
         {'value': sitemeteopondere.ponderation})))
    return story


def _image_to_story(image, entite):
    """Return a story  from a _composant_site.Image"""
    if image is None:
        return

    story = _collections.OrderedDict((
        ('AdressedelImage' + entite, {'value': image.adresse}),
        ('TypIll' + entite, {'value': image.typeill}),
        ('FormatIll' + entite, {'value': image.formatimg}),
        ('ComImg' + entite, {'value': image.commentaire})))
    return story


def _role_to_story(role, tags, entite):
    """Return a story from rolecontact.RoleContact."""
    if role is None:
        return

    dtdeb = datetime2iso(role.dtdeb)
    dtfin = datetime2iso(role.dtfin)
    dtmaj = datetime2iso(role.dtmaj)
    rolecontactbalise = 'RoleContact' + entite
    story = _collections.OrderedDict((
        ('CdContact', {'value': role.contact.code}),
        (rolecontactbalise, {'value': role.role}),
        ('DtDebutContact' + entite, {'value': dtdeb}),
        ('DtFinContact' + entite, {'value': dtfin}),
        (tags.dtmajrolecontact + entite, {'value': dtmaj})))
    return story


def _commune_to_story(commune):
    """Return a story from a _composant_site.commune"""
    if commune is None:
        return
    story = _collections.OrderedDict((
        ('CdCommune', {'value': commune.code}),
        ('LbCommune', {'value': commune.libelle})))
    return story


def _siteamontaval_to_element(site, amont):
    """Return a story with only code and label from sitehydro.Sitehydro"""
    if site is None:
        return
    story = _collections.OrderedDict((
        ('CdSiteHydro', {'value': site.code}),
        ('LbSiteHydro', {'value': site.libelle})))
    return story


def _siteattache_to_dict(siteattache):
    """Return a story from a sitehydro.Sitehydroattache"""
    if siteattache is None:
        return
    story = _collections.OrderedDict()

    story['SiteHydro'] = {'sub': _collections.OrderedDict((
        ('CdSiteHydro', {'value': siteattache.code}),
        ('LbSiteHydro', {'value': siteattache.libelle})))}
    story['PonderationSiteHydroAttache'] = {'value': siteattache.ponderation}
    story['DecalSiteHydroAttache'] = {'value': siteattache.decalage}
    return story


def _loistat_to_story(loistat, entite):
    """Return a story from a _composant_site.LoiStat."""
    if loistat is None:
        return
    story = _collections.OrderedDict((
        ('TypContexteLoiStat', {'value': loistat.contexte}),
        ('TypLoi' + entite, {'value': loistat.loi})))
    return story


def _codesitemeteo_to_value(sitemeteo, bdhydro=False, strict=True):
    """Conversion code of sitemeteo."""
    code = sitemeteo.code
    if strict:
        _required(sitemeteo, ['code'])
        # in bdhydro cdsitemeteo 8 or 9 char
        if bdhydro and sitemeteo.code[0] == '0':
            code = sitemeteo.code[1:]
    return code


def _station_to_story(station, bdhydro=False, strict=True):
    """Return a story from a sitehydro.Station."""

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    if strict:
        _required(station, ['code'])

    dtmaj = datetime2iso(station.dtmaj)
    dtmiseservice = datetime2iso(station.dtmiseservice)
    dtfermeture = datetime2iso(station.dtfermeture)
    surveillance = station.surveillance
    essai = station.essai
    typestation = station.typestation

    plages = [_plage_to_story(plage, 'StationHydro') for plage in station.plages]

    stationsant = [_substation_to_story(station=stationant, tag='StationHydroAnterieure')
                   for stationant in station.stationsanterieures]
    stationspost = [_substation_to_story(station=stationpost, tag='StationHydroPosterieure')
                    for stationpost in station.stationsposterieures]

    qualifs = [_qualifdonnees_to_story(qualif) for qualif in station.qualifsdonnees]
    finalites = [{'CdFinaliteStationHydro': {'value': finalite}} for finalite in station.finalites]
    lois = [_loistat_to_story(loi, 'StationHydro') for loi in station.loisstat]
    images = [_image_to_story(image, 'StationHydro') for image in station.images]
    roles = [_role_to_story(role=role, tags=tags, entite='StationHydro')
             for role in station.roles]
    reseaux = [_reseau_to_story(reseau) for reseau in station.reseaux]
    capteurs = [_capteur_to_story(capteur, bdhydro=bdhydro, strict=strict)
                for capteur in station.capteurs]

    refsalti = [_refalti_to_story(refalti) for refalti in station.refsalti]

    stationsamont = [_substation_to_story(station=stationamont, tag='StationHydroAmont')
                     for stationamont in station.stationsamont]
    stationsaval = [_substation_to_story(station=stationaval, tag='StationHydroAval')
                    for stationaval in station.stationsaval]
    plagesfille = [_plagestation_to_story(plagestation=plagestation, entite='StationHydroFille')
                   for plagestation in station.plagesstationsfille]
    plagesmere = [_plagestation_to_story(plagestation=plagestation, entite='StationHydroMere')
                  for plagestation in station.plagesstationsmere]

    # template for station simple element
    story = _collections.OrderedDict((
        ('CdStationHydro', {'value': station.code}),
        ('LbStationHydro', {'value': station.libelle}),
        ('TypStationHydro', {'value': typestation}),
        (tags.complementlibellestationhydro, {
            'value': station.libellecomplement}),
        (tags.comprivestationhydro, {'value': station.commentaireprive}),
        (tags.dtmajstationhydro, {'value': dtmaj}),
        ('CoordStationHydro', {
            'value': None,
            'force': True if station.coord is not None else False}),
        ('PkStationHydro', {'value': station.pointk}),
        ('DtMiseServiceStationHydro', {'value': dtmiseservice}),
        ('DtFermetureStationHydro', {'value': dtfermeture}),
        ('ASurveillerStationHydro', {'value': surveillance}),
        ('NiveauAffichageStationHydro', {
            'value': station.niveauaffichage}),
        ('DroitPublicationStationHydro', {
            'value': station.droitpublication}),
        ('DelaiDiscontinuiteStationHydro', {
            'value': station.delaidiscontinuite}),
        ('DelaiAbsenceStationHydro', {'value': station.delaiabsence}),
        ('EssaiStationHydro', {'value': essai}),
        ('InfluLocaleStationHydro', {'value': station.influence}),
        ('ComInfluLocaleStationHydro', {
            'value': station.influencecommentaire}),
        ('ComStationHydro', {'value': station.commentaire}),

        ('StationsHydroAnterieures', {
                'value': None,
                'list': stationsant, 'subtag': 'StationHydroAnterieure'}),
        ('StationsHydroPosterieures', {
                'value': None,
                'list': stationspost, 'subtag': 'StationHydroPosterieure'}),
        ('QualifsDonneesStationHydro', {
                'value': None,
                'list': qualifs, 'subtag': 'QualifDonneesStationHydro'
                }),
        ('FinalitesStationHydro', {
                'value': None,
                'list': finalites, 'subtag': 'FinaliteStationHydro'}),
        ('LoisStatContexteStationHydro', {
                'value': None,
                'list': lois, 'subtag': 'LoiStatContexteStationHydro'}),
        ('ImagesStationHydro', {
                'value': None,
                'list': images, 'subtag': 'ImageStationHydro'}),
        (tags.rolscontactstationhydro, {
                'value': None,
                'list': roles, 'subtag': 'RolContactStationHydro'}),
        ('PlagesUtilStationHydro', {
            'value': None,
            'list': plages, 'subtag': 'PlageUtilStationHydro'}),
        ('ReseauxMesureStationHydro', {
            'value': None,
            'list': reseaux, 'subtag': 'ReseauMesureStationHydro'}),
        ('Capteurs', {
            'value': None,
            'list': capteurs, 'subtag': 'Capteur'}),
        ('RefsAlti', {
            'value': None,
            'list': refsalti, 'subtag': 'RefAlti'}),
        ('CdStationHydroAncienRef', {'value': station.codeh2})))

    if station.commune is not None:
        story['Commune'] = {'sub': _commune_to_story(station.commune)}

    if len(station.stationsamont) > 0:
        story['StationsHydroAmont'] = {'value': None, 'list': stationsamont,
                                       'subtag': 'StationHydroAmont'}
    if len(station.stationsaval) > 0:
        story['StationsHydroAval'] = {'value': None, 'list': stationsaval,
                                      'subtag': 'StationHydroAval'}
    if len(station.plagesstationsfille) > 0:
        story['PlagesAssoStationHydroFille'] = {'value': None,
                                                'list': plagesfille,
                                                'subtag': 'PlageAssoStationHydroFille'}
    if len(station.plagesstationsmere) > 0:
        story['PlagesAssoStationHydroMere'] = {'value': None,
                                               'list': plagesmere,
                                               'subtag': 'PlageAssoStationHydroMere'}

    # update the coord if necessary
    if station.coord is not None:
        story['CoordStationHydro'] = {
            'sub': _collections.OrderedDict((
                ('CoordXStationHydro', {'value': station.coord.x}),
                ('CoordYStationHydro', {'value': station.coord.y}),
                ('ProjCoordStationHydro',
                    {'value': station.coord.proj})))}

    return story


def _plage_to_story(plage, entite):
    """Return a story from a sitehydro.PlageUtil."""
    if plage is None:
        return None

    story = _collections.OrderedDict()
    story['DtDebPlageUtil{}'.format(entite)] = {
            'value': datetime2iso(plage.dtdeb)}
    if plage.dtfin is not None:
        story['DtFinPlageUtil{}'.format(entite)] = {
            'value': datetime2iso(plage.dtfin)}
    if plage.dtactivation is not None:
        story['DtActivationPlageUtil{}'.format(entite)] = {
            'value': datetime2iso(plage.dtactivation)}
    if plage.dtdesactivation is not None:
        story['DtDesactivationPlageUtil{}'.format(entite)] = {
            'value': datetime2iso(plage.dtdesactivation)}
    if plage.active is not None:
        story['ActivePlageUtil{}'.format(entite)] = {
            'value': plage.active}
    return story


def _tronconvigilance_to_story(entitevigicrues, bdhydro=False, strict=True):
    """Return a story from a sitehydro.Tronconvigilance."""

    if entitevigicrues is None:
        return

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    if strict:
        _required(entitevigicrues, ['code'])

    # template for tronconvigilance simple elements
    story = _collections.OrderedDict((
        (tags.cdentvigicru, {'value': entitevigicrues.code}),
        (tags.nomentvigicru, {'value': entitevigicrues.libelle})))

    return story


def _substation_to_story(station, tag):
    """Return a story with only code and libelle from station"""
    if station is None:
        return
    story = _collections.OrderedDict()
    story['CdStationHydro'] = {'value': station.code}
    story['LbStationHydro'] = {'value': station.libelle}
    return story


def _qualifdonnees_to_story(qualif):
    """Return a a story from qualif"""
    if qualif is None:
        return None

    story = _collections.OrderedDict()
    story['CdRegime'] = {'value': qualif.coderegime}
    story['QualifDonStationHydro'] = {
            'value': qualif.qualification}
    story['ComQualifDonStationHydro'] = {'value': qualif.commentaire}
    return story


def _reseau_to_story(reseau):
    return _collections.OrderedDict(
        (('CodeSandreRdd', {'value': reseau.code}),
         ('NomRdd', {'value': reseau.libelle})))


def _capteur_to_story(capteur, bdhydro=False, strict=True):
    """Return a story from a sitehydro.Capteur."""

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    if strict:
        _required(capteur, ['code'])
    if bdhydro:
        _required(capteur, ['libelle'])

    if capteur.surveillance is not None:
        surveillance = capteur.surveillance
    else:
        surveillance = None

    if capteur.essai is not None:
        essai = capteur.essai
    else:
        essai = None

    typecapteur = capteur.typecapteur

    dtmaj = None
    if capteur.dtmaj is not None:
        dtmaj = datetime2iso(capteur.dtmaj)
    plages = [_plage_to_story(plage, 'Capteur') for plage in capteur.plages]
    # template for capteur simple element
    story = _collections.OrderedDict((
        ('CdCapteur', {'value': capteur.code}),
        ('LbCapteur', {'value': capteur.libelle}),
        ('MnCapteur', {'value': capteur.mnemo}),
        ('TypCapteur', {'value': typecapteur}),
        ('TypMesureCapteur', {'value': capteur.typemesure}),
        ('ASurveillerCapteur', {'value': surveillance}),
        (tags.dtmajcapteur, {'value': dtmaj}),
        (tags.pdtcapteur, {'value': capteur.pdt}),
        ('EssaiCapteur', {'value': essai}),
        ('ComCapteur', {'value': capteur.commentaire})))

    if capteur.observateur is not None:
        story['Observateur'] = {'sub': {'CdContact': {
            'value': capteur.observateur.code}}}

    story['PlagesUtilCapteur'] = {'value': None, 'list': plages, 'subtag': 'PlageUtilCapteur'}
    story['CdCapteurAncienRef'] = {'value': capteur.codeh2}
    return story


def _refalti_to_story(refalti):
    """Return a story from  a _composant_site.RefAlti"""
    if refalti is None:
        return None

    dtfin = datetime2iso(refalti.dtfin)
    dtactivation = datetime2iso(refalti.dtactivation)
    dtdesactivation = datetime2iso(refalti.dtdesactivation)
    dtmaj = datetime2iso(refalti.dtmaj)

    story = _collections.OrderedDict()
    if refalti.altitude is not None:
        story['AltitudeRefAlti'] = {'value': refalti.altitude.altitude}
        story['SysAltiRefAlti'] = {'value': refalti.altitude.sysalti}
    story['DtDebutRefAlti'] = {'value': datetime2iso(refalti.dtdeb)}
    story['DtFinRefAlti'] = {'value': dtfin}
    story['DtActivationRefAlti'] = {'value': dtactivation}
    story['DtDesactivationRefAlti'] = {'value': dtdesactivation}
    story['DtMajRefAlti'] = {'value': dtmaj}
    return story


def _plagestation_to_story(plagestation, entite):
    """Return a story from a _copmosant_site.PlageStation"""
    if plagestation is None:
        return

    # element = _etree.Element('PlageAsso' + entite)
    tagentite = entite.replace('Station', 'station')
    # element.append(_substation_to_element(station=plagestation,
    #                                       tag=tagentite))
    # story['StationHydro'] = {'value': None, 'force': True}

    story = _collections.OrderedDict((
        (tagentite, {'sub': _substation_to_story(station=plagestation, tag=tagentite)}),
        ('DtDebPlageAssoStationHydroMereFille', {'value': datetime2iso(plagestation.dtdeb)}),
        ('DtFinPlageAssoStationHydroMereFille', {'value': datetime2iso(plagestation.dtfin)}),
        ('DtMajPlageAssoStationHydroMereFille', {'value': datetime2iso(plagestation.dtmaj)})))
    return story


def sitemeteo_to_story(sitemeteo, bdhydro=False, strict=True):
    """Return a story from a sitemeteo.Sitemeteo."""

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    code = _codesitemeteo_to_value(sitemeteo, bdhydro, strict)

    if sitemeteo.zonehydro is not None:
        zonehydro_story = {
            'sub': _collections.OrderedDict((
                ('CdZoneHydro', {
                    'value': sitemeteo.zonehydro.code}),
                ('LbZoneHydro', {
                    'value': sitemeteo.zonehydro.libelle})))}
    else:
        zonehydro_story = {'value': None}

    images = [_image_to_story(image, 'SiteMeteo') for image in sitemeteo.images]
    reseaux = [_reseau_to_story(reseau) for reseau in sitemeteo.reseaux]
    roles = [_role_to_story(role=role, tags=tags, entite='SiteMeteo')
             for role in sitemeteo.roles]
    grandeurs = [_grandeur_to_story(grandeur, strict=strict)
                 for grandeur in sitemeteo.grandeurs]
    visites = [_visite_to_story(visite) for visite in sitemeteo.visites]
    # template for sitemeteo simple elements
    story = _collections.OrderedDict((
        ('CdSiteMeteo', {'value': code}),
        ('LbSiteMeteo', {'value': sitemeteo.libelle}),
        ('LbUsuelSiteMeteo', {'value': sitemeteo.libelleusuel}),
        ('MnSiteMeteo', {'value': sitemeteo.mnemo}),
        ('LieuDitSiteMeteo', {'value': sitemeteo.lieudit}),
        ('CoordSiteMeteo', {
            'value': None,
            'force': True if sitemeteo.coord is not None else False}),
        ('AltiSiteMeteo', {
            'value': None,
            'force': True if sitemeteo.altitude is not None else False}),
        ('FuseauHoraireSiteMeteo', {'value': sitemeteo.fuseau}),
        (tags.dtmajsitemeteo, {'value': datetime2iso(sitemeteo.dtmaj)}),
        ('DtOuvertureSiteMeteo', {
            'value': datetime2iso(sitemeteo.dtouverture)}),
        ('DtFermSiteMeteo', {
            'value': datetime2iso(sitemeteo.dtfermeture)}),
        ('DroitPublicationSiteMeteo', {
            'value': sitemeteo.droitpublication}),
        ('EssaiSiteMeteo', {'value': sitemeteo.essai}),
        ('ComSiteMeteo', {'value': sitemeteo.commentaire}),
        ('ImagesSiteMeteo', {
            'value': None,
            'list': images, 'subtag': 'ImageSiteMeteo'}),
        ('ReseauxMesureSiteMeteo', {
            'value': None,
            'list': reseaux, 'subtag': 'RSX'}),
        (tags.rolscontactsitemeteo, {
            'value': None,
            'list': roles, 'subtag': 'RolContactSiteMeteo'}),
        ('ZoneHydro', zonehydro_story)))

    if sitemeteo.commune is not None:
        story['Commune'] = {'sub': _commune_to_story(sitemeteo.commune)}

    story['GrdsMeteo'] = {'value': None, 'list': grandeurs, 'subtag': 'GrdMeteo'}

    story['VisitesSiteMeteo'] = {
        'value': None,
        'list': visites, 'subtag': 'VisiteSiteMeteo'}

    # update the coord if necessary
    if sitemeteo.coord is not None:
        story['CoordSiteMeteo'] = {
            'sub': _collections.OrderedDict((
                ('CoordXSiteMeteo', {'value': sitemeteo.coord.x}),
                ('CoordYSiteMeteo', {'value': sitemeteo.coord.y}),
                ('ProjCoordSiteMeteo', {'value': sitemeteo.coord.proj})))}

    # update the altitude if necessary
    if sitemeteo.altitude is not None:
        story['AltiSiteMeteo'] = {
            'sub': _collections.OrderedDict((
                ('AltitudeSiteMeteo', {
                    'value': sitemeteo.altitude.altitude}),
                ('SysAltimetriqueSiteMeteo', {
                    'value': sitemeteo.altitude.sysalti})))}

    return story


def _grandeur_to_story(grandeur, strict=True):
    """Return a story from a sitehydro.grandeur."""

    tags = _sandre_tags.SandreTagsV2

    # prerequisites
    if strict:
        _required(grandeur, ['typemesure'])

    surveillance = grandeur.surveillance
    delaiabsence = grandeur.delaiabsence

    grdseuils = []

    classesqualite = [_classequalite_to_story(classequalite)
                      for classequalite in grandeur.classesqualite]
    # template for grandeur simple element
    story = _collections.OrderedDict((
        ('CdGrdMeteo', {'value': grandeur.typemesure}),
        ('DtMiseServiceGrdMeteo', {
            'value': datetime2iso(grandeur.dtmiseservice)}),
        ('DtFermetureServiceGrdMeteo', {
            'value': datetime2iso(grandeur.dtfermeture)}),
        ('EssaiGrdMeteo', {'value': grandeur.essai}),
        ('ASurveillerGrdMeteo', {'value': surveillance}),
        ('DelaiAbsGrdMeteo', {'value': delaiabsence}),
        (tags.pdtgrdmeteo, {'value': grandeur.pdt}),
        ('ClassesQualiteGrd', {
            'value': None,
            'list': classesqualite, 'subtag': 'ClasseQualiteGrd'}),
        ('ValeursSeuilsGrdMeteo', {
            'value': None,
            'force': True if len(grdseuils) > 0 else False}),
        ('DtMajGrdMeteo', {'value': datetime2iso(grandeur.dtmaj)})))
    return story


def _classequalite_to_story(classequalite):
    """Return a story from a sitemeteo.ClasseQualite"""
    if classequalite is None:
        return
    dtvisite = None
    if classequalite.visite is not None:
        dtvisite = datetime2iso(classequalite.visite.dtvisite)

    # template for classequalite simple element
    story = _collections.OrderedDict((
        ('CdqClasseQualiteGrd', {'value': classequalite.classe}),
        ('DtVisiteSiteMeteo', {'value': dtvisite}),
        ('DtDebutClasseQualiteGrd', {
            'value': datetime2iso(classequalite.dtdeb)}),
        ('DtFinClasseQualiteGrd', {
            'value': datetime2iso(classequalite.dtfin)})))
    return story


def _visite_to_story(visite):
    """Return a story from a sitemeteo.Visite."""
    if visite is None:
        return
    # template for tronconvigilance simple elements
    story = _collections.OrderedDict((
        ('DtVisiteSiteMeteo', {'value': datetime2iso(visite.dtvisite)}),
        ('CdContact', {'value': visite.contact.code
                       if visite.contact is not None else None}),
        ('MethClassVisiteSiteMeteo', {'value': visite.methode}),
        ('ModeOperatoireUtiliseVisiteSiteMeteo', {'value': visite.modeop})))
    return story


def modeleprevision_to_story(modeleprevision, bdhydro=False, strict=True):
    """Return a story element from a modeleprevision.Modeleprevision."""

    # prerequisite
    if strict:
        _required(modeleprevision, ['code'])

    cdcontact = None
    if modeleprevision.contact is not None:
        cdcontact = modeleprevision.contact.code
    # template for modeleprevision simple elements
    siteshydro = [_collections.OrderedDict((('CdSiteHydro', {'value': site.code}),))
                  for site in modeleprevision.siteshydro]
    story = _collections.OrderedDict((
        ('CdContact', {'value': cdcontact}),
        ('CdModelePrevision', {'value': modeleprevision.code}),
        ('LbModelePrevision', {'value': modeleprevision.libelle}),
        ('TypModelePrevision', {'value': modeleprevision.typemodele}),
        ('DescModelePrevision', {'value': modeleprevision.description}),
        ('DtMajModelePrevision',
         {'value': datetime2iso(modeleprevision.dtmaj)}),
        ('SitesHydro', {'list': siteshydro, 'subtag': 'SiteHydro'})
    ))
    return story


def seuilhydro_to_story(seuilhydro, bdhydro=False, strict=True):
    """Return a story from a seuil.Seuilhydro."""

    tags = _sandre_tags.SandreTagsV2
    # prerequisites
    if strict:
        _required(seuilhydro, ['code'])

    if seuilhydro.sitehydro is not None:
        forcesitehydro = True
    else:
        forcesitehydro = False

    publication = seuilhydro.publication

    valeurs = [_valeurseuil_to_story(valeur, bdhydro, strict) for valeur in seuilhydro.valeurs]

    # template for seuilhydro simple element
    story = _collections.OrderedDict((
        (tags.cdseuilhydro, {'value': seuilhydro.code}),
        ('SiteHydro', {'value': None, 'force': forcesitehydro}),
        (tags.typseuilhydro, {'value': seuilhydro.typeseuil}),
        (tags.natureseuilhydro, {'value': seuilhydro.nature}),
        (tags.dureeseuilhydro, {'value': seuilhydro.duree}),
        (tags.lbusuelseuilhydro, {'value': seuilhydro.libelle}),
        (tags.mnseuilhydro, {'value': seuilhydro.mnemo}),
        (tags.typpubliseuilhydro, {'value': publication}),
        (tags.indicegraviteseuilhydro, {'value': seuilhydro.gravite}),
        (tags.valforceeseuilhydro, {
            'value': seuilhydro.valeurforcee
            if seuilhydro.valeurforcee is not None else None})))

    story[tags.dtmajseuilhydro] = {
            'value': datetime2iso(seuilhydro.dtmaj)}
    story[tags.comseuilhydro] = {'value': seuilhydro.commentaire}

    # add the stations values
    if len(seuilhydro.valeurs) > 0:
        story[tags.valsseuilhydro] = {
            'value': None, 'list': valeurs, 'subtag': 'ValSeuilHydro'}

    # add sitehydro in SandreV2
    if forcesitehydro:
        story['SiteHydro'] = {
            'sub': _collections.OrderedDict((
                ('CdSiteHydro', {'value': seuilhydro.sitehydro.code}),
                ('LbSiteHydro', {'value': seuilhydro.sitehydro.libelle})))}

    return story


def _valeurseuil_to_story(valeurseuil, bdhydro=False, strict=True):
    """Return a story from a seuil.Valeurseuil.

    Requires valeurseuil.entite.code to be a station hydro code.

"""
    # prerequisites
    if strict:
        _required(valeurseuil, ['entite', 'valeur'])
        # _required(valeurseuil.entite, ['code'])

    if not isinstance(valeurseuil.entite,
                      (_sitehydro.Station, _sitehydro.Sitehydro,
                       _sitehydro.Capteur, _sitemeteo.Grandeur)):
        raise TypeError(
            'valeurseuil.entite is not a Sitehydro'
            ' or a Station or a Capteur or a Grandeur')
    if isinstance(valeurseuil.entite, _sitemeteo.Grandeur):
        typeseuil = 'SeuilMeteo'
    else:
        typeseuil = 'SeuilHydro'

    # template for valeurseuilstation simple element
    story = _collections.OrderedDict((
        ('ValVal' + typeseuil, {
            'value': valeurseuil.valeur}),
        ('ToleranceVal' + typeseuil, {'value': valeurseuil.tolerance}),
        ('DtActivationVal' + typeseuil, {
            'value': datetime2iso(valeurseuil.dtactivation)}),
        ('DtDesactivationVal' + typeseuil, {
            'value': datetime2iso(valeurseuil.dtdesactivation)})))

    if isinstance(valeurseuil.entite, _sitehydro.Station):
        story['StationHydro'] = {
            'sub': _collections.OrderedDict((
                ('CdStationHydro', {'value': valeurseuil.entite.code}),
                ('LbStationHydro', {'value': valeurseuil.entite.libelle})))
            }
    if isinstance(valeurseuil.entite, _sitehydro.Sitehydro):
        story['SiteHydro'] = {
            'sub': _collections.OrderedDict((
                ('CdSiteHydro', {'value': valeurseuil.entite.code}),
                ('LbSiteHydro', {'value': valeurseuil.entite.libelle})))}
    if isinstance(valeurseuil.entite, _sitehydro.Capteur):
        story['Capteur'] = {
            'sub': _collections.OrderedDict((
                ('CdCapteur', {'value': valeurseuil.entite.code}),
                ('LbCapteur', {'value': valeurseuil.entite.libelle})))}
    return story


def seuilmeteo_to_story(seuilmeteo, bdhydro=False, strict=True):
    """Return a stroy from a seuil.Seuilmeteo"""
    if seuilmeteo is None:
        return

    tags = _sandre_tags.SandreTagsV2

    grd = seuilmeteo.grandeurmeteo
    cdsitemeteo = _codesitemeteo_to_value(grd.sitemeteo, bdhydro, strict)

    dicsite = _collections.OrderedDict((
        ('CdSiteMeteo', {'value': cdsitemeteo}),
        ('LbSiteMeteo', {'value': grd.sitemeteo.libelle})
        ))
    dicgrd = _collections.OrderedDict((
        ('DtMiseServiceGrdMeteo',
         {'value': datetime2iso(grd.dtmiseservice)}),
        ('CdGrdMeteo', {'value': grd.typemesure})
        ))
    rulessite = {'value': None, 'sub': dicsite}
    rulesgrd = {'value': None, 'sub': dicgrd}

    valeurs = [_valeurseuil_to_story(valeur, bdhydro, strict) for valeur in seuilmeteo.valeurs]

    # template for seuilhydro simple element
    story = _collections.OrderedDict((
        (tags.cdseuilmeteo, {'value': seuilmeteo.code}),
        ('SiteMeteo', rulessite),
        ('GrdMeteo', rulesgrd),
        (tags.typseuilmeteo, {'value': seuilmeteo.typeseuil}),
        (tags.natureseuilmeteo, {'value': seuilmeteo.nature}),
        (tags.dureeseuilmeteo, {'value': seuilmeteo.duree}),
        (tags.lbusuelseuilmeteo, {'value': seuilmeteo.libelle}),
        (tags.mnseuilmeteo, {'value': seuilmeteo.mnemo}),
        (tags.indicegraviteseuilmeteo, {'value': seuilmeteo.gravite})))

    story[tags.dtmajseuilmeteo] = {'value': datetime2iso(seuilmeteo.dtmaj)}
    story[tags.comseuilmeteo] = {'value': seuilmeteo.commentaire}

    if len(seuilmeteo.valeurs) > 0:
        story['ValsSeuilMeteo'] = {'value': None, 'list': valeurs, 'subtag': 'ValSeuilMeteo'}
    return story


def evenement_to_story(evenement, bdhydro=False, strict=True):
    """Return a story from a evenement.Evenement."""

    # prerequisite
    _required(evenement, ['contact', 'entite', 'dt'])
    if strict:
        _required(evenement.contact, ['code'])
        _required(evenement.entite, ['code'])
        _required(evenement, ['descriptif'])

    ressources = [_ressource_to_story(ressource, bdhydro, strict)
                  for ressource in evenement.ressources]
    # template for serie simple elements
    story = _collections.OrderedDict()
    story['CdContact'] = {'value': evenement.contact.code}
    # entite can be a Sitehydro, a Station or a Sitemeteo
    if isinstance(evenement.entite, _sitemeteo.Sitemeteo):
        code = _codesitemeteo_to_value(sitemeteo=evenement.entite,
                                       bdhydro=bdhydro, strict=strict)
    else:
        code = evenement.entite.code

    story['Cd{}'.format(CLS_MAPPINGS[evenement.entite.__class__])] = {
        'value': code}
    # suite
    story['DtEvenement'] = {
        'value': datetime2iso(evenement.dt)}

    story['TypEvenement'] = {'value': evenement.typeevt}

    story['DescEvenement'] = {'value': evenement.descriptif}

    story['TypPubliEvenement'] = {'value': evenement.publication}

    story['DtMajEvenement'] = {
        'value': None if evenement.dtmaj is None
        else datetime2iso(evenement.dtmaj)}

    story['RessEvenement'] = {'value': None, 'list': ressources, 'subtag': 'ResEvenement'}
    if evenement.dtfin is not None:
        story['DtFinEvenement'] = {
            'value': datetime2iso(evenement.dtfin)}
    return story


def _ressource_to_story(ressource, bdhydro=False, strict=True):
    return _collections.OrderedDict((
        ('UrlResEvenement', {'value': ressource.url}),
        ('LbResEvenement', {'value': ressource.libelle})))


def courbetarage_to_story(courbe, bdhydro=False, strict=True):
    """Return a story from a courbetarage.CourbeTarage."""

    tags = _sandre_tags.SandreTagsV2

    # prerequisite
    _required(courbe, ['code', 'libelle', 'typect', 'station'])
    if strict:
        pass
#            _required(courbe.contact, ['code'])
#            _required(courbe.entite, ['code'])
#            _required(courbe, ['descriptif'])

    pivots = [_pivotct_to_story(pivot, strict) for pivot in courbe.pivots]
    periodes = [_periodect_to_story(periode, strict, tags) for periode in courbe.periodes]
    # template for serie simple elements
    story = _collections.OrderedDict()
    story['CdCourbeTarage'] = {'value': courbe.code}
    story['LbCourbeTarage'] = {'value': courbe.libelle}
    story['TypCourbeTarage'] = {'value': courbe.typect}
    story['DtCreatCourbeTarage'] = {
            'value': datetime2iso(courbe.dtcreation) if courbe.dtcreation is not None else None
    }
    story['LimiteInfCourbeTarage'] = {'value': courbe.limiteinf}
    story['LimiteSupCourbeTarage'] = {'value': courbe.limitesup}
    story['LimiteInfPubCourbeTarage'] = {'value': courbe.limiteinfpub}
    story['LimiteSupPubCourbeTarage'] = {'value': courbe.limitesuppub}
    story['DnCourbeTarage'] = {'value': courbe.dn}
    story['AlphaCourbeTarage'] = {'value': courbe.alpha}
    story['BetaCourbeTarage'] = {'value': courbe.beta}
    story['ComCourbeTarage'] = {'value': courbe.commentaire}
    story['CdStationHydro'] = {'value': courbe.station.code}
    story['CdContact'] = {
        'value': getattr(
            getattr(courbe, 'contact', None), 'code', None)}
    story['PivotsCourbeTarage'] = {'value': None, 'list': pivots, 'subtag': 'PivotCourbeTarage'}
    story['PeriodesUtilisationCourbeTarage'] = {'value': None, 'list': periodes,
                                                'subtag': 'PeriodeUtilisationCourbeTarage'}
    story['DtMajCourbeTarage'] = {
        'value': None if courbe.dtmaj is None
        else datetime2iso(courbe.dtmaj)}
    story['ComPrivCourbeTarage'] = {'value': courbe.commentaireprive}
    return story


def _pivotct_to_story(pivot, strict=True):
    _required(pivot, ['hauteur'])
    story = _collections.OrderedDict()
    story['HtPivotCourbeTarage'] = {'value': pivot.hauteur}

    if isinstance(pivot, _courbetarage.PivotCTPoly):
        _required(pivot, ['debit'])
        story['QPivotCourbeTarage'] = {'value': pivot.debit}
    elif isinstance(pivot, _courbetarage.PivotCTPuissance):
        _required(pivot, ['vara', 'varb', 'varh'])
        story['VarAPivotCourbeTarage'] = {'value': pivot.vara}
        story['VarBPivotCourbeTarage'] = {'value': pivot.varb}
        story['VarHPivotCourbeTarage'] = {'value': pivot.varh}

    else:
        raise TypeError('pivot is not a PivotCTPoly or a PivotCTPuissance')
    return story


def _periodect_to_story(periode, strict=True, tags=_sandre_tags.SandreTagsV2):
    _required(periode, ['dtdeb', 'etat'])
    histos = [_histoperiode_to_story(histo, tags) for histo in periode.histos]
    story = _collections.OrderedDict()
    story[tags.dtdebperiodeutilct] = {
        'value': datetime2iso(periode.dtdeb)}

    story['DtFinPeriodeUtilisationCourbeTarage'] = {
            'value': datetime2iso(periode.dtfin) if periode.dtfin is not None else None
    }

    story['EtatPeriodeUtilisationCourbeTarage'] = {
            'value': periode.etat if periode.etat is not None else None
    }

    story[tags.histosactivationperiode] = {'value': None, 'list': histos,
                                           'subtag': 'HistoActivationPeriode'}
    return story


def _histoperiode_to_story(histo, tags=_sandre_tags.SandreTagsV2):
    _required(histo, ['dtactivation'])
    story = _collections.OrderedDict()
    story[tags.dtactivationhistoperiode] = {
        'value': datetime2iso(histo.dtactivation)
    }

    story[tags.dtdesactivationhistoperiode] = {
            'value': datetime2iso(histo.dtdesactivation)
            if histo.dtdesactivation is not None else None
    }
    return story


def jaugeage_to_story(jaugeage, bdhydro=False, strict=True):
    """Return a story from a jaugeage.Jaugeage."""

    _required(jaugeage, ['site', 'dtdeb'])
    tags = _sandre_tags.SandreTagsV2
    if strict:
        _required(jaugeage.site, ['code'])

    hauteurs = [_hjaug_to_story(hauteur, strict=strict) for hauteur in jaugeage.hauteurs]
    courbes = [_simplecourbetarage_to_story(courbe) for courbe in jaugeage.courbestarage]
    # template for seriehydro simple elements
    story = _collections.OrderedDict()
    story['CdJaugeage'] = {'value': jaugeage.code}
    story['DebitJaugeage'] = {'value': jaugeage.debit}
    story['DtDebJaugeage'] = {
        'value': datetime2iso(jaugeage.dtdeb) if jaugeage.dtdeb is not None else None
    }
    story['DtFinJaugeage'] = {
        'value': datetime2iso(jaugeage.dtfin) if jaugeage.dtfin is not None else None
    }
    story['SectionMouilJaugeage'] = {'value': jaugeage.section_mouillee}
    story['PerimMouilleJaugeage'] = {'value': jaugeage.perimetre_mouille}
    story['LargMiroirJaugeage'] = {'value': jaugeage.largeur_miroir}

    story['ModeJaugeage'] = {'value': jaugeage.mode}

    story['ComJaugeage'] = {'value': jaugeage.commentaire}
    story['VitesseMoyJaugeage'] = {'value': jaugeage.vitessemoy}
    story['VitesseMaxJaugeage'] = {'value': jaugeage.vitessemax}
    story[tags.vitessemaxsurface] = {'value': jaugeage.vitessemax_surface}
    # TODO fuzzy mode
    story['CdSiteHydro'] = {'value': jaugeage.site.code}

    # story['HauteursJaugeage']
    if len(jaugeage.hauteurs) > 0:
        story['HauteursJaugeage'] = {'value': None, 'list': hauteurs, 'subtag': 'HauteurJaugeage'}

    story['DtMajJaugeage'] = {
        'value': datetime2iso(jaugeage.dtmaj) if jaugeage.dtmaj is not None else None
    }

    story['NumJaugeage'] = {'value': jaugeage.numero}

    story['IncertCalJaugeage'] = {'value': jaugeage.incertitude_calculee}

    story['IncertRetenueJaugeage'] = {'value': jaugeage.incertitude_retenue}

    story['QualifJaugeage'] = {'value': jaugeage.qualification}

    story['ComPrivJaugeage'] = {'value': jaugeage.commentaire_prive}
    if len(jaugeage.courbestarage) > 0:
        story['CourbesTarage'] = {'value': None, 'list': courbes, 'subtag': 'CourbeTarage'}
    return story


def _simplecourbetarage_to_story(courbe):
    return _collections.OrderedDict((
        ('CdCourbeTarage', {'value': courbe.code}),
        ('LbCourbeTarage', {'value': courbe.libelle})
    ))


def _hjaug_to_story(hjaug, strict=True):
    _required(hjaug, ['station', 'sysalti', 'coteretenue'])
    if strict:
        _required(hjaug.station, ['code'])
        if hjaug.stationfille is not None:
            _required(hjaug.stationfille, ['code'])
    # template for seriehydro simple elements
    story = _collections.OrderedDict()
    story['CdStationHydro'] = {'value': hjaug.station.code}
    story['SysAltiStationJaugeage'] = {'value': hjaug.sysalti}
    story['CoteRetenueStationJaugeage'] = {'value': hjaug.coteretenue}
    story['CoteDebutStationJaugeage'] = {'value': hjaug.cotedeb}
    story['CoteFinStationJaugeage'] = {'value': hjaug.cotefin}
    story['DnStationJaugeage'] = {'value': hjaug.denivele}
    story['DistanceStationJaugeage'] = {'value': hjaug.distancestation}
    if hjaug.stationfille is not None:
        stationfille = _collections.OrderedDict((
            ('CdStationHydro', {'value': hjaug.stationfille.code}),
        ))
        story['StationFille'] = {'value': None, 'sub': stationfille}

    story['DtDebutRefAlti'] = {
        'value': datetime2iso(hjaug.dtdeb_refalti) if hjaug.dtdeb_refalti is not None else None
    }
    return story


def courbecorrection_to_story(courbe, bdhydro=False, strict=True):
    """Return a story from a courbecorrection.CourbeCorrection."""
    # prerequisite
    _required(courbe, ['station'])
    if strict:
        _required(courbe.station, ['code'])

    pivots = [_pivotcc_to_story(pivot, strict=strict) for pivot in courbe.pivots]
    # template for seriehydro simple elements
    story = _collections.OrderedDict()
    # TODO fuzzy mode
    story['CdStationHydro'] = {'value': courbe.station.code}
    story['LbCourbeCorrH'] = {'value': courbe.libelle}
    story['ComCourbeCorrH'] = {'value': courbe.commentaire}
    # story['PointsPivot']
    story['PointsPivot'] = {'value': None, 'list': pivots, 'subtag': 'PointPivot'}
    story['DtMajCourbeCorrH'] = {
        'value': datetime2iso(courbe.dtmaj) if courbe.dtmaj is not None else None
    }
    return story


def _pivotcc_to_story(pivotcc, strict=True):
    """Return a story from a courbecorrection.PivotCC"""
    _required(pivotcc, ['dte', 'deltah'])

    tags = _sandre_tags.SandreTagsV2

    story = _collections.OrderedDict()

    story['DtPointPivot'] = {'value': datetime2iso(pivotcc.dte)}
    story['DeltaHPointPivot'] = {'value': pivotcc.deltah}
    story['DtActivationPointPivot'] = {
        'value': datetime2iso(pivotcc.dtactivation) if pivotcc.dtactivation is not None else None
    }
    story[tags.dtdesactivationpointpivot] = {
        'value': datetime2iso(pivotcc.dtdesactivation)
        if pivotcc.dtdesactivation is not None else None
    }
    return story


def seriehydro_to_story(seriehydro, bdhydro=False, strict=True):
    """Return a story from a obshydro.Serie."""

    tags = _sandre_tags.SandreTagsV2
    # prerequisite
    _required(seriehydro, ['entite'])

    if strict:
        _required(seriehydro.entite, ['code'])
        _required(seriehydro, ['grandeur'])

    # template for seriehydro simple elements
    story = _collections.OrderedDict()
    # entite can be a Sitehydro, a Station or a Capteur
    story['Cd{}'.format(CLS_MAPPINGS[seriehydro.entite.__class__])] = {
        'value': seriehydro.entite.code}
    # suite
    story[tags.grdseriehydro] = {'value': seriehydro.grandeur}
    if seriehydro.dtdeb is not None:
        story[tags.dtdebseriehydro] = {
            'value': datetime2iso(seriehydro.dtdeb)}
    if seriehydro.dtfin is not None:
        story[tags.dtfinseriehydro] = {
            'value': datetime2iso(seriehydro.dtfin)}

    if seriehydro.dtprod is not None:
        story[tags.dtprodseriehydro] = {
            'value': datetime2iso(seriehydro.dtprod)}

    if seriehydro.sysalti is not None:
        story[tags.sysaltiseriehydro] = {'value': seriehydro.sysalti}
    if seriehydro.perime is not None:
        story[tags.serieperimhydro] = {
            'value': seriehydro.perime}

    if seriehydro.pdt is not None:
        story[tags.pdtseriehydro] = {'value': seriehydro.pdt.to_int()}

    story['CdContact'] = {
        'value': getattr(
            getattr(seriehydro, 'contact', None), 'code', None)}

    # add the observations
    if seriehydro.observations is not None:
        obss = [_observation_to_story(obs, bdhydro, strict)
                for obs in seriehydro.observations.itertuples()]
        story['ObssHydro'] = {'list': obss, 'subtag': 'ObsHydro'}
    return story


def _observation_to_story(obs, bdhydro=False, strict=True):
    """Return a strory from tuple obshydro.Observation."""
    return _collections.OrderedDict((
        ('DtObsHydro', {'value': datetime2iso(obs.Index)}),
        ('ResObsHydro', {'value': obs.res}),
        ('QualifObsHydro', {'value': obs.qal}),
        ('MethObsHydro', {'value': obs.mth}),
        ('ContObsHydro', {'value': obs.cnt}),
        ('StObsHydro', {'value': obs.statut})
    ))


def seriemeteo_to_story(serie, bdhydro=False, strict=True):
    """Return a story element from obsmeteo.Serie."""
    code = _codesitemeteo_to_value(sitemeteo=serie.grandeur.sitemeteo,
                                   bdhydro=bdhydro,
                                   strict=strict)
    story = _collections.OrderedDict((
        ('CdSiteMeteo', {'value': code}),
        ('CdGrdMeteo', {'value': serie.grandeur.typemesure}),
        ('DureeSerieObsMeteo', {'value': int(serie.duree.total_seconds() / 60)}),
    ))

    if serie.dtdeb is not None:
        story['DtDebSerieObsMeteo'] = {'value': datetime2iso(serie.dtdeb)}
    if serie.dtfin is not None:
        story['DtFinSerieObsMeteo'] = {'value': datetime2iso(serie.dtfin)}
    if serie.dtprod is not None:
        story['DtProdSerieObsMeteo'] = {'value': datetime2iso(serie.dtprod)}
    if serie.contact is not None:
        story['CdContact'] = {'value': serie.contact.code}
    if serie.observations is not None:
        obss = [_obsmeteo_to_story(obs) for obs in serie.observations.itertuples()]
        story['ObssMeteo'] = {'list': obss, 'subtag': 'ObsMeteo'}
    return story


def _obsmeteo_to_story(obs):
    """Return a story from tuple obsmeteo.Observation."""
    story = _collections.OrderedDict((
        ('DtObsMeteo', {'value': datetime2iso(obs.Index)}),
        ('ResObsMeteo', {'value': obs.res})
    ))
    if not _math.isnan(obs.qua):
        story['IndiceQualObsMeteo'] = {'value': int(obs.qua)}
    story['ContxtObsMeteo'] = {'value': obs.ctxt}
    story['QualifObsMeteo'] = {'value': obs.qal}
    story['MethObsMeteo'] = {'value': obs.mth}
    story['StObsMeteo'] = {'value': obs.statut}
    return story


def serieobselab_to_story(serieobselab):
    """Return a story from obselaboreehydro.SerieObsElab."""
    story = _collections.OrderedDict((
        ('TypDeGrdSerieObsElaborHydro', {'value': serieobselab.typegrd}),
    ))

    if serieobselab.pdt is not None:
        story['PDTSerieObsElaborHydro'] = {'value': serieobselab.pdt.to_int()}

    story['DtDebPlagSerieObsElaborHydro'] = {'value': datetime2iso(serieobselab.dtdeb)}
    story['DtFinPlagSerieObsElaborHydro'] = {'value': datetime2iso(serieobselab.dtfin)}
    story['DtProdSerieObsElaborHydro'] = {'value': datetime2iso(serieobselab.dtprod)}
    story['DtDesactivationSerieObsElaborHydro'] = {
        'value': datetime2iso(serieobselab.dtdesactivation)}
    story['DtActivationSerieObsElaborHydro'] = {'value': datetime2iso(serieobselab.dtactivation)}
    story['SysAltiSerieObsElaborHydro'] = {'value': serieobselab.sysalti}
    story['GlissanteSerieObsElaborHydro'] = {'value': serieobselab.glissante}
    story['DtDebutRefAlti'] = {'value': datetime2iso(serieobselab.dtdebrefalti)}

    if serieobselab.contact is not None:
        story['CdContact'] = {'value': serieobselab.contact.code}

    if isinstance(serieobselab.entite, _sitehydro.Sitehydro):
        story['CdSiteHydro'] = {'value': serieobselab.entite.code}
    else:
        story['CdStationHydro'] = {'value': serieobselab.entite.code}

    if serieobselab.observations is not None:
        obss = [_obselab_to_story(obs) for obs in serieobselab.observations.itertuples()]
        story['ObssElaborHydro'] = {'list': obss, 'subtag': 'ObsElaborHydro'}
    return story


def _obselab_to_story(obselab):
    """Return a story from tuple obselaboreehydro.ObservationElaboree."""
    return _collections.OrderedDict((
        ('DtObsElaborHydro', {'value': datetime2iso(obselab.Index)}),
        ('DtResObsElaborHydro', {'value': datetime2iso(obselab.dtres)}),
        ('ResObsElaborHydro', {'value': obselab.res}),
        ('QualifObsElaborHydro', {'value': obselab.qal}),
        ('MethObsElaborHydro', {'value': obselab.mth}),
        ('ContObsElaborHydro', {'value': obselab.cnt}),
        ('StObsElaborHydro', {'value': obselab.statut})
    ))


def serieobselabmeteo_to_sory(seriemeteo, bdhydro=False, strict=True):
    """Return a story from obselaboreemeteo.SerieObsElabMeteo."""
    story = _collections.OrderedDict()

    if isinstance(seriemeteo.site, _sitehydro.Sitehydro):
        story['CdSiteHydro'] = {'value': seriemeteo.site.code}
    elif isinstance(seriemeteo.site, _sitemeteo.SitemeteoPondere):
        story['CdSiteMeteo'] = {'value': _codesitemeteo_to_value(seriemeteo.site, bdhydro, strict)}
        story['ValPondSiteMeteo'] = {'value': seriemeteo.site.ponderation}
    story['CdGrdSerieObsElaborMeteo'] = {'value': seriemeteo.grandeur}
    story['TypSerieObsElaborMeteo'] = {'value': seriemeteo.typeserie}

    story['DtDebSerieObsElaborMeteo'] = {'value': datetime2iso(seriemeteo.dtdeb)}
    story['DtFinSerieObsElaborMeteo'] = {'value': datetime2iso(seriemeteo.dtfin)}
    if seriemeteo.duree is not None:
        story['DureeSerieObsElaborMeteo'] = {'value': int(seriemeteo.duree.total_seconds() / 60)}

    if seriemeteo.ipa is not None:
        ipa = _ipa_to_story(seriemeteo.ipa)
        story['SerieObsElaborMeteoIpa'] = {'sub': ipa}

    if seriemeteo.observations is not None:
        obss = [_obsmeteoelab_tostory(obs) for obs in seriemeteo.observations.itertuples()]
        story['ObssElaborMeteo'] = {'list': obss, 'subtag': 'ObsElaborMeteo'}
    return story


def _obsmeteoelab_tostory(obs):
    """Return a story from a tuple obselaboreemeteo.ObsElabMeteo."""
    story = _collections.OrderedDict((
        ('DtObsElaborMeteo', {'value': datetime2iso(obs.Index)}),
        ('ResObsElaborMeteo', {'value': obs.res}),
    ))
    if not _numpy.isnan(obs.qua):
        story['IndiceQualObsElaborMeteo'] = {'value': obs.qua}
    story['QualifObsElaborMeteo'] = {'value': obs.qal}
    story['MethObsElaborMeteo'] = {'value': obs.mth}
    story['StObsElaborMeteo'] = {'value': obs.statut}

    return story


def _ipa_to_story(ipa):
    """Return a story from an obselaboreemeteo.IPA."""
    return _collections.OrderedDict((
        ('KSerieObsElaborMeteoIpa', {'value': ipa.coefk}),
        ('PDTSerieObsElaborMeteoIpa', {'value': ipa.npdt})
    ))


def seriesgradients_to_story(seriesgradients, bdhydro=False, strict=True):
    """Return a story from a list of gradienthydro.SerieGradients."""
    tags = _sandre_tags.SandreTagsV2

    grd_series = {}
    for serie in seriesgradients:
        if serie.grd in grd_series:
            grd_series[serie.grd].append(serie)
        else:
            grd_series[serie.grd] = [serie]
    grds = []
    for grd, series in grd_series.items():
        story = _collections.OrderedDict((
            ('GrdGradHydro', {'value': grd}),
        ))
        gradients = []
        for serie in series:
            dtprod = datetime2iso(serie.dtprod)
            for grad in serie.gradients.itertuples():
                # template for seuilhydro simple element
                gradient = _collections.OrderedDict((
                    ('DtProdGradHydro', {'value': dtprod}),
                    ('DtObsGradHydro', {'value': datetime2iso(grad.Index)}),
                    ('DureeGradHydro', {'value': serie.duree}),
                    ('ResGradHydro', {'value': grad.res}),
                    (tags.stgradhydro, {'value': grad.statut}),
                    ('QualifGradHydro', {'value': grad.qal}),
                    ('MethQualifGradHydro', {'value': grad.mth})))
                if isinstance(serie.entite, _sitehydro.Sitehydro):
                    gradient['CdSiteHydro'] = {'value': serie.entite.code}
                elif isinstance(serie.entite, _sitehydro.Station):
                    gradient['CdStationHydro'] = {'value': serie.entite.code}
                elif isinstance(serie.entite, _sitehydro.Capteur):
                    gradient['CdCapteur'] = {'value': serie.entite.code}
                if serie.contact is not None:
                    gradient['CdContact'] = {'value': serie.contact.code}
                gradients.append(gradient)
        story['GradHydro'] = {'list': gradients}
        grds.append(story)
    return grds


def validsannee_to_story(validsannee, bdhydro=False, strict=True):
    """Return a story from a ValidsAnnee object."""
    stories = []
    for vannee in validsannee.itertuples():
        cdentite = vannee.Index[0]
        if len(cdentite) == 8:
            tagentite = 'CdSiteHydro'
        else:
            tagentite = 'CdStationHydro'
        dtmaj = datetime2iso(vannee.dtmaj) if _pandas.notnull(vannee.dtmaj) else None
        dispoh = int(vannee.dispoh) if _pandas.notnull(vannee.dispoh) else None
        dispoq = int(vannee.dispoq) if _pandas.notnull(vannee.dispoq) else None
        story = _collections.OrderedDict((
            ('AnneeValidAnneeHydro', {'value': vannee.Index[1]}),
            (tagentite, {'value': cdentite}),
            ('QualifValidAnneeHydro', {'value': vannee.qualif}),
            ('DispoHValidAnneeHydro', {'value': dispoh}),
            ('DispoQValidAnneeHydro', {'value': dispoq}),
            ('ComValidAnneeHydro', {'value': vannee.cmnt}),
            ('DtMajValidAnneeHydro', {'value': dtmaj})))
        stories.append(story)
    return stories


def simulation_to_story(simulation, bdhydro=False, strict=True):
    """Return a story from a simulation.Simulation."""

    # prerequisite
    _required(
        simulation, ['dtprod', 'entite', 'intervenant', 'modeleprevision'])
    if strict:
        _required(simulation.entite, ['code'])
        _required(simulation, ['grandeur'])
        _required(simulation.modeleprevision, ['code'])
        _required(simulation.intervenant, ['code'])
    if bdhydro:
        _required(simulation, ['statut'])

    # template for simulation simple element
    cdentite = 'Cd{}'.format(CLS_MAPPINGS[simulation.entite.__class__])
    story = _collections.OrderedDict((
        (cdentite, {'value': simulation.entite.code}),
        ('DtProdSimul', {
            'value': datetime2iso(simulation.dtprod)}),
        ('GrdSimul', {'value': simulation.grandeur}),
        ('CdSimul', {'value': simulation.code}),
        ('IndiceQualSimul', {'value': simulation.qualite}),
        ('DtFinValidSimul', {'value': datetime2iso(simulation.dtfinvalidite)}),
        ('DtDebSimul', {'value': datetime2iso(simulation.dtdeb)}),
        ('DtFinSimul', {'value': datetime2iso(simulation.dtfin)}),
        ('DtBaseSimul', {'value': datetime2iso(simulation.dtbase)}),
        ('DtDerObsSimul', {'value': datetime2iso(simulation.dtderobs)}),
        ('ModeCalSimul', {'value': simulation.modecalcul}),
        ('StSimul', {'value': simulation.statut}),
        ('TypPubliSimul', {'value': simulation.publication}),
        ('SysAltiSimul', {'value': simulation.sysalti}),
        ('ContexteSimul', {'value': simulation.contexte}),
        ('ComSimul', {'value': simulation.commentaire}),
        ('ComPrivSimul', {'value': simulation.cmntprive}),
        ('ModeSimul', {'value': simulation.mode}),
        ('CdModelePrevision', {'value': simulation.modeleprevision.code}),
        ('CdContact', {'value': simulation.contact.code})))
    # suite
    # story['CdModelePrevision'] = {'value': simulation.modeleprevision.code}
    story['CdIntervenant'] = {
        'value': str(simulation.intervenant.code),
        'attr': {"schemeAgencyID": simulation.intervenant.origine}}
    prevs_tend = _previsions_tend_to_stories(simulation.previsions_tend)
    if prevs_tend:
        story['PrevsTendance'] = {'list': prevs_tend, 'subtag': 'PrevTendance'}

    prevs_det = _previsions_det_to_stories(simulation.previsions_det)
    if prevs_tend:
        story['PrevsDeterministe'] = {'list': prevs_det, 'subtag': 'PrevDeterministe'}

    prevs_prb = _previsions_prb_to_stories(simulation.previsions_prb)
    if prevs_tend:
        story['PrevsProb'] = {'list': prevs_prb, 'subtag': 'PrevProb'}

    if simulation.scenario is not None:
        story['ScnSimul'] = {'sub': _scenario_simul_to_story(simulation.scenario)}

    prevs_ensemble = _prevs_ensemble_to_stories(simulation.prevs_ensemble)
    if prevs_tend:
        story['PrevsEnsemble'] = {'list': prevs_ensemble, 'subtag': 'PrevEnsemble'}

    prevs_evol = _prevs_evol_to_stories(simulation.prevs_evol)
    if prevs_tend:
        story['PrevsEvol'] = {'list': prevs_evol, 'subtag': 'PrevEvol'}
    # return
    return story


def _previsions_tend_to_stories(previsions_tend):
    """Return a story from a simulation.PrevisionsTendance."""
    stories = []
    if previsions_tend is None:
        return stories
    # iter by date and add the previsions
    for dte in previsions_tend.index.levels[0].values:
        story = _collections.OrderedDict()
        # dte is mandatory...
        story['DtPrevTendance'] = {'value': datetime2iso(_numpy.datetime64(dte, 's').item())}
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # for one date we can have multiple values
        # we put all of them in a dict {prb: res, ...}
        # so that we can pop them on by one
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        prevs = previsions_tend.loc[dte]

        mapping = (
            ('moy', 'ResMoyPrevTendance'),
            ('min', 'ResMinPrevTendance'),
            ('max', 'ResMaxPrevTendance'),
        )
        # we begin to deal with the direct tags...
        # order matters: moy, min and max !!
        incertdte = None
        for tend, tag in mapping:
            if tend in prevs.index:
                values = prevs.loc[tend].to_dict()
                story[tag] = {'value': values['res']}
                incertdte = int(values['incertdte'])

        if incertdte is not None and incertdte != 0:
            story['IncertDtPrevTendance'] = {'value': incertdte}
        stories.append(story)

    # return
    return stories


def _previsions_det_to_stories(previsions_det):
    """Return a story from a simulation.PrevisionsDeterministes."""
    stories = []
    if previsions_det is None:
        return stories

    for prev in previsions_det.itertuples():
        story = _collections.OrderedDict((
            ('DtPrevDeterministe', {'value': datetime2iso(prev.Index)}),
            ('ResPrevDeterministe', {'value': prev.res})
        ))
        if prev.incertdte is not None and prev.incertdte != 0:
            story['IncertDtPrevDeterministe'] = {'value': prev.incertdte}

        stories.append(story)
    # for prev in previsions_det.items():
    #     print(prev)
    return stories


def _previsions_prb_to_stories(previsions_prb):
    """Return a story from a simulation.PrevisionsPrb."""
    stories = []
    if previsions_prb is None:
        return stories
    # iter by date and add the previsions
    for dte in previsions_prb.index.levels[0].values:
        story = _collections.OrderedDict()
        story['DtPrevProb'] = {'value': datetime2iso(_numpy.datetime64(dte, 's').item())}
        pprobs = [_collections.OrderedDict((('PProbPrev', {'value': pprob}),
                                            ('ResProbPrev', {'value': res})))
                  for pprob, res in previsions_prb[dte].items()]
        story['ProbsPrev'] = {'list': pprobs, 'subtag': 'ProbPrev'}
        stories.append(story)

    return stories


def _prevs_ensemble_to_stories(prevs_ensemble):
    """Return stories from a simulation.PrevsEnsemble."""
    stories = []
    if prevs_ensemble is None:
        return stories
    # iter by date and add the previsions
    for dte in prevs_ensemble.index.levels[0].values:
        story = _collections.OrderedDict((
            ('DtPrevEnsembliste', {'value': datetime2iso(_numpy.datetime64(dte, 's').item())}),
        ))
        membres = []
        for membre in prevs_ensemble.loc[dte].itertuples():
            membres.append(_collections.OrderedDict((
                ('LbMembrePrevEnsemble', {'value': membre.Index}),
                ('PoidMembrePrevEnsemble', {'value': membre.poids}),
                ('ResMembrePrevEnsemble', {'value': membre.res})
            )))
        story['MembresPrevEnsemble'] = {'list': membres, 'subtag': 'MembrePrevEnsemble'}
        stories.append(story)
    return stories


def _prevs_evol_to_stories(prevs_evol):
    """Return stories from list of PrevEvol"""
    stories = []
    if not prevs_evol:
        return stories
    for prv in prevs_evol:
        stories.append(_collections.OrderedDict((
            ('TypPrevEvol', {'value': prv.evol}),
            ('DtDebPrevEvol', {'value': datetime2iso(prv.dtdeb)}),
            ('IncertDtPrevEvol', {'value': prv.incertdte})
        )))
    return stories


def _scenario_simul_to_story(scenario):
    """Return a story from a ScenarioSimul"""
    if scenario is None:
        return None
    return _collections.OrderedDict((
        ('LbScnSimul', {'value': scenario.libelle}),
        ('DescScnSimul', {'value': scenario.descriptif})
    ))


def datetime2iso(date):
    """Formatage au format iso d'une date supportant les dates avant 1900
    Arguments:
        date (datetime) = date à convertir
    """
    if date is None:
        return None
    return ('{0.year:04d}-{0.month:02d}-{0.day:02d}'
            'T{0.hour:02d}:{0.minute:02d}:{0.second:02d}').format(date)


def date2iso(date):
    """Formatage au format iso d'une date sans l'heure
    supportant les dates avant 1900
    Arguments:
        date (datetime) = date à convertir
    """
    if date is None:
        return None
    return '{0.year:04d}-{0.month:02d}-{0.day:02d}'.format(date)


def _required(obj, attrs):
    """Raise an exception if an attribute is missing or None.

    Arguments:
        obj = the object to test
        attrs (list of strings) = the list of attributes

    """
    for attr in attrs:
        try:
            if getattr(obj, attr) is None:
                raise ValueError()
        except Exception:
            raise ValueError(
                'attribute {attr} is requested with a value other '
                'than None for object {obj}'.format(
                    attr=attr, obj=str(obj)))
    return True
