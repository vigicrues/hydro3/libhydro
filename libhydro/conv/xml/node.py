"""Hydrometry Json and XML are converted to Node."""

from abc import ABC, abstractmethod


class Node(ABC):
    """Abstract/interface class Node.

    This class contains abstract functions to be implemented to extrcact information from an obkect.
    """

    @abstractmethod
    def subobj(self, tag):
        """Get sub object"""
        pass

    @abstractmethod
    def sublist(self, taglist, tagobj=None):
        """Get list os sub objects."""
        pass

    @abstractmethod
    def value(self, tag, cast=str):
        """Get value tag"""
        pass

    @abstractmethod
    def attrib(self, tag, attr):
        """Get attribute associated with tag"""
        pass

    @abstractmethod
    def to_dict(self):
        """Convert Node to dict."""
        pass


class JsonNode(Node):
    """Class encapsulating an associative array created by json.loads

    Properties:
        dic (dict) = tableau associatif produit par json.loads

    """
    def __init__(self, dic):
        self.dic = dic

    def subobj(self, tag):
        """Return  a sub JsonNoe."""
        obj = self.dic.get(tag, None)
        return JsonNode(obj) if obj is not None else None

    def sublist(self, taglist, tagobj=None):
        """Return a list of sub JsonNode."""
        objs = self.dic.get(taglist, []) if taglist is not None else self.dic.get(tagobj, [])
        return [JsonNode(obj) for obj in objs]

    def value(self, tag, cast=str):
        """Return cast(dic[tag]) or None."""
        if self.dic is None:
            return None
        value = self.dic.get(tag, None)
        if value is None:
            return None
        if cast == bool and not isinstance(value, bool):
            # return wether text is a kind of True... or not
            return str(value).lower() in ('true', 'vrai', '1')
        return cast(value)

    def attrib(self, tag, attr):
        """In Json, attributes are assoative keys."""
        return self.value(attr)

    def to_dict(self):
        """Obj is a dict."""
        return self.dic


class XMLNode(Node):
    """Class encapsulating a XML ELement

    Proprietes:
        elt (lxml.Element) = element xml

    """

    def __init__(self, elt):
        self.elt = elt

    def subobj(self, tag):
        """Return a children element or None."""
        elt = self.elt.find(tag)
        return XMLNode(elt)if elt is not None else None

    def sublist(self, taglist, tagobj=None):
        """Return a list of sub elements"""
        elts = self.elt.findall(taglist + '/' + tagobj) if taglist is not None\
            else self.elt.findall(tagobj)
        return [XMLNode(elt) for elt in elts]

    def value(self, tag, cast=str):
        """Return cast(element/tag.text) or None."""
        if self.elt is not None:
            elt = self.elt.find(tag)
            if (elt is not None) and (elt.text is not None):
                if cast == bool:
                    # return wether text is a kind of True... or not
                    return str(elt.text).lower() in ('true', 'vrai', '1')
                # else
                return cast(elt.text)
        return None

    def attrib(self, tag, attr):
        """Return attribute value of tag."""
        elt = self.elt.find(tag)
        if elt is not None:
            return elt.attrib.get(attr, None)
        return None

    def to_dict(self):
        """Simple conversion element to dict."""
        return {child.tag: child.text for child in self.elt}
