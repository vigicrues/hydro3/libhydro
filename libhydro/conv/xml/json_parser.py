"""Module xml.json_parser.

Module permettant de convertir un chaîne json hydrometrie en objets métier.
"""

import json

from libhydro.conv.xml import parser as _parser
from libhydro.conv.xml.node import JsonNode


def parse_json(hydrojson):
    """Parse json string."""
    dic = json.loads(hydrojson)
    return parse_dict(dic)


def parse_dict(hydrodic):
    """Parse hydro dict."""
    return _parser.parse(JsonNode(hydrodic))
