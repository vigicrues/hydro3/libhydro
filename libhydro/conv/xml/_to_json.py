"""Export libhudro objects to hydrometry json."""
import json

from . import _to_story


# -- config -------------------------------------------------------------------
# order matters in XML, we must have the keys list !
ORDERED_ACCEPTED_KEYS = [
    'scenario',
    # line 140: [1:7]
    'intervenants', 'siteshydro', 'sitesmeteo',
    'seuilshydro', 'seuilsmeteo', 'modelesprevision',
    # line 180: [7:]
    'evenements', 'courbestarage', 'jaugeages', 'courbescorrection',
    'serieshydro', 'seriesmeteo', 'seriesobselab', 'seriesobselabmeteo',
    'simulations', 'seriesgradients', 'validsannee']


# -- testsfunction ------------------------------------------------------------
def _to_json(scenario=None, intervenants=None, siteshydro=None, sitesmeteo=None,
             seuilshydro=None, seuilsmeteo=None, modelesprevision=None,
             evenements=None, courbestarage=None, jaugeages=None,
             courbescorrection=None, serieshydro=None, seriesmeteo=None,
             seriesobselab=None, seriesobselabmeteo=None,
             simulations=None, seriesgradients=None, validsannee=None,
             bdhydro=False, strict=True,
             ordered=False, version=None, with_none=False, paths_to_ignore=None):
    """Return a json string a partir des donnees passes en argument.

    Cette fonction est privee et les utilisateurs sont invites a utiliser la
    classe xml.Message comme interface d'ecriture des fichiers XML Hydrometrie.

    Arguments:
        scenario (xml.Scenario) = 1 element
        intervenants (intervenant.Intervenant collection) = iterable or None
        siteshydro (sitehydro.Sitehydro collection) = iterable or None
        sitesmeteo (sitemeteo.Sitemeteo collection) = iterable or None
        seuilshydro (seuil.Seuilhydro collection) = iterable or None
        seuilsmeteo (seuil.Seuilmeteo collection) = iterable or None
        modelesprevision (modeleprevision.Modeleprevision collection) =
            iterable or None
        evenements (evenement.Evenement collection) = iterable ou None
        courbestarage (courbetarage.CourbeTarage collection) = iterable ou None
        jaugeages (jaugeage.Jaugeage collection) = iterable ou None
        courbescorrection (courbecorrection.CourbeCorrection collection) =
            iterable ou None
        serieshydro (obshydro.Serie collection) = iterable or None
        seriesmeteo (obsmeteo.Serie collection) = iterable or None
        seriesobselab(obselaboreehydro.SerieObsElab collection) =
            iterable or None
        seriesobselabmeteo(obselaboreemeteo.SerieObsElabMeteo collection) =
            iterable or None
        seriesgradients(gradienthydro.Seriesgradients collection) =
            iterable or None
        simulations (simulation.Simulation collection) = iterable or None
        validsannee (_validannee.ValidsAnnee) = années hydrométriques
        bdhydro (bool, defaut False) = controle de conformite bdhydro
        strict (bool, defaut True) = controle de conformite XML Hydrometrie
        ordered (bool, default False) = essaie de conserver l'ordre de certains
            elements
        version (str or None) = version Sandre 1.1 ou 2 ou None
            si None utilisation du la version du scenario

    """
    # make a deep copy of locals() which is a dict {arg_name: arg_value, ...}
    # keep only Message items
    # and replace default empty lists with None
    args = {
        k: (v if (k == 'validsannee' or v != []) else None) for k, v in locals().items()
        if k in ORDERED_ACCEPTED_KEYS}

    # TODO - this is awful :/ we should factorize those lines

    if version is None and args['scenario'] is not None:
        version = args['scenario'].version

    story = {}
    # add the scenario
    if args['scenario'] is not None:
        story['Scenario'] = {
            'sub': _to_story.scenario_to_story(scenario, bdhydro, strict)
        }

    refhyd = {}
    # intervenants
    if args['intervenants'] is not None:
        refhyd['Intervenants'] = {
            'list': [_to_story.intervenant_to_story(intervenant, bdhydro, strict)
                     for intervenant in args['intervenants']]}

    # siteshydro
    if args['siteshydro'] is not None:
        refhyd['SitesHydro'] = {
            'list': [_to_story.sitehydro_to_story(sitehydro, bdhydro, strict)
                     for sitehydro in args['siteshydro']]
        }
    if args['sitesmeteo'] is not None:
        refhyd['SitesMeteo'] = {
            'list': [_to_story.sitemeteo_to_story(sitehydro, bdhydro, strict)
                     for sitehydro in args['sitesmeteo']]
        }
    if args['modelesprevision'] is not None:
        refhyd['ModelesPrevision'] = {
            'list': [_to_story.modeleprevision_to_story(modele, bdhydro, strict)
                     for modele in args['modelesprevision']]
        }

    if args['seuilshydro'] is not None:
        refhyd['SeuilsHydro'] = {
            'list': [_to_story.seuilhydro_to_story(seuilhydro=seuilhydro, bdhydro=bdhydro,
                                                   strict=strict)
                     for seuilhydro in args['seuilshydro']]}
    # seuilsmeteo
    if args['seuilsmeteo'] is not None:
        refhyd['SeuilsMeteo'] = {
            'list': [_to_story.seuilmeteo_to_story(seuilmeteo=seuilmeteo, bdhydro=bdhydro,
                                                   strict=strict)
                     for seuilmeteo in args['seuilsmeteo']]
        }
    donnees = {}

    if args['evenements'] is not None:
        donnees['Evenements'] = {
            'list': [_to_story.evenement_to_story(evenement=evenement, bdhydro=bdhydro,
                                                  strict=strict)
                     for evenement in args['evenements']]
        }
    if args['courbestarage'] is not None:
        donnees['CourbesTarage'] = {
            'list': [_to_story.courbetarage_to_story(courbe=courbe, bdhydro=bdhydro, strict=strict)
                     for courbe in args['courbestarage']]
        }
    if args['jaugeages'] is not None:
        donnees['Jaugeages'] = {
            'list': [_to_story.jaugeage_to_story(jaugeage=jaugeage, bdhydro=bdhydro, strict=strict)
                     for jaugeage in args['jaugeages']]
        }
    if args['courbescorrection'] is not None:
        donnees['CourbesCorrH'] = {
            'list': [_to_story.courbecorrection_to_story(courbe=courbe, bdhydro=bdhydro,
                                                         strict=strict)
                     for courbe in args['courbescorrection']]
        }
    if args['serieshydro'] is not None:
        donnees['SeriesObsHydro'] = {
            'list': [_to_story.seriehydro_to_story(seriehydro=seriehydro, bdhydro=bdhydro,
                                                   strict=strict)
                     for seriehydro in args['serieshydro']]
        }
    if args['seriesmeteo'] is not None:
        donnees['SeriesObsMeteo'] = {
            'list': [_to_story.seriemeteo_to_story(serie=serie, bdhydro=bdhydro, strict=strict)
                     for serie in args['seriesmeteo']]
        }
    if args['seriesobselab'] is not None:
        donnees['SeriesObsElaborHydro'] = {
            'list': [_to_story.serieobselab_to_story(serieobselab=serie)
                     for serie in args['seriesobselab']]
        }
    if args['seriesobselabmeteo'] is not None:
        donnees['SeriesObsElaborMeteo'] = {
            'list': [_to_story.serieobselabmeteo_to_sory(seriemeteo=seriemeteo, bdhydro=bdhydro,
                                                         strict=strict)
                     for seriemeteo in args['seriesobselabmeteo']]
        }
    if args['seriesgradients'] is not None:
        gradients = _to_story.seriesgradients_to_story(args['seriesgradients'], bdhydro=bdhydro,
                                                       strict=strict)
        if gradients:
            donnees['GradsHydro'] = {'list': gradients}

    if args['validsannee'] is not None:
        validsannee = _to_story.validsannee_to_story(args['validsannee'], bdhydro=bdhydro,
                                                     strict=strict)
        if validsannee:
            donnees['ValidsAnneeHydro'] = {'list': validsannee}

    if args['simulations'] is not None:
        donnees['Simuls'] = {
            'list': [_to_story.simulation_to_story(simul, bdhydro, strict)
                     for simul in args['simulations']]
        }

    # return
    if refhyd:
        story['RefHyd'] = {'sub': refhyd}
    if donnees:
        story['Donnees'] = {'sub': donnees}
    if paths_to_ignore is not None:
        for path in paths_to_ignore:
            remove_tag(story, path)
    # ('Donnees', 'CourbesTarage', 'DtMajCourbeTarage'))
    # remove_tag(story, ('Donnees', 'CourbesCorrH', 'DtMajCourbeCorrH'))

    return json.dumps(_factory(story, with_none=with_none))


def remove_tag(story, path):
    tag = path[0]
    if tag not in story:
        return
    if len(path) == 1:
        del story[tag]
        return
    story = story[tag]
    if 'sub' in story:
        remove_tag(story['sub'], path[1:])
    elif 'list' in story:
        for obj in story['list']:
            remove_tag(obj, path[1:])


# -- utility functions --------------------------------------------------------
def _factory(story, with_none=False):
    """Return the <root> element including elements described in story.

    Story is a dictionnary which keys are the XML tags to create and values
    one of the possible 3 forms:

        0./ Rule is an _etree_Element
            add the element to root
        1./ Rule to create a sub-element (recursif)
        -------------------------------------------
        This rule is processed like:
            {'sub': a sub-story dictionnary}

        2./ Rule to create a single element or a serie of the same element
        ------------------------------------------------------------------
        This rule is processed like:
            {
                'value': the text value or an iterable of text values,
                'attr': {the tag attributes} (default None)
                'force': bool (default False)
            }

        If value is an iterable, an XML tag is created for each item of values.

        When force is True, a None value create the element tag, otherwise
        rule is left.

    WARNING: as order matters for XML Hydrometrie files, one must use
             collections.OrderedDict to store the story.

    """
    dic = {}
    # parse story
    for tag, rule in story.items():
        # recursif call for sub-element
        if 'sub' in rule:
            # pas de balise vide pour le scenario
            dic[tag] = _factory(rule.get('sub'), False if tag == "Scenario" else with_none)
            continue

        if 'list' in rule:
            if rule['list']:
                # rule['list'] can be empty
                dic[tag] = [_factory(substory, with_none) for substory in rule['list']]
            continue
        # single element or multi elements
        if 'value' in rule:
            # init
            value = rule.get('value')
            attr = rule.get('attr', None)
            force = True if with_none else rule.get('force', False)

            # empty tag
            if (value is None) and (not force):
                continue

            dic[tag] = value
            if attr is not None:
                for key, val in attr.items():
                    dic[key] = val

    # return
    return dic
