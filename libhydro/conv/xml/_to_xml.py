# coding: utf-8
"""Module xml._to_xml.

Ce module contient les fonctions de generation des fichiers au format
XML Hydrometrie (version 1.1 exclusivement).

Toutes les heures sont considerees UTC si le fuseau horaire n'est pas precise.

Les fonctions de ce module sont a usage prive, il est recommande d'utiliser la
classe xml.Message comme interface aux fichiers XML Hydrometrie.

"""
# -- imports ------------------------------------------------------------------
import collections as _collections

from lxml import etree as _etree

from libhydro.conv.xml import _to_story

# -- strings ------------------------------------------------------------------
# contributor Sébastien ROMON
__version__ = '0.6.6'
__date__ = '2022-10-10'

# HISTORY
# V0.6.6 - 2022-10-10
#   do not write incertdte when its value is zero
# SR - 2017-09-29 use itertuples intsead of iterrows
# SR - 2017-09- 25 export type capteur to xml
# V0.6.5 - SR- 2017-09-22
# export entitehydro, tronconhydro, zonehydro
# and precisioncoursdeau of site to xml
# V0.6.4 - SR- 2017-09-19
# export pdt of grandeur
# V0.6.3 - SR- 2017-09-05
# export plages d'utilisatin of station and capteur to xml
# V0.6.2 - SR- 2017-07-18
# export some properties of station to xml
# V0.6.1 - SR - 2017-07-05
# export jaugeages
# V0.6 - SR - 2017-06-20
# export des courbes de correction
# V0.5 - SR - 2017-06-20
# export CourbeTarage
# V0.4.9 - SR - 2017-06-09
# export sysalti and perim Serie properties
# V0.4.8
# export des prévisons de tendance puis des prévisions probabilistes
# V0.4 - 2014-07-31
#   factorize the global functions
#   replace isoformat() by strftime()
#   add the to_xml.bdhydro argument
#   add the modelesprevision element
#   add the required function
# V0.3 - 2014-07-25
#   add the sitesmeteo and seriesmeteo elements
# V0.1 - 2013-08-20
#   first shot

# -- todos --------------------------------------------------------------------
# TODO - required could be a decorator

# -- config -------------------------------------------------------------------
# order matters in XML, we must have the keys list !
ORDERED_ACCEPTED_KEYS = [
    'scenario',
    # line 140: [1:7]
    'intervenants', 'siteshydro', 'sitesmeteo',
    'seuilshydro', 'seuilsmeteo', 'modelesprevision',
    # line 180: [7:]
    'evenements', 'courbestarage', 'jaugeages', 'courbescorrection',
    'serieshydro', 'seriesmeteo', 'seriesobselab', 'seriesobselabmeteo',
    'simulations', 'seriesgradients', 'validsannee'
]


# sandre hydrometrie namespaces
NS = (
    'http://xml.sandre.eaufrance.fr/scenario/hydrometrie/{version}',
    'http://www.w3.org/2001/XMLSchema-instance')
NS_ATTR = {
    'xmlns': NS[0],
    '{%s}schemaLocation' % NS[1]: '%s %s/sandre_sc_hydrometrie.xsd' % (
        NS[0], NS[0])}


def _to_xml(scenario=None, intervenants=None, siteshydro=None, sitesmeteo=None,
            seuilshydro=None, seuilsmeteo=None, modelesprevision=None,
            evenements=None, courbestarage=None, jaugeages=None,
            courbescorrection=None, serieshydro=None, seriesmeteo=None,
            seriesobselab=None, seriesobselabmeteo=None,
            simulations=None, seriesgradients=None, validsannee=None,
            bdhydro=False, strict=True,
            ordered=False, version=None):
    """Return a etree.Element a partir des donnees passes en argument.

    Cette fonction est privee et les utilisateurs sont invites a utiliser la
    classe xml.Message comme interface d'ecriture des fichiers XML Hydrometrie.

    Arguments:
        scenario (xml.Scenario) = 1 element
        intervenants (intervenant.Intervenant collection) = iterable or None
        siteshydro (sitehydro.Sitehydro collection) = iterable or None
        sitesmeteo (sitemeteo.Sitemeteo collection) = iterable or None
        seuilshydro (seuil.Seuilhydro collection) = iterable or None
        seuilsmeteo (seuil.Seuilmeteo collection) = iterable or None
        modelesprevision (modeleprevision.Modeleprevision collection) =
            iterable or None
        evenements (evenement.Evenement collection) = iterable ou None
        courbestarage (courbetarage.CourbeTarage collection) = iterable ou None
        jaugeages (jaugeage.Jaugeage collection) = iterable ou None
        courbescorrection (courbecorrection.CourbeCorrection collection) =
            iterable ou None
        serieshydro (obshydro.Serie collection) = iterable or None
        seriesmeteo (obsmeteo.Serie collection) = iterable or None
        seriesobselab(obselaboreehydro.SerieObsElab collection) =
            iterable or None
        seriesobselabmeteo(obselaboreemeteo.SerieObsElabMeteo collection) =
            iterable or None
        seriesgradients(gradienthydro.Seriesgradients collection) =
            iterable or None
        simulations (simulation.Simulation collection) = iterable or None
        validsannee (_validannee.ValidsAnnee) = années hydrométriques
        bdhydro (bool, defaut False) = controle de conformite bdhydro
        strict (bool, defaut True) = controle de conformite XML Hydrometrie
        ordered (bool, default False) = essaie de conserver l'ordre de certains elements
        version (str or None) = version Sandre 1.1 ou 2 ou None
            si None utilisation du la version du scenario

    """
    # make a deep copy of locals() which is a dict {arg_name: arg_value, ...}
    # keep only Message items
    # and replace default empty lists with None
    args = {
        k: (v if (k == 'validsannee' or v != []) else None) for k, v in locals().items()
        if k in ORDERED_ACCEPTED_KEYS}

    if version is None and args['scenario'] is not None:
        version = args['scenario'].version

    story = _collections.OrderedDict()
    # add the scenario
    if args['scenario'] is not None:
        story['Scenario'] = {'sub': _to_story.scenario_to_story(scenario, bdhydro, strict)}
    refhyd = _collections.OrderedDict()
    if args['intervenants'] is not None:
        intervenants = [_to_story.intervenant_to_story(intervenant, bdhydro, strict)
                        for intervenant in args['intervenants']]
        refhyd['Intervenants'] = {'list': intervenants, 'subtag': 'Intervenant'}
    if args['siteshydro'] is not None:
        siteshydro = [_to_story.sitehydro_to_story(sitehydro, bdhydro, strict)
                      for sitehydro in args['siteshydro']]
        refhyd['SitesHydro'] = {'list': siteshydro, 'subtag': 'SiteHydro'}
    if args['sitesmeteo'] is not None:
        sitesmeteo = [_to_story.sitemeteo_to_story(sitemeteo, bdhydro, strict)
                      for sitemeteo in args['sitesmeteo']]
        refhyd['SitesMeteo'] = {'list': sitesmeteo, 'subtag': 'SiteMeteo'}
    if args['modelesprevision'] is not None:
        modeles = [_to_story.modeleprevision_to_story(modele, bdhydro, strict)
                   for modele in args['modelesprevision']]
        refhyd['ModelesPrevision'] = {'list': modeles, 'subtag': 'ModelePrevision'}
    if args['seuilshydro'] is not None:
        seuilshydro = [_to_story.seuilhydro_to_story(seuil, bdhydro, strict)
                       for seuil in args['seuilshydro']]
        refhyd['SeuilsHydro'] = {'list': seuilshydro, 'subtag': 'SeuilHydro'}
    if args['seuilsmeteo'] is not None:
        seuilsmeteo = [_to_story.seuilmeteo_to_story(seuil, bdhydro, strict)
                       for seuil in args['seuilsmeteo']]
        refhyd['SeuilsMeteo'] = {'list': seuilsmeteo, 'subtag': 'SeuilMeteo'}
    if refhyd:
        story['RefHyd'] = {'sub': refhyd}

    donnees = _collections.OrderedDict()
    if args['evenements'] is not None:
        evenements = [_to_story.evenement_to_story(evenement, bdhydro, strict)
                      for evenement in args['evenements']]
        donnees['Evenements'] = {'list': evenements, 'subtag': 'Evenement'}
    if args['courbestarage'] is not None:
        courbestarage = [_to_story.courbetarage_to_story(courbe, bdhydro, strict)
                         for courbe in args['courbestarage']]
        donnees['CourbesTarage'] = {'list': courbestarage, 'subtag': 'CourbeTarage'}
    if args['jaugeages'] is not None:
        jaugeages = [_to_story.jaugeage_to_story(jaugeage, bdhydro, strict)
                     for jaugeage in args['jaugeages']]
        donnees['Jaugeages'] = {'list': jaugeages, 'subtag': 'Jaugeage'}
    if args['courbescorrection'] is not None:
        ccors = [_to_story.courbecorrection_to_story(ccor, bdhydro, strict)
                 for ccor in args['courbescorrection']]
        donnees['CourbesCorrH'] = {'list': ccors, 'subtag': 'CourbeCorrH'}
    if args['serieshydro'] is not None:
        serieshydro = [_to_story.seriehydro_to_story(serie, bdhydro, strict)
                       for serie in args['serieshydro']]
        donnees['SeriesObsHydro'] = {'list': serieshydro, 'subtag': 'SerieObsHydro'}
    if args['seriesmeteo'] is not None:
        seriesmeteo = [_to_story.seriemeteo_to_story(serie, bdhydro, strict)
                       for serie in args['seriesmeteo']]
        donnees['SeriesObsMeteo'] = {'list': seriesmeteo, 'subtag': 'SerieObsMeteo'}
    if args['seriesobselab'] is not None:
        seriesobselab = [_to_story.serieobselab_to_story(serie)
                         for serie in args['seriesobselab']]
        donnees['SeriesObsElaborHydro'] = {'list': seriesobselab, 'subtag': 'SerieObsElaborHydro'}
    if args['seriesobselabmeteo'] is not None:
        seriesobselabmeteo = [_to_story.serieobselabmeteo_to_sory(serie, bdhydro, strict)
                              for serie in args['seriesobselabmeteo']]
        donnees['SeriesObsElaborMeteo'] = {'list': seriesobselabmeteo,
                                           'subtag': 'SerieObsElaborMeteo'}
    if args['seriesgradients'] is not None:
        gradients = _to_story.seriesgradients_to_story(args['seriesgradients'], bdhydro, strict)
        donnees['GradsHydro'] = {'list': gradients, 'subtag': 'GrdsGradHydro'}
    if args['validsannee'] is not None:
        valids = _to_story.validsannee_to_story(args['validsannee'], bdhydro, strict)
        donnees['ValidsAnneeHydro'] = {'list': valids, 'subtag': 'ValidAnneeHydro'}
    if args['simulations'] is not None:
        simuls = [_to_story.simulation_to_story(simul, bdhydro, strict)
                  for simul in args['simulations']]
        donnees['Simuls'] = {'list': simuls, 'subtag': 'Simul'}
    if donnees:
        story['Donnees'] = {'sub': donnees}

    # init the tree
    if bdhydro:
        tree = _etree.Element('hydrometrie')
    else:
        ns_attr = {}
        for key, value in NS_ATTR.items():
            ns_attr[key] = value.format(version=version)
        tree = _etree.Element('hydrometrie',
                              attrib=ns_attr)
    return _factory(root=tree, story=story)


# -- utility functions --------------------------------------------------------
def _factory(root, story):
    """Return the <root> element including elements described in story.

    Story is a dictionnary which keys are the XML tags to create and values
    one of the possible 3 forms:

        0./ Rule is an _etree_Element
            add the element to root
        1./ Rule to create a sub-element (recursif)
        -------------------------------------------
        This rule is processed like:
            {'sub': a sub-story dictionnary}

        2./ Rule to create a single element or a serie of the same element
        ------------------------------------------------------------------
        This rule is processed like:
            {
                'value': the text value or an iterable of text values,
                'attr': {the tag attributes} (default None)
                'force': bool (default False)
            }

        If value is an iterable, an XML tag is created for each item of values.

        When force is True, a None value create the element tag, otherwise
        rule is left.

    WARNING: as order matters for XML Hydrometrie files, one must use
             collections.OrderedDict to store the story.

    """
    # parse story
    for tag, rule in story.items():

        # DEBUG - print(rule)
        if isinstance(rule, _etree._Element):
            root.append(rule)
            continue

        # recursif call for sub-element
        if 'sub' in rule:
            child = _etree.SubElement(root, tag)
            root.append(_factory(root=child, story=rule.get('sub')))

        if 'list' in rule and rule['list']:
            if 'subtag' in rule:
                # Build a children element <tag> and add to it several <subtag> elements
                # xml element <tag><subtag/><subtag/></tag>
                subtag = rule['subtag']
                child = _etree.SubElement(root, tag)
                root.append(child)
            else:
                # build several children <tag>
                subtag = tag
                child = root
            for substory in rule['list']:
                subchild = _etree.SubElement(child, subtag)
                child.append(_factory(root=subchild, story=substory))

        # single element or multi elements
        if 'value' in rule:

            # init
            value = rule.get('value')
            attr = rule.get('attr', None)
            force = rule.get('force', False)

            # empty tag
            if (value is None) and (not force):
                continue
            # bool to str
            if isinstance(value, bool):
                value = str(value).lower()

            # for a simple tag, we make a list
            if not isinstance(value, (list, tuple)):
                value = [value]

            # finally we create a tag for each item in the list
            for text in value:
                root.append(
                    _make_element(tag_name=tag, text=text, tag_attrib=attr))

    # return
    return root


def _make_element(tag_name, text, tag_attrib=None):
    """Return etree.Element <tag_name {attrib}>str(text)</tag_name>."""
    # DEBUG - print(locals())
    element = _etree.Element(_tag=tag_name, attrib=tag_attrib)
    if text is not None:
        element.text = str(text)
    return element
