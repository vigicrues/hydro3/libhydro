"""Parse JsonNode or XMLNode."""

import datetime
import collections as _collections
from libhydro.conv.xml import Scenario
from libhydro.core import (
    _composant, intervenant as _intervenant, sitehydro as _sitehydro,
    sitemeteo as _sitemeteo, seuil as _seuil,
    modeleprevision as _modeleprevision, obshydro as _obshydro,
    obsmeteo as _obsmeteo, simulation as _simulation, evenement as _evenement,
    courbetarage as _courbetarage, courbecorrection as _courbecorrection,
    jaugeage as _jaugeage, obselaboreehydro as _obselaboreehydro,
    obselaboreemeteo as _obselaboreemeteo,
    _composant_site, rolecontact as _rolecontact, zonehydro as _zonehydro,
    gradienthydro as _gradienthydro, validannee as _validannee)
from libhydro.conv.xml.sandre_tags import SandreTagsV2

# -- Emetteur and Destinataire named tuples -----------------------------------
RefHyd = _collections.namedtuple('RefHyd', ['intervenants', 'siteshydro', 'sitesmeteo',
                                            'seuilshydro', 'seuilsmeteo', 'modelesprevision'])
Donnees = _collections.namedtuple(
    'Donnees', ['evenements', 'courbestarage', 'courbescorrection',
                'jaugeages', 'serieshydro', 'seriesmeteo',
                'seriesobselab', 'seriesobselabmeteo', 'seriesgradients',
                'simulations', 'validsannee'])


class VersionError(Exception):
    """Version error is raised"""
    pass


def parse(root):
    """Parse a root Node"""
    scenario = _parse_scenario(root.subobj('Scenario'))
    refhyd = _parse_refhyd(root)
    donnees = _parse_donnees(root)
    return {'scenario': scenario, 'intervenants': refhyd.intervenants,
            'siteshydro': refhyd.siteshydro, 'sitesmeteo': refhyd.sitesmeteo,
            'seuilshydro': refhyd.seuilshydro, 'seuilsmeteo': refhyd.seuilsmeteo,
            'modelesprevision': refhyd.modelesprevision,
            'evenements': donnees.evenements, 'courbestarage': donnees.courbestarage,
            'courbescorrection': donnees.courbescorrection,
            'jaugeages': donnees.jaugeages, 'serieshydro': donnees.serieshydro,
            'seriesmeteo': donnees.seriesmeteo, 'seriesobselab': donnees.seriesobselab,
            'seriesobselabmeteo': donnees.seriesobselabmeteo,
            'seriesgradients': donnees.seriesgradients, 'simulations': donnees.simulations,
            'validsannee': donnees.validsannee}


def _parse_refhyd(root):
    """Parse to RefHYd object."""
    refhyd = root.subobj('RefHyd')
    if refhyd is None:
        return RefHyd(intervenants=[], siteshydro=[], sitesmeteo=[],
                      seuilshydro=[], seuilsmeteo=[], modelesprevision=[])
    return RefHyd(
        intervenants=[_parse_intervenant(inter) for inter in
                      refhyd.sublist('Intervenants', 'Intervenant')],
        siteshydro=[_parse_sitehydro(site) for site in
                    refhyd.sublist('SitesHydro', 'SiteHydro')],
        sitesmeteo=[_parse_sitemeteo(site) for site in
                    refhyd.sublist('SitesMeteo', 'SiteMeteo')],
        seuilshydro=[_parse_seuilhydro(seuil) for seuil in
                     refhyd.sublist('SeuilsHydro', 'SeuilHydro')],
        seuilsmeteo=[_parse_seuilmeteo(seuil) for seuil in
                     refhyd.sublist('SeuilsMeteo', 'SeuilMeteo')],
        modelesprevision=[_parse_modeleprevision(modele) for modele in
                          refhyd.sublist('ModelesPrevision', 'ModelePrevision')]
    )


def _parse_donnees(hyd):
    """Parse to Donnees object."""
    donnees = hyd.subobj('Donnees')
    if donnees is None:
        return Donnees(None, None, None, None, None, None, None, None, None, None, None)
    return Donnees(
        evenements=[_parse_evenement(evt)
                    for evt in donnees.sublist('Evenements', 'Evenement')],
        courbestarage=[_parse_courbetarage(courbe)
                       for courbe in donnees.sublist('CourbesTarage', 'CourbeTarage')],
        courbescorrection=[_parse_courbecorrection(courbe)
                           for courbe in donnees.sublist('CourbesCorrH', 'CourbeCorrH')],
        jaugeages=[_parse_jaugeage(jaugeage)
                   for jaugeage in donnees.sublist('Jaugeages', 'Jaugeage')],
        serieshydro=[_parse_seriehydro(serie)for serie in
                     donnees.sublist(SandreTagsV2.serieshydro, SandreTagsV2.seriehydro)],
        seriesmeteo=[_parse_seriemeteo(serie) for serie in
                     donnees.sublist('SeriesObsMeteo', 'SerieObsMeteo')],
        seriesobselab=[_parse_serieobselab(serie) for serie in
                       donnees.sublist(SandreTagsV2.seriesobselabhydro, 'SerieObsElaborHydro')],
        seriesobselabmeteo=[_parse_serieobselabmeteo(serie) for serie in
                            donnees.sublist('SeriesObsElaborMeteo', 'SerieObsElaborMeteo')],
        seriesgradients=_parse_gradients(donnees),
        simulations=[_parse_simulation(sim)
                     for sim in donnees.sublist('Simuls', 'Simul')],
        validsannee=_parse_validsannee(donnees)
    )


def _parse_scenario(scen):
    """Parse Scenario node and return a xml.Scenario."""
    version = scen.value('VersionScenario')
    if version is None or version < '2':
        raise VersionError('Version must be present and higher than 2')
    # emetteur pas de contacts si absence de balise CdContact
    emetteur_contacts = None
    emetteur = scen.subobj('Emetteur')
    emetteur_cdcontact = emetteur.value('CdContact') if emetteur is not None else None
    if emetteur_cdcontact is not None:
        emetteur_contacts = _intervenant.Contact(code=emetteur_cdcontact)
    # destinataire pas de contacts si absence de balise CdContact
    dest_contacts = None
    dest = scen.subobj('Destinataire')
    dest_cdcontact = dest.value('CdContact') if dest is not None else None
    if dest_cdcontact is not None:
        dest_contacts = _intervenant.Contact(code=dest_cdcontact)

    return Scenario(
        emetteur=_intervenant.Intervenant(
            code=emetteur.value('CdIntervenant'),
            nom=emetteur.value('NomIntervenant'),
            contacts=emetteur_contacts, ),
        destinataire=_intervenant.Intervenant(
            code=dest.value('CdIntervenant'),
            nom=dest.value('NomIntervenant'),
            contacts=dest_contacts, ),
        dtprod=scen.value('DateHeureCreationFichier'),
        reference=scen.value('RefFichier'),
        envoi=scen.value('RefFichierEnvoi'),
        version=scen.value('VersionScenario'))


def _parse_intervenant(inter):
    """Return a intervenant.Intervenant from a Node Intervenant"""
    # prepare args
    args = {
        'code': inter.value('CdIntervenant'),
        'origine': inter.attrib('CdIntervenant', 'schemeAgencyID'),
        'nom': inter.value('NomIntervenant'),
        'statut': inter.value('StIntervenant'),
        'dtcreation': inter.value('DateCreationIntervenant'),
        'dtmaj': inter.value('DateMajIntervenant'),
        'auteur': inter.value('AuteurIntervenant'),
        'mnemo': inter.value('MnIntervenant'),
        'adresse': _parse_adresse(inter, 'Intervenant'),
        'commentaire': inter.value('CommentairesIntervenant'),
        'activite': inter.value('ActivitesIntervenant'),
        'nominternational': inter.value('NomInternationalIntervenant'),
        'siret': inter.value('CdSIRETRattacheIntervenant'),
    }
    commune = inter.subobj('Commune')
    if commune is not None:
        cdcommune = commune.value('CdCommune')
        lbcommune = commune.value('LbCommune')
        args['commune'] = _composant_site.Commune(
            code=cdcommune, libelle=lbcommune)

    args['telephone'] = inter.value('TelephoneComplementIntervenant')
    args['fax'] = inter.value('FaxComplementIntervenant')
    args['siteweb'] = inter.value('SiteWebComplementIntervenant')

    args['contacts'] = [_parse_contact(contact)
                        for contact in inter.sublist('Contacts', 'Contact')]

    # build an Intervenant
    intervenant = _intervenant.Intervenant(**args)
    # update the Contacts
    for contact in intervenant.contacts:
        contact.intervenant = intervenant
    # return
    return intervenant


def _parse_contact(contact, intervenant=None):
    """Parse Contact Node and return a intervenant.Contact."""
    # prepare args
    args = {}
    args['code'] = contact.value('CdContact')
    args['nom'] = contact.value('NomContact')
    args['prenom'] = contact.value('PrenomContact')
    args['civilite'] = contact.value('CiviliteContact', int)
    args['intervenant'] = intervenant
    profil = contact.value('ProfilContact')
    if profil is not None:
        args['profil'] = profil
    args['adresse'] = _parse_adresse(contact, 'Contact')
    args['fonction'] = contact.value('FonctionContact')
    args['telephone'] = contact.value('TelephoneContact')
    args['portable'] = contact.value('PortContact')
    args['fax'] = contact.value('FaxContact')
    args['mel'] = contact.value('MelContact')
    args['dtmaj'] = contact.value('DateMajContact')

    args['profilsadmin'] = [
        _parse_profiladmin(profil)
        for profil in contact.sublist('ProfilsAdminLocal', 'ProfilAdminLocal')
    ]

    args['alias'] = contact.value('AliasContact')
    args['motdepasse'] = contact.value('MotPassContact')
    args['dtactivation'] = contact.value('DtActivationContact')
    args['dtdesactivation'] = contact.value('DtDesactivationContact')

    # build a Contact and return
    return _intervenant.Contact(**args)


def _parse_adresse(node, entite):
    """Return an intervenant.Adresse from conact or Intevenant Node."""
    args = {}
    if entite == 'Intervenant':
        args['adresse1'] = node.value('RueIntervenant')
        args['adresse1_cplt'] = node.value('ImmoIntervenant')
        args['lieudit'] = node.value('LieuIntervenant')
        args['boitepostale'] = node.value('BpIntervenant')
        args['dep'] = node.value('DepIntervenant')
        args['codepostal'] = node.value('CP' + entite)
        args['pays'] = node.value('PaysComplementIntervenant')
        args['adresse2'] = node.value('AdEtrangereComplementIntervenant')
    else:
        args['adresse1'] = node.value('Ad' + entite)
        args['codepostal'] = node.value('Cp' + entite)
        args['pays'] = node.value('Pays' + entite)
        args['adresse2'] = node.value('AdEtrangere' + entite)
    args['ville'] = node.value('Ville' + entite)

    return _intervenant.Adresse(**args)


def _parse_profiladmin(profil):
    """Parse ProfilAdminlocal Node and return ProfilAdminLocal."""
    args = {'profil': profil.value('CdProfilAdminLocal')}
    args['zoneshydro'] = [_parse_zonehydro(zone)
                          for zone in profil.sublist('ZonesHydro', 'ZoneHydro')]
    args['dtactivation'] = profil.value('DtActivationProfilAdminLocal')
    args['dtdesactivation'] = profil.value('DtDesactivationProfilAdminLocal')
    return _intervenant.ProfilAdminLocal(**args)


def _parse_zonehydro(zone):
    """Parse ZoneHydro Node"""
    args = {
        'code':  zone.value('CdZoneHydro'),
        'libelle': zone.value('LbZoneHydro')
    }
    return _zonehydro.Zonehydro(**args)


def _parse_sitehydro(site):
    """Return a sitehydro.Sitehydro from a SiteHydro Node."""
    args = {
        'code': site.value('CdSiteHydro'),
        'codeh2': site.value('CdSiteHydroAncienRef'),
        'precisiontype': site.value('PrecisionTypSiteHydro'),
        'libelle': site.value('LbSiteHydro'),
        'libelleusuel': site.value('LbUsuelSiteHydro')
    }
    typesite = site.value('TypSiteHydro')
    if typesite is not None:
        args['typesite'] = typesite

    coord = site.subobj('CoordSiteHydro')
    if coord is not None:
        args['coord'] = _parse_coord(coord, 'SiteHydro')

    args['stations'] = [
        _parse_station(station)
        for station in site.sublist('StationsHydro', 'StationHydro')
    ]

    args['communes'] = [_parse_commune(commune)
                        for commune in site.sublist('Communes', 'Commune')]

    args['entitesvigicrues'] = [
        _parse_tronconvigilance(troncon)
        for troncon in site.sublist(SandreTagsV2.entsvigicru, SandreTagsV2.entvigicru)
    ]

    entitehydro = site.subobj('EntiteHydrographique')
    if entitehydro is not None:
        codeentite = entitehydro.value('CdEntiteHydrographique')
        lbentite = entitehydro.value('NomEntiteHydrographique')
        args['entitehydro'] = _zonehydro.Entitehydro(
            code=codeentite, libelle=lbentite)

    args['tronconhydro'] = site.value('CdTronconHydrographique')

    zonehydro = site.subobj('ZoneHydro')
    if zonehydro is not None:
        cdzonehydro = zonehydro.value('CdZoneHydro')
        lbzonehydro = zonehydro.value('LbZoneHydro')
        args['zonehydro'] = _zonehydro.Zonehydro(
            code=cdzonehydro, libelle=lbzonehydro)
    args['precisioncoursdeau'] = site.value('PrecisionCoursDEauSiteHydro')
    args['mnemo'] = site.value('MnSiteHydro')
    args['complementlibelle'] = site.value(SandreTagsV2.comtlbsitehydro)
    args['pkamont'] = site.value('PkAmontSiteHydro', float)
    args['pkaval'] = site.value('PkAvalSiteHydro', float)
    altel = site.subobj('AltiSiteHydro')
    if altel is not None:
        altitude = altel.value('AltitudeSiteHydro')
        sysalti = altel.value('SysAltimetriqueSiteHydro', int)
        args['altitude'] = _composant_site.Altitude(altitude=altitude,
                                                    sysalti=sysalti)

    args['dtmaj'] = site.value(SandreTagsV2.dtmajsitehydro)
    args['bvtopo'] = site.value('BassinVersantSiteHydro', float)
    args['bvhydro'] = site.value('BassinVersantHydroSiteHydro', float)
    args['fuseau'] = site.value('FuseauHoraireSiteHydro', int)

    args['statut'] = site.value(SandreTagsV2.stsitehydro, int)

    args['dtpremieredonnee'] = site.value('DtPremDonSiteHydro')
    args['moisetiage'] = site.value('PremMoisEtiageSiteHydro', int)
    args['moisanneehydro'] = site.value('PremMoisAnHydSiteHydro', int)
    args['dureecrues'] = site.value('DureeCarCruSiteHydro', int)
    args['publication'] = site.value('DroitPublicationSiteHydro', int)
    args['essai'] = site.value('EssaiSiteHydro', bool)
    args['influence'] = site.value('InfluGeneSiteHydro', int)
    args['influencecommentaire'] = site.value('ComInfluGeneSiteHydro')
    args['commentaire'] = site.value('ComSiteHydro')

    siteassocie = site.subobj('SiteHydroAssocie')
    if siteassocie is not None:
        code = siteassocie.value('CdSiteHydro')
        args['siteassocie'] = _sitehydro.Sitehydro(code=code)

    args['periodes_shv'] = [
        _parse_periode_shv(periode)
        for periode in site.sublist('PeriodesActiviteSiteHydroVirtuel',
                                    'PeriodeActiviteSiteHydroVirtuel')
    ]

    args['massedeau'] = site.value('CdEuMasseDEau')

    args['loisstat'] = [
        _parse_loistat(loi, 'SiteHydro')
        for loi in site.sublist('LoisStatContexteSiteHydro', 'LoiStatContexteSiteHydro')
    ]

    args['images'] = [
        _parse_image(image, 'SiteHydro')
        for image in site.sublist('ImagesSiteHydro', 'ImageSiteHydro')
    ]

    args['roles'] = [
        _parse_role(role,  'SiteHydro')
        for role in site.sublist(SandreTagsV2.rolscontactsitehydro,
                                 SandreTagsV2.rolcontactsitehydro)
    ]

    args['sitesamont'] = [
        _parse_siteamontaval(amont)
        for amont in site.sublist('SitesHydroAmont', 'SiteHydroAmont')
    ]

    args['sitesaval'] = [
        _parse_siteamontaval(aval)
        for aval in site.sublist('SitesHydroAval', 'SiteHydroAval')
    ]

    args['pluiesbassin'] = [
        _parse_pluiebassin(pluie)
        for pluie in site.sublist(SandreTagsV2.pluiesdebassin, SandreTagsV2.pluiedebassin)
    ]

    args['cdbnbv'] = site.value('CdBNBV')

    # build a Sitehydro and return
    return _sitehydro.Sitehydro(**args)


def _parse_tronconvigilance(troncon):
    """Parse troncon vigilance NOde."""
    # prepare args
    args = {
        'code': troncon.value(SandreTagsV2.cdentvigicru),
        'libelle': troncon.value(SandreTagsV2.nomentvigicru)
    }
    # build a Tronconvigilance and return
    return _composant_site.EntiteVigiCrues(**args)


def _parse_periode_shv(periode):
    """Parse Periode of virtual site"""
    args = {
        'dtactivation': periode.value('DtDebActivationPeriodeActiviteSiteHydroVirtuel'),
        'dtdesactivation': periode.value('DtFinActivationPeriodeActiviteSiteHydroVirtuel'),
        'dtdeb': periode.value('DtDebPeriodeActiviteSiteHydroVirtuel'),
        'dtfin': periode.value('DtFinPeriodeActiviteSiteHydroVirtuel')
    }
    args['sitesattaches'] = [
        _parse_siteattache(site)
        for site in periode.sublist('SitesHydroAttaches', 'SiteHydroAttache')
    ]

    return _sitehydro.PeriodeSitehydrovirtuel(**args)


def _parse_siteattache(site):
    """Parse siteattache"""
    sitehydro = site.subobj('SiteHydro')
    libelle = sitehydro.value('LbSiteHydro')
    args = {
        'code': sitehydro.value('CdSiteHydro'),
        'ponderation': site.value('PonderationSiteHydroAttache', float),
        'decalage': site.value('DecalSiteHydroAttache', int)
    }
    siteattache = _sitehydro.Sitehydroattache(**args)
    siteattache.libelle = libelle
    return siteattache


def _parse_siteamontaval(site):
    args = {
        'code': site.value('CdSiteHydro'),
        'libelle': site.value('LbSiteHydro')
    }
    return _sitehydro.Sitehydro(**args)


def _parse_pluiebassin(pluie):
    args = {
        'code': pluie.value('CdSiteMeteo'),
        'ponderation': pluie.value(SandreTagsV2.ponderationpluiedebassin, float)
    }
    return _sitemeteo.SitemeteoPondere(**args)


def _parse_coord(coord, entite):
    """Return a dict {'x': x, 'y': y, 'proj': proj}.

    Arg entite is the xml element suffix, a string in
    (SiteHydro, StationHydro).

    """
    coord = {
        'x': coord.value(f'CoordX{entite}', float),
        'y': coord.value(f'CoordY{entite}', float),
        'proj': coord.value(f'ProjCoord{entite}', int)
    }
    return coord


def _parse_commune(commune):
    """Return a _composant_site.Commune
       from a dict Commune.
    """
    return _composant_site.Commune(
        code=commune.value('CdCommune'),
        libelle=commune.value('LbCommune')
    )


def _parse_station(station):
    """Return a sitehydro.Station from a dict StationHydro."""
    # prepare args
    args = {
        'code': station.value('CdStationHydro'),
        'libelle': station.value('LbStationHydro')
    }
    typestation = station.value('TypStationHydro')
    if typestation is not None:
        args['typestation'] = typestation
    args['libellecomplement'] = station.value(SandreTagsV2.complementlibellestationhydro)
    args['commentaireprive'] = station.value(SandreTagsV2.comprivestationhydro)
    args['dtmaj'] = station.value(SandreTagsV2.dtmajstationhydro)
    coord = station.subobj('CoordStationHydro')
    if coord is not None:
        args['coord'] = _parse_coord(coord, 'StationHydro')
    args['pointk'] = station.value('PkStationHydro', float)
    args['dtmiseservice'] = station.value('DtMiseServiceStationHydro')
    args['dtfermeture'] = station.value('DtFermetureStationHydro')
    args['surveillance'] = station.value('ASurveillerStationHydro', bool)
    niveauaffichage = station.value('NiveauAffichageStationHydro')
    if niveauaffichage is not None:
        args['niveauaffichage'] = niveauaffichage

    droitpublication = station.value('DroitPublicationStationHydro', int)
    if droitpublication is not None:
        args['droitpublication'] = droitpublication

    args['delaidiscontinuite'] = station.value('DelaiDiscontinuiteStationHydro', int)
    args['delaiabsence'] = station.value('DelaiAbsenceStationHydro', int)
    args['essai'] = station.value('EssaiStationHydro', bool)
    args['influence'] = station.value('InfluLocaleStationHydro', int)
    args['influencecommentaire'] = station.value('ComInfluLocaleStationHydro')
    args['commentaire'] = station.value('ComStationHydro')

    # Récupération stations antérieures et posterieures

    args['stationsanterieures'] = [
        _parse_substation(ant)
        for ant in station.sublist('StationsHydroAnterieures',
                                   'StationHydroAnterieure')
    ]

    args['stationsposterieures'] = [
        _parse_substation(post)
        for post in station.sublist('StationsHydroPosterieures',
                                    'StationHydroPosterieure')
        ]

    args['qualifsdonnees'] = [
        _parse_qualifdonnees(qualif)
        for qualif in station.sublist('QualifsDonneesStationHydro',
                                      'QualifDonneesStationHydro')
    ]

    args['finalites'] = [
        finalite.value('CdFinaliteStationHydro', int)
        for finalite in station.sublist('FinalitesStationHydro', 'FinaliteStationHydro')
    ]

    args['loisstat'] = [
        _parse_loistat(loi, 'StationHydro')
        for loi in station.sublist('LoisStatContexteStationHydro', 'LoiStatContexteStationHydro')
    ]

    args['images'] = [
        _parse_image(image, 'StationHydro')
        for image in station.sublist('ImagesStationHydro', 'ImageStationHydro')
    ]

    args['roles'] = [
        _parse_role(role, entite='StationHydro')
        for role in station.sublist(SandreTagsV2.rolscontactstationhydro,
                                    SandreTagsV2.rolcontactstationhydro)
    ]

    args['plages'] = [
        _parse_plage(plage, 'StationHydro')
        for plage in station.sublist('PlagesUtilStationHydro', 'PlageUtilStationHydro')
    ]

    args['reseaux'] = [
        _composant_site.ReseauMesure(code=reseau.value('CodeSandreRdd'),
                                     libelle=reseau.value('NomRdd'))
        for reseau in station.sublist('ReseauxMesureStationHydro', 'ReseauMesureStationHydro')
    ]

    args['capteurs'] = [
        _parse_capteur(capteur)
        for capteur in station.sublist('Capteurs', 'Capteur')
    ]

    args['refsalti'] = [
        _parse_refalti(refalti)
        for refalti in station.sublist('RefsAlti', 'RefAlti')
    ]

    args['codeh2'] = station.value('CdStationHydroAncienRef')

    commune = station.subobj('Commune')
    if commune is not None:
        args['commune'] = _parse_commune(commune)

    # stations amont et aval

    args['stationsamont'] = [
        _parse_substation(amont)
        for amont in station.sublist('StationsHydroAmont', 'StationHydroAmont')
    ]

    args['stationsaval'] = [
        _parse_substation(aval)
        for aval in station.sublist('StationsHydroAval', 'StationHydroAval')
    ]

    # plages stations fille et mère

    args['plagesstationsfille'] = [
        _parse_plagestation(plagestation, 'stationHydroFille')
        for plagestation in
        station.sublist('PlagesAssoStationHydroFille', 'PlageAssoStationHydroFille')
    ]

    args['plagesstationsmere'] = [
        _parse_plagestation(plagestation, 'stationHydroMere')
        for plagestation in station.sublist('PlagesAssoStationHydroMere',
                                            'PlageAssoStationHydroMere')
    ]

    # build a Station and return
    return _sitehydro.Station(**args)


def _parse_substation(station):
    """Return a sitehydro.Station with only code and libelle"""
    if station is None:
        return None
    args = {'code': station.value('CdStationHydro'),
            'libelle': station.value('LbStationHydro')}
    # TODO only return args
    return _sitehydro.Station(**args)


def _parse_qualifdonnees(qualif):
    """Return a _composant_site.QualifDonnees
    from a <QualifDonneesStationHydro> element
    """
    return _composant_site.QualifDonnees(
        coderegime=qualif.value('CdRegime', int),
        qualification=qualif.value('QualifDonStationHydro', int),
        commentaire=qualif.value('ComQualifDonStationHydro')
    )


def _parse_loistat(loi, entite):
    """Return an composant_site.LoiStat
       from a dict <LoiStatContexteSiteHydro> or <LoiStatContexteStationHydro>
    """
    return _composant_site.LoiStat(
        contexte=loi.value('TypContexteLoiStat', int),
        loi=loi.value('TypLoi' + entite, int)
    )


def _parse_image(img, entite):
    """Return a _composant_site.Image from a dict Image"""
    args = {
        'adresse': img.value('AdressedelImage' + entite),
        'typeill': img.value('TypIll' + entite, int),
        # 'image': img.value('ImageIll' + entite, str),
        'formatimg': img.value('FormatIll' + entite),
        'commentaire': img.value('ComImg' + entite)
    }
    return _composant_site.Image(**args)


def _parse_role(role, entite):
    """Return a _rolecontact.Role
       from a dict RolContactSiteHydro or RolContactStationHydro.
    """
    args = {
        'contact': _intervenant.Contact(code=role.value('CdContact'))
    }
    rolebalise = 'RoleContact' + entite
    args['role'] = role.value(rolebalise)
    args['dtdeb'] = role.value('DtDebutContact' + entite)
    args['dtfin'] = role.value('DtFinContact' + entite)
    args['dtmaj'] = role.value(SandreTagsV2.dtmajrolecontact + entite)
    return _rolecontact.RoleContact(**args)


def _parse_plage(plage, entite):
    """Return a sitehydro.PlageUtil

    from a <PlageUtilStationHydro> element or >PlageUtilCapteur> element

    Arg entite is the xml element suffix, a string in
    (StationHydro, Capteur).

    """
    if plage is None:
        return None
    args = {
        'dtdeb': plage.value(f'DtDebPlageUtil{entite}'),
        'dtfin': plage.value(f'DtFinPlageUtil{entite}'),
        'dtactivation': plage.value(f'DtActivationPlageUtil{entite}'),
        'dtdesactivation': plage.value(f'DtDesactivationPlageUtil{entite}'),
        'active': plage.value(f'ActivePlageUtil{entite}', bool)
    }
    return _sitehydro.PlageUtil(**args)


def _parse_refalti(alti):
    """Return a _composant_site.RefAlti
    from a dict RefAlti
    """
    if alti is None:
        return None
    return _composant_site.RefAlti(
        dtdeb=alti.value('DtDebutRefAlti'),
        dtfin=alti.value('DtFinRefAlti'),
        dtactivation=alti.value('DtActivationRefAlti'),
        dtdesactivation=alti.value('DtDesactivationRefAlti'),
        altitude=_composant_site.Altitude(
            altitude=alti.value('AltitudeRefAlti', float),
            sysalti=alti.value('SysAltiRefAlti', int)),
        dtmaj=alti.value('DtMajRefAlti')
    )


def _parse_plagestation(plage, tag_station):
    """Return a composant_site.PlageStation from a PlageAssoStationHydroFille
    or a PlageAssoStationHydroMere element
    """
    station = _parse_substation(plage.subobj(tag_station))

    return _sitehydro.PlageStation(
        code=station.code,
        libelle=station.libelle,
        dtdeb=plage.value('DtDebPlageAssoStationHydroMereFille'),
        dtfin=plage.value('DtFinPlageAssoStationHydroMereFille'),
        dtmaj=plage.value('DtMajPlageAssoStationHydroMereFille')
    )


def _parse_capteur(capteur):
    """Return a sitehydro.Capteur from dict Capteur."""
    # prepare args
    args = {
        'code': capteur.value('CdCapteur'),
        'codeh2': capteur.value('CdCapteurAncienRef'),
        'libelle': capteur.value('LbCapteur'),
        'mnemo': capteur.value('MnCapteur'),
        'surveillance': capteur.value('ASurveillerCapteur', bool),
        'dtmaj': capteur.value(SandreTagsV2.dtmajcapteur),
        'pdt': capteur.value(SandreTagsV2.pdtcapteur, int),
        'essai': capteur.value('EssaiCapteur', bool),
        'commentaire': capteur.value('ComCapteur')
    }
    typecapteur = capteur.value('TypCapteur', int)
    # Radar ->Radar hauteur
    if typecapteur == 4:
        typecapteur = 16
    elif typecapteur == 12:
        # Limni -> Flotteur
        typecapteur = 6
    elif typecapteur == 3:
        # Ultrasons -> Ultrason immergé
        typecapteur = 8
    if typecapteur is not None:
        args['typecapteur'] = typecapteur
    typemesure = capteur.value('TypMesureCapteur')
    if typemesure is not None:
        args['typemesure'] = typemesure

    args['plages'] = [
        _parse_plage(plage, 'Capteur')
        for plage in capteur.sublist('PlagesUtilCapteur', 'PlageUtilCapteur')
    ]
    observateur = capteur.subobj('Observateur')
    if observateur is not None:
        codecontact = observateur.value('CdContact')
        if codecontact is not None:
            args['observateur'] = _intervenant.Contact(code=codecontact)

    # build a Capteur and return
    return _sitehydro.Capteur(**args)


def _parse_sitemeteo(site):
    """Return a sitemeteo.Sitemeteo and seuils from a <SiteMeteo> element."""
    # prepare args
    args = {}
    args['code'] = site.value('CdSiteMeteo')
    args['libelle'] = site.value('LbSiteMeteo')
    args['libelleusuel'] = site.value('LbUsuelSiteMeteo')
    args['mnemo'] = site.value('MnSiteMeteo')
    args['lieudit'] = site.value('LieuDitSiteMeteo')

    coord = site.subobj('CoordSiteMeteo')
    if coord is not None:
        args['coord'] = _parse_coord(coord, 'SiteMeteo')

    alti = site.subobj('AltiSiteMeteo')
    if alti is not None:
        args['altitude'] = _parse_altitude(alti, site='SiteMeteo')
    args['fuseau'] = site.value('FuseauHoraireSiteMeteo')
    args['dtmaj'] = site.value(SandreTagsV2.dtmajsitemeteo)
    args['dtouverture'] = site.value('DtOuvertureSiteMeteo')
    args['dtfermeture'] = site.value('DtFermSiteMeteo')
    args['droitpublication'] = site.value('DroitPublicationSiteMeteo', bool)
    args['essai'] = site.value('EssaiSiteMeteo', bool)
    args['commentaire'] = site.value('ComSiteMeteo')

    args['images'] = [
        _parse_image(image, 'SiteMeteo')
        for image in site.sublist('ImagesSiteMeteo', 'ImageSiteMeteo')]

    commune = site.subobj('Commune')
    if commune is not None:
        args['commune'] = _parse_commune(commune)

    args['reseaux'] = [
        _composant_site.ReseauMesure(code=reseau.value('CodeSandreRdd'),
                                     libelle=reseau.value('NomRdd'))
        for reseau in site.sublist('ReseauxMesureSiteMeteo', 'RSX')]

    args['roles'] = [
        _parse_role(role, 'SiteMeteo')
        for role in site.sublist(SandreTagsV2.rolscontactsitemeteo,
                                 SandreTagsV2.rolcontactsitemeteo)]

    zonehydro = site.subobj('ZoneHydro')

    if zonehydro is not None:
        cdzonehydro = zonehydro.value('CdZoneHydro')
        lbzonehydro = zonehydro.value('LbZoneHydro')
        args['zonehydro'] = _zonehydro.Zonehydro(
             code=cdzonehydro, libelle=lbzonehydro)

    args['visites'] = [
        _parse_visite(visite)
        for visite in site.sublist('VisitesSiteMeteo', 'VisiteSiteMeteo')]

    # build a Sitemeteo
    sitemeteo = _sitemeteo.Sitemeteo(**args)
    # add the Grandeurs
    for grd in site.sublist('GrdsMeteo', 'GrdMeteo'):
        sitemeteo.grandeurs.append(_parse_grandeur(grd, sitemeteo))
    # return
    return sitemeteo


def _parse_altitude(alti, site):
    """return a _composant_site.Altitude from a <AltiSiteMeteo> element"""
    args = {'altitude':  alti.value('Altitude' + site, float),
            'sysalti': alti.value('SysAltimetrique' + site, int)}
    return _composant_site.Altitude(**args)


def _parse_grandeur(grd, sitemeteo=None):
    """Return a sitemeteo.Grandeur and seuils from a <GrdMeteo> element."""
    # prepare args
    args = {}
    args['typemesure'] = grd.value('CdGrdMeteo')
    args['dtmiseservice'] = grd.value('DtMiseServiceGrdMeteo')
    args['dtfermeture'] = grd.value('DtFermetureServiceGrdMeteo')
    args['essai'] = grd.value('EssaiGrdMeteo', bool)
    if sitemeteo is not None:
        args['sitemeteo'] = sitemeteo
    args['surveillance'] = grd.value('ASurveillerGrdMeteo', bool)
    args['delaiabsence'] = grd.value('DelaiAbsGrdMeteo', int)
    args['pdt'] = grd.value(SandreTagsV2.pdtgrdmeteo, int)

    args['classesqualite'] = [
            _parse_classequalite(classe)
            for classe in grd.sublist('ClassesQualiteGrd', 'ClasseQualiteGrd')]
    args['dtmaj'] = grd.value('DtMajGrdMeteo')
    # build a Grandeur and return
    return _sitemeteo.Grandeur(**args)


def _parse_classequalite(classe):
    """Return a sitemeteo.ClasseQualite from a <ClasseQualiteGrd> element."""
    args = {}
    args['classe'] = classe.value('CdqClasseQualiteGrd')
    dtvisite = classe.value('DtVisiteSiteMeteo')
    if dtvisite is not None:
        args['visite'] = _sitemeteo.Visite(dtvisite=dtvisite)
    args['dtdeb'] = classe.value('DtDebutClasseQualiteGrd')
    args['dtfin'] = classe.value('DtFinClasseQualiteGrd')
    return _sitemeteo.ClasseQualite(**args)


def _parse_visite(visite):
    """Parse VisiteSiteMeteoreturn obj an retrun a sitemeteo.Visite."""
    args = {}
    args['dtvisite'] = visite.value('DtVisiteSiteMeteo')
    codecontact = visite.value('CdContact')
    if codecontact is not None:
        args['contact'] = _intervenant.Contact(code=codecontact)
    args['methode'] = visite.value('MethClassVisiteSiteMeteo')
    args['modeop'] = visite.value('ModeOperatoireUtiliseVisiteSiteMeteo')
    return _sitemeteo.Visite(**args)


def _parse_courbecorrection(courbe):
    return _courbecorrection.CourbeCorrection(
        station=_sitehydro.Station(code=courbe.value('CdStationHydro')),
        libelle=courbe.value('LbCourbeCorrH'),
        commentaire=courbe.value('ComCourbeCorrH'),
        pivots=[_parse_pivotcc(pivot)
                for pivot in courbe.sublist('PointsPivot', 'PointPivot')],
        dtmaj=courbe.value('DtMajCourbeCorrH')
    )


def _parse_pivotcc(pivot):
    return _courbecorrection.PivotCC(
        dte=pivot.value('DtPointPivot'),
        deltah=pivot.value('DeltaHPointPivot', float) or 0,
        dtactivation=pivot.value('DtActivationPointPivot'),
        dtdesactivation=pivot.value(SandreTagsV2.dtdesactivationpointpivot)
    )


def _parse_jaugeage(jau):
    codesite = jau.value('CdSiteHydro')
    site = _sitehydro.Sitehydro(code=codesite)
    # mode not mandatory -> constructor default value
    mode = jau.value('ModeJaugeage')
    if mode is not None:
        mode = int(mode)
    args = {
        'code': jau.value('CdJaugeage'),
        'dte': jau.value('DtJaugeage'),
        'debit': jau.value('DebitJaugeage', float),
        'dtdeb': jau.value('DtDebJaugeage'),
        'dtfin': jau.value('DtFinJaugeage'),
        'section_mouillee': jau.value('SectionMouilJaugeage', float),
        'perimetre_mouille': jau.value('PerimMouilleJaugeage', float),
        'largeur_miroir': jau.value('LargMiroirJaugeage', float),
        'commentaire': jau.value('ComJaugeage'),
        'vitessemoy': jau.value('VitesseMoyJaugeage', float),
        'vitessemax': jau.value('VitesseMaxJaugeage', float),
        'vitessemax_surface': jau.value(SandreTagsV2.vitessemaxsurface, float),
        'site': site,
        'hauteurs': [
            _parse_hjaug(hjau)
            for hjau in jau.sublist('HauteursJaugeage', 'HauteurJaugeage')],
        'dtmaj': jau.value('DtMajJaugeage')
    }
    if mode is not None:
        args['mode'] = mode

    args['numero'] = jau.value('NumJaugeage')
    args['incertitude_calculee'] = jau.value('IncertCalJaugeage', float)
    args['incertitude_retenue'] = jau.value('IncertRetenueJaugeage', float)
    qualification = jau.value('QualifJaugeage')
    if qualification is not None:
        args['qualification'] = qualification
    args['commentaire_prive'] = jau.value('ComPrivJaugeage')
    args['courbestarage'] = [_parse_ctjaugeage(ctjau)
                             for ctjau in jau.sublist('CourbesTarage', 'CourbeTarage')]

    return _jaugeage.Jaugeage(**args)


def _parse_hjaug(hjau):
    codestation = hjau.value('CdStationHydro')
    station = _sitehydro.Station(code=codestation)
    stationfille = None
    element_fille = hjau.subobj('StationFille')
    if element_fille is not None:
        codestationfille = element_fille.value('CdStationHydro')
        stationfille = _sitehydro.Station(code=codestationfille)
    args = {
        'station': station,
        'sysalti': hjau.value('SysAltiStationJaugeage'),
        'coteretenue': hjau.value('CoteRetenueStationJaugeage'),
        'cotedeb': hjau.value('CoteDebutStationJaugeage'),
        'cotefin': hjau.value('CoteFinStationJaugeage'),
        'denivele': hjau.value('DnStationJaugeage'),
        'distancestation': hjau.value('DistanceStationJaugeage'),
        'stationfille': stationfille,
        'dtdeb_refalti': hjau.value('DtDebutRefAlti')
    }
    return _jaugeage.HauteurJaugeage(**args)


def _parse_ctjaugeage(ctjau):
    """Return a jaugeage.CourbeTarageJaugeage from a <CourbeTarage> element"""
    return _jaugeage.CourbeTarageJaugeage(
        code=ctjau.value('CdCourbeTarage', int),
        libelle=ctjau.value('LbCourbeTarage'))


def _parse_evenement(evt):
    # prepare args
    # entite can be a Sitehydro, a Station or a Sitemeteo
    args = {}
    cdsitehydro = evt.value('CdSiteHydro')
    if cdsitehydro is not None:
        entite = _sitehydro.Sitehydro(
            code=cdsitehydro)
    else:
        cdstation = evt.value('CdStationHydro')
        if cdstation is not None:
            entite = _sitehydro.Station(
                code=evt.value('CdStationHydro'))
        else:
            cdsitemeteo = evt.value('CdSiteMeteo')
            entite = _sitemeteo.Sitemeteo(
                code=cdsitemeteo)
    args['entite'] = entite
    args['dtmaj'] = evt.value('DtMajEvenement')
    # Conversion nomenclature 534 en 874
    publication = evt.value(SandreTagsV2.publicationevenement, int)
    if publication is not None:
        args['publication'] = publication

    args['descriptif'] = evt.value('DescEvenement')
    args['contact'] = _intervenant.Contact(
        code=evt.value('CdContact'))
    args['dt'] = evt.value('DtEvenement')

    typeevt = evt.value('TypEvenement')
    if typeevt is not None:
        args['typeevt'] = typeevt
    ressources = []
    for ressource in evt.sublist('RessEvenement', 'ResEvenement'):
        url = ressource.value('UrlResEvenement')
        libelle = ressource.value('LbResEvenement')
        ressources.append(_evenement.Ressource(url=url,
                                               libelle=libelle))
    args['ressources'] = ressources
    args['dtfin'] = evt.value('DtFinEvenement')
    # build an Eveement and return
    return _evenement.Evenement(**args)


def _parse_courbetarage(courbe):
    # build a Contact
    # balise CodeContact Non obligatoire
    contact = None
    cdcontact = courbe.value('CdContact')
    if cdcontact is not None:
        contact = _intervenant.Contact(code=cdcontact)
    typect = courbe.value('TypCourbeTarage', int)
    args = {
        'code': courbe.value('CdCourbeTarage'),
        'libelle': courbe.value('LbCourbeTarage'),
        'typect': typect,
        'limiteinf': courbe.value('LimiteInfCourbeTarage', float),
        'limitesup': courbe.value('LimiteSupCourbeTarage', float),
        'dn': courbe.value('DnCourbeTarage', float),
        'alpha': courbe.value('AlphaCourbeTarage', float),
        'beta': courbe.value('BetaCourbeTarage', float),
        'commentaire': courbe.value('ComCourbeTarage'),
        'station': _sitehydro.Station(code=courbe.value('CdStationHydro')),
        'contact': contact,
        'pivots': [
            _parse_pivotct(e, typect)
            for e in courbe.sublist('PivotsCourbeTarage', 'PivotCourbeTarage')],
        'periodes': [
            _parse_periodect(periode)
            for periode in courbe.sublist('PeriodesUtilisationCourbeTarage',
                                          'PeriodeUtilisationCourbeTarage')],
        'dtmaj': courbe.value('DtMajCourbeTarage')
    }

    args['limiteinfpub'] = courbe.value('LimiteInfPubCourbeTarage', float)
    args['limitesuppub'] = courbe.value('LimiteSupPubCourbeTarage', float)
    args['dtcreation'] = courbe.value('DtCreatCourbeTarage')
    args['commentaireprive'] = courbe.value('ComPrivCourbeTarage')

    return _courbetarage.CourbeTarage(**args)


def _parse_pivotct(pivot, typect):
    # qualif is not mandatory
    args = {'hauteur': pivot.value('HtPivotCourbeTarage', float)}
    qualif = pivot.value('QualifPivotCourbeTarage', int)
    if qualif is not None:
        args['qualif'] = qualif
    if typect == 0:
        args['debit'] = pivot.value('QPivotCourbeTarage', float)
        return _courbetarage.PivotCTPoly(**args)
    if typect == 4:
        args['vara'] = pivot.value('VarAPivotCourbeTarage', float)
        args['varb'] = pivot.value('VarBPivotCourbeTarage', float)
        args['varh'] = pivot.value('VarHPivotCourbeTarage', float)
        return _courbetarage.PivotCTPuissance(**args)
    raise ValueError('TypCourbeTarage must be 0 or 4')


def _parse_periodect(periode):
    """Return a PeriodeCT from  <PeriodeUtilisationCourbeTarage> element."""
    return _courbetarage.PeriodeCT(
        dtdeb=periode.value(SandreTagsV2.dtdebperiodeutilct),
        dtfin=periode.value('DtFinPeriodeUtilisationCourbeTarage'),
        etat=periode.value('EtatPeriodeUtilisationCourbeTarage', int),
        histos=[
            _parse_histoct(histo)
            for histo in periode.sublist(SandreTagsV2.histosactivationperiode,
                                         SandreTagsV2.histoactivationperiode)]
    )


def _parse_histoct(histo):
    """Return HistoActivePeriode from <HistoActivPeriod>"""
    return _courbetarage.HistoActivePeriode(
        dtactivation=histo.value(SandreTagsV2.dtactivationhistoperiode),
        dtdesactivation=histo.value(SandreTagsV2.dtdesactivationhistoperiode)
    )


def _parse_seuilhydro(seuil):
    # prepare args
    args = {}
    site = seuil.subobj('SiteHydro')
    if site is not None:
        args['sitehydro'] = _parse_sitehydro(site)

    args['code'] = seuil.value(SandreTagsV2.cdseuilhydro)
    typeseuil = seuil.value(SandreTagsV2.typseuilhydro)
    if typeseuil is not None:
        args['typeseuil'] = typeseuil
    duree = seuil.value(SandreTagsV2.dureeseuilhydro, int)
    if duree is not None:
        args['duree'] = duree
    args['nature'] = seuil.value(SandreTagsV2.natureseuilhydro)
    args['libelle'] = seuil.value(SandreTagsV2.lbusuelseuilhydro)
    args['mnemo'] = seuil.value(SandreTagsV2.mnseuilhydro)
    args['gravite'] = seuil.value(SandreTagsV2.indicegraviteseuilhydro)
    args['commentaire'] = seuil.value(SandreTagsV2.comseuilhydro)

    args['publication'] = seuil.value(SandreTagsV2.typpubliseuilhydro, int)

    args['valeurforcee'] = seuil.value(SandreTagsV2.valforceeseuilhydro, bool)
    args['dtmaj'] = seuil.value(SandreTagsV2.dtmajseuilhydro)
    seuilhydro = _seuil.Seuilhydro(**args)

    # add the values
    args['valeurs'] = [
        _parse_valeurseuilhydro(valeur, seuilhydro)
        for valeur in seuil.sublist('ValsSeuilHydro', 'ValSeuilHydro')
    ]
    # add valeurs and return
    seuilhydro.valeurs = args['valeurs']
    return seuilhydro


def _parse_valeurseuilhydro(valeur, seuil):
    # prepare args
    args = {}
    args['valeur'] = valeur.value('ValValSeuilHydro')
    args['seuil'] = seuil

    station = valeur.subobj('StationHydro')
    if station is not None:
        args['entite'] = _parse_station(station)
    else:
        site = valeur.subobj('SiteHydro')
        if site is not None:
            args['entite'] = _parse_sitehydro(site)
        else:
            capteur = valeur.subobj('Capteur')
            if capteur is not None:
                args['entite'] = _parse_capteur(capteur)

    args['tolerance'] = valeur.value('ToleranceValSeuilHydro')
    args['dtactivation'] = valeur.value('DtActivationValSeuilHydro')
    args['dtdesactivation'] = valeur.value('DtDesactivationValSeuilHydro')
    # build a Valeurseuil and return
    return _seuil.Valeurseuil(**args)


def _parse_seuilmeteo(seuil):
    # prepare args
    args = {}
    site = seuil.subobj('SiteMeteo')
    sitemeteo = _parse_sitemeteo(site)
    grd = seuil.subobj('GrdMeteo')
    args['grandeurmeteo'] = _parse_grandeur(grd, sitemeteo)

    args['code'] = seuil.value(SandreTagsV2.cdseuilmeteo)

    typeseuil = seuil.value(SandreTagsV2.typseuilmeteo)
    if typeseuil is not None:
        args['typeseuil'] = typeseuil
    args['nature'] = seuil.value(SandreTagsV2.natureseuilmeteo)
    duree = seuil.value(SandreTagsV2.dureeseuilmeteo, int)
    if duree is not None:
        args['duree'] = duree
    args['libelle'] = seuil.value(SandreTagsV2.lbusuelseuilmeteo)
    args['mnemo'] = seuil.value(SandreTagsV2.mnseuilmeteo)
    args['gravite'] = seuil.value(SandreTagsV2.indicegraviteseuilmeteo)
    args['dtmaj'] = seuil.value(SandreTagsV2.dtmajseuilmeteo)
    args['commentaire'] = seuil.value(SandreTagsV2.comseuilmeteo)
    seuilmeteo = _seuil.Seuilmeteo(**args)

    # add the values
    args['valeurs'] = [
        _parse_valeurseuilmeteo(valeur, seuilmeteo)
        for valeur in seuil.sublist('ValsSeuilMeteo', 'ValSeuilMeteo')]

    # add valeurs and return
    seuilmeteo.valeurs = args['valeurs']
    return seuilmeteo


def _parse_valeurseuilmeteo(valeur, seuilmeteo):
    """Return a seuil.Valeurseuil from a <ValSeuilMeteo> element."""
    args = {'seuil': seuilmeteo,
            'entite': seuilmeteo.grandeurmeteo}
    args['valeur'] = valeur.value(SandreTagsV2.valvalseuilmeteo)
    args['tolerance'] = valeur.value(SandreTagsV2.tolerancevalseuilmeteo)
    args['dtactivation'] = valeur.value(SandreTagsV2.dtactivationvalseuilmeteo)
    args['dtdesactivation'] = valeur.value(SandreTagsV2.dtdesactivationvalseuilmeteo)
    return _seuil.Valeurseuil(**args)


def _parse_modeleprevision(modele):
    """Return a modeleprevision.Modeleprevision from a """
    # prepare args
    args = {}
    cdcontact = modele.value('CdContact')
    if cdcontact is not None:
        args['contact'] = _intervenant.Contact(code=cdcontact)
    args['code'] = modele.value('CdModelePrevision')
    args['libelle'] = modele.value('LbModelePrevision')
    typemodele = modele.value('TypModelePrevision', int)
    if typemodele is not None:
        args['typemodele'] = typemodele
    args['description'] = modele.value('DescModelePrevision')
    args['dtmaj'] = modele.value('DtMajModelePrevision')

    args['siteshydro'] = []
    for site in modele.sublist('SitesHydro', 'SiteHydro'):
        code = site.value('CdSiteHydro')
        args['siteshydro'].append(_sitehydro.Sitehydro(code=code))
    # build a Modeleprevision and return
    return _modeleprevision.Modeleprevision(**args)


def _parse_seriehydro(serie):
    # prepare args
    # entite can be a Sitehydro, a Station or a Capteur
    cdsitehydro = serie.value('CdSiteHydro')
    entite = None
    if cdsitehydro is not None:
        entite = _sitehydro.Sitehydro(code=cdsitehydro)
    else:
        cdstation = serie.value('CdStationHydro')
        if cdstation is not None:
            entite = _sitehydro.Station(
                code=cdstation)
        else:
            cdcapteur = serie.value('CdCapteur')
            if cdcapteur is not None:
                entite = _sitehydro.Capteur(code=cdcapteur)
    # build a Contact
    # balise CodeContact Non obligatoire
    contact = None
    cdcontact = serie.value('CdContact')
    if cdcontact is not None:
        contact = _intervenant.Contact(code=cdcontact)

    # utilisation d'un dictionnaire afin que sysalti ne soit pas transmis
    # au constructeur si la balise n'existe pas
    obss = serie.sublist('ObssHydro', 'ObsHydro')
    args = {
        'entite': entite,
        'grandeur': serie.value(SandreTagsV2.grdseriehydro),
        'dtdeb': serie.value(SandreTagsV2.dtdebseriehydro),
        'dtfin': serie.value(SandreTagsV2.dtfinseriehydro),
        'dtprod': serie.value(SandreTagsV2.dtprodseriehydro),
        'perime': serie.value(SandreTagsV2.serieperimhydro, bool),
        'contact': contact,
        'observations': _parse_obsshydro(obss) if obss else None}
    # balise sysalti
    sysalti = serie.value(SandreTagsV2.sysaltiseriehydro, int)
    if sysalti is not None:
        args['sysalti'] = sysalti

    # balise pdt
    pdt_duree = serie.value(SandreTagsV2.pdtseriehydro, int)
    if pdt_duree is not None:
        args['pdt'] = _composant.PasDeTemps(
            duree=pdt_duree, unite=_composant.PasDeTemps.MINUTES)

    # build a Serie and return
    return _obshydro.Serie(**args)


def _parse_obsshydro(obss):
    observations = []
    mapping = {
        'DtObsHydro': ('dte', str),
        'ResObsHydro': ('res', float),
        'MethObsHydro': ('mth', int),
        'QualifObsHydro': ('qal', int),
        'ContObsHydro': ('cnt', int),
        SandreTagsV2.statutobshydro: ('statut', int)
    }
    for obs in obss:
        args = {}
        for tag, txt in obs.to_dict().items():
            tmp = mapping[tag]
            args[tmp[0]] = _cast(txt, tmp[1])
        if 'mth' in args and args['mth'] == 12:
            args['mth'] = 8
        if args['res'] is None:
            continue
        observations.append(_obshydro.Observation(**args))
    # build the Observations and return
    return _obshydro.Observations(*observations).sort_index()


def _parse_seriemeteo(serie):
    # build a Grandeur
    grandeur = _sitemeteo.Grandeur(
        typemesure=serie.value('CdGrdMeteo'),
        sitemeteo=_sitemeteo.Sitemeteo(serie.value('CdSiteMeteo')))
    # prepare the duree in minutes
    duree = serie.value('DureeSerieObsMeteo', int) or 0
    # build a Contact
    cdcontact = serie.value('CdContact')
    if cdcontact is not None:
        contact = _intervenant.Contact(code=cdcontact)
    else:
        contact = None
    obss = serie.sublist('ObssMeteo', 'ObsMeteo')
    # build a Serie without the observations and return
    return _obsmeteo.Serie(
        grandeur=grandeur,
        duree=duree * 60,
        dtprod=serie.value('DtProdSerieObsMeteo'),
        dtdeb=serie.value('DtDebSerieObsMeteo'),
        dtfin=serie.value('DtFinSerieObsMeteo'),
        contact=contact,
        observations=_parse_obssmeteo(obss) if obss else None
    )


def _parse_obssmeteo(obss):
    observations = [_parse_obsmeteo(obs) for obs in obss]
    # build the Observations and return
    return _obsmeteo.Observations(*observations).sort_index()


def _parse_obsmeteo(obs):
    args = {}
    args['dte'] = obs.value('DtObsMeteo')
    args['res'] = obs.value('ResObsMeteo')
    if args['res'] is None:
        return None
    mth = obs.value('MethObsMeteo', int)
    if mth is not None:
        args['mth'] = mth
    qal = obs.value('QualifObsMeteo', int)
    if qal is not None:
        args['qal'] = qal
    qua = obs.value('IndiceQualObsMeteo', int)
    if qua is not None:
        args['qua'] = qua

    statut = obs.value('StObsMeteo', int)
    if statut is not None:
        args['statut'] = statut

    ctxt = obs.value('ContxtObsMeteo', int)
    if ctxt is not None:
        args['ctxt'] = ctxt
    # build the Observation and return
    return _obsmeteo.Observation(**args)


def _parse_serieobselab(serie):
    """Return a obselaboreehydro.SerieObsElab
       from a SerieObsElaborHydro element.
    """
    args_serie = {}
    cdsitehydro = serie.value('CdSiteHydro')
    if cdsitehydro is not None:
        args_serie['entite'] = _sitehydro.Sitehydro(code=cdsitehydro)
    else:
        cdstation = serie.value('CdStationHydro')
        if cdstation is not None:
            args_serie['entite'] = _sitehydro.Station(code=cdstation)

    args_serie['dtprod'] = serie.value('DtProdSerieObsElaborHydro')
    # mandatory
    args_serie['typegrd'] = serie.value('TypDeGrdSerieObsElaborHydro')

    unite = None
    if args_serie['typegrd'][-1] == 'J':
        unite = _composant.PasDeTemps.JOURS
    elif args_serie['typegrd'][-1] == 'H':
        unite = _composant.PasDeTemps.HEURES

    duree = serie.value('PDTSerieObsElaborHydro', int)
    if duree is not None and unite is not None:
        args_serie['pdt'] = _composant.PasDeTemps(duree=duree,
                                                  unite=unite)

    args_serie['dtdeb'] = serie.value('DtDebPlagSerieObsElaborHydro')
    args_serie['dtfin'] = serie.value('DtFinPlagSerieObsElaborHydro')
    args_serie['dtdesactivation'] = serie.value('DtDesactivationSerieObsElaborHydro')
    args_serie['dtactivation'] = serie.value('DtActivationSerieObsElaborHydro')

    sysalti = serie.value('SysAltiSerieObsElaborHydro', int)
    if sysalti is not None:
        args_serie['sysalti'] = sysalti

    glissante = serie.value('GlissanteSerieObsElaborHydro', bool)
    if glissante is not None:
        args_serie['glissante'] = glissante

    args_serie['dtdebrefalti'] = serie.value('DtDebutRefAlti')

    cdcontact = serie.value('CdContact')
    if cdcontact is not None:
        args_serie['contact'] = _intervenant.Contact(code=cdcontact)

    serieelab = _obselaboreehydro.SerieObsElab(**args_serie)

    observations = []
    for obs in serie.sublist('ObssElaborHydro', 'ObsElaborHydro'):
        args = {}
        args['dte'] = obs.value('DtObsElaborHydro')  # mandatory
        args['dtres'] = obs.value('DtResObsElaborHydro')  # Sandre V2.1
        if args['dtres'] is None:
            args['dtres'] = args['dte']
        args['res'] = obs.value('ResObsElaborHydro', float)  # mandatory

        cnt = obs.value('ContObsElaborHydro', int)
        if cnt is not None:
            args['cnt'] = cnt

        statut = obs.value('StObsElaborHydro', int)
        if statut is not None:
            args['statut'] = statut

        qal = obs.value('QualifObsElaborHydro', int)
        if qal is not None:
            args['qal'] = qal
        mth = obs.value('MethObsElaborHydro', int)
        if mth is not None:
            args['mth'] = mth
        observations.append(_obselaboreehydro.ObservationElaboree(**args))

    # add observations to serie
    if len(observations) > 0:
        serieelab.observations = _obselaboreehydro.ObservationsElaborees(
            *observations).sort_index()
    return serieelab


def _parse_serieobselabmeteo(serie):
    """Return a obselaboreemeteo.SerieObsElabMeteo
       from a SeriesObsElaborMeteo element.
    """
    args_serie = {}
    cdsitehydro = serie.value('CdSiteHydro')
    if cdsitehydro is not None:
        args_serie['site'] = _sitehydro.Sitehydro(code=cdsitehydro)
    else:
        code = serie.value('CdSiteMeteo')
        if code is not None:
            ponderation = serie.value('ValPondSiteMeteo', float)
            args_serie['site'] = _sitemeteo.SitemeteoPondere(
                code=code,
                ponderation=ponderation)
    args_serie['grandeur'] = serie.value('CdGrdSerieObsElaborMeteo')
    args_serie['typeserie'] = serie.value('TypSerieObsElaborMeteo')
    dtdeb = serie.value('DtDebSerieObsElaborMeteo')
    if dtdeb is not None:
        args_serie['dtdeb'] = dtdeb
    dtfin = serie.value('DtFinSerieObsElaborMeteo')
    if dtfin is not None:
        args_serie['dtfin'] = dtfin
    duree = serie.value('DureeSerieObsElaborMeteo', int)
    if duree is not None:
        args_serie['duree'] = 60 * duree
    ipa = serie.subobj('SerieObsElaborMeteoIpa')
    if ipa is not None:
        args_ipa = {}
        args_ipa['coefk'] = ipa.value('KSerieObsElaborMeteoIpa', float)
        npdt = ipa.value('PDTSerieObsElaborMeteoIpa', int)
        if npdt is not None:
            args_ipa['npdt'] = npdt
        args_serie['ipa'] = _obselaboreemeteo.Ipa(**args_ipa)

    serieelab = _obselaboreemeteo.SerieObsElabMeteo(**args_serie)

    observations = []
    for obs in serie.sublist('ObssElaborMeteo', 'ObsElaborMeteo'):
        args = {}
        args['dte'] = obs.value('DtObsElaborMeteo')  # mandatory
        args['res'] = obs.value('ResObsElaborMeteo', float)  # mandatory
        qua = obs.value('IndiceQualObsElaborMeteo', float)
        if qua is not None:
            args['qua'] = qua
        qal = obs.value('QualifObsElaborMeteo', int)
        if qal is not None:
            args['qal'] = qal
        mth = obs.value('MethObsElaborMeteo', int)
        if mth is not None:
            args['mth'] = mth
        statut = obs.value('StObsElaborMeteo', int)
        if statut is not None:
            args['statut'] = statut

        observations.append(_obselaboreemeteo.ObsElabMeteo(**args))

    # add observations to serie
    if len(observations) > 0:
        serieelab.observations = _obselaboreemeteo.ObssElabMeteo(
            *observations).sort_index()
    return serieelab


def _parse_gradients(donnees):
    seriesgradients = []
    # if element is None:
    #     return seriesgradients
    serieslistgradients = {}
    series = _collections.OrderedDict()
    for grdgrad in donnees.sublist('GradsHydro', 'GrdsGradHydro'):
        grd = grdgrad.value('GrdGradHydro')
        for grad in grdgrad.sublist(None, 'GradHydro'):
            args = {}
            dtprod = grad.value('DtProdGradHydro')
            dtprod = datetime.datetime.strptime(
                dtprod, '%Y-%m-%dT%H:%M:%S')
            args['dte'] = grad.value('DtObsGradHydro')
            duree = grad.value('DureeGradHydro', int)
            args['res'] = grad.value('ResGradHydro', float)
            args['statut'] = grad.value(SandreTagsV2.stgradhydro, int)
            args['qal'] = grad.value('QualifGradHydro', int)
            args['mth'] = grad.value('MethQualifGradHydro', int)

            gradient = _gradienthydro.Gradient(**args)

            cdentite = grad.value('CdStationHydro')
            if cdentite is not None:
                entite = _sitehydro.Station(code=cdentite)
            else:
                cdentite = grad.value('CdSiteHydro')
                if cdentite is not None:
                    entite = _sitehydro.Sitehydro(code=cdentite)
                else:
                    cdentite = grad.value('CdCapteur')
                    entite = _sitehydro.Capteur(code=cdentite)
            cdcontact = grad.value('CdContact')
            if cdcontact is not None:
                contact = _intervenant.Contact(code=cdcontact)
            else:
                contact = None
            key = (grd, cdentite, duree, cdcontact)
            if key not in series:
                serieslistgradients[key] = [gradient]
                series[key] = _gradienthydro.SerieGradients(
                    entite=entite, grd=grd, dtprod=dtprod, duree=duree,
                    contact=contact)
            else:
                if series[key].dtprod < dtprod:
                    series[key].dtprod = dtprod
                serieslistgradients[key].append(gradient)
    for key, serie in series.items():
        serie.gradients = _gradienthydro.Gradients(*serieslistgradients[key])
        seriesgradients.append(serie)

    return seriesgradients


def _parse_simulation(sim):
    # prepare args
    # entite can be a Sitehydro or a Station
    entite = None
    cdsite = sim.value('CdSiteHydro')
    if cdsite is not None:
        entite = _sitehydro.Sitehydro(
            code=sim.value('CdSiteHydro'))
    else:
        cdstation = sim.value('CdStationHydro')
        if cdstation is not None:
            entite = _sitehydro.Station(
                code=sim.value('CdStationHydro'))
    args = {
        'entite': entite,
        'dtprod': sim.value('DtProdSimul'),
        'grandeur': sim.value('GrdSimul'),
        'commentaire': sim.value('ComSimul'),
    }

    statut = sim.value(SandreTagsV2.statutsimul, int)
    if statut is not None:
        args['statut'] = statut

    # prepare qualite
    # warning: qualite is int(float())
    qualite = sim.value(SandreTagsV2.qualitesimul, float)
    if qualite is not None:
        qualite = int(qualite)
    args['qualite'] = qualite

    cdmodele = sim.value('CdModelePrevision')
    if cdmodele is not None:
        args['modeleprevision'] = _modeleprevision.Modeleprevision(code=cdmodele)

    cdintervenant = sim.value('CdIntervenant')
    if cdintervenant is not None:
        args['intervenant'] = _intervenant.Intervenant(
            code=cdintervenant)

    publication = sim.value('TypPubliSimul', int)
    if publication is not None:
        args['publication'] = publication
    args['code'] = sim.value('CdSimul', int)
    args['dtfinvalidite'] = sim.value('DtFinValidSimul')
    args['dtdeb'] = sim.value('DtDebSimul')
    args['dtfin'] = sim.value('DtFinSimul')
    args['dtbase'] = sim.value('DtBaseSimul')
    args['dtderobs'] = sim.value('DtDerObsSimul')
    args['modecalcul'] = sim.value('ModeCalSimul')
    args['sysalti'] = sim.value('SysAltiSimul')
    args['contexte'] = sim.value('ContexteSimul')
    args['cmntprive'] = sim.value('ComPrivSimul')
    args['mode'] = sim.value('ModeSimul')
    sim.value('CdModelePrevision')
    cdcontact = sim.value('CdContact')
    if cdcontact is not None:
        args['contact'] = _intervenant.Contact(code=cdcontact)

    args['previsions_tend'] = _parse_prevs_tendance(sim)
    args['previsions_det'] = _parse_prevs_det(sim)
    args['previsions_prb'] = _parse_prevs_prb(sim)
    args['scenario'] = _parse_scenario_simul(sim)
    args['prevs_ensemble'] = _parse_prevs_ensemble(sim)
    args['prevs_evol'] = _parse_prevs_evol(sim)

    return _simulation.Simulation(**args)


def _parse_prevs_tendance(sim):
    prvs = []
    for prev in sim.sublist('PrevsTendance', 'PrevTendance'):
        dte = prev.value('DtPrevTendance')
        incertdte = prev.value('IncertDtPrevTendance', int)
        if incertdte is None:
            incertdte = 0
        res = {
            'moy': prev.value('ResMoyPrevTendance', float),
            'min': prev.value('ResMinPrevTendance', float),
            'max': prev.value('ResMaxPrevTendance', float),
        }
        for tend, value in res.items():
            if value is None:
                continue
            prvs.append(_simulation.PrevisionTendance(
                dte=dte,
                tend=tend,
                res=value,
                incertdte=incertdte
            ))
    if len(prvs) == 0:
        return None
    return _simulation.PrevisionsTendance(*prvs)


def _parse_prevs_det(sim):
    prvs = []
    for prev in sim.sublist('PrevsDeterministe', 'PrevDeterministe'):
        incertdte = prev.value('IncertDtPrevDeterministe', int)
        if incertdte is None:
            incertdte = 0
        prvs.append(_simulation.PrevisionDeterministe(
            dte=prev.value('DtPrevDeterministe'),
            res=prev.value('ResPrevDeterministe', float),
            incertdte=incertdte
        ))
    if len(prvs) == 0:
        return None
    return _simulation.PrevisionsDeterministes(*prvs)


def _parse_prevs_prb(sim):
    """Return PrevisionsPrb from Simul object."""
    prbs = []
    for prev in sim.sublist('PrevsProb', 'PrevProb'):
        dte = prev.value('DtPrevProb')
        for probprev in prev.sublist('ProbsPrev', 'ProbPrev'):
            prbs.append(
                _simulation.PrevisionPrb(
                    dte=dte,
                    res=probprev.value('ResProbPrev', float),
                    prb=probprev.value('PProbPrev', int)))

    if len(prbs) == 0:
        return None
    return _simulation.PrevisionsPrb(*prbs)


def _parse_prevs_ensemble(sim):
    """Return Prevsensemble from a Simul object."""
    prvs = []
    for prev in sim.sublist('PrevsEnsemble', 'PrevEnsemble'):
        dte = prev.value('DtPrevEnsembliste')
        for membre in prev.sublist('MembresPrevEnsemble', 'MembrePrevEnsemble'):
            poids = membre.value('PoidMembrePrevEnsemble', int)
            if poids is None:
                poids = 1
            prvs.append(_simulation.PrevEnsemble(
                dte=dte,
                lb=membre.value('LbMembrePrevEnsemble'),
                res=membre.value('ResMembrePrevEnsemble', float),
                poids=poids
            ))
    if len(prvs) == 0:
        return None
    return _simulation.PrevsEnsemble(*prvs)


def _parse_prevs_evol(sim):
    """Return list of PrevEvol from a Simul object"""
    prvsevol = []
    for elt in sim.sublist('PrevsEvol', 'PrevEvol'):
        evol = elt.value('TypPrevEvol', int)
        dtdeb = elt.value('DtDebPrevEvol')
        incertdte = elt.value('IncertDtPrevEvol', int)
        prvsevol.append(_simulation.PrevEvol(evol=evol, dtdeb=dtdeb,
                                             incertdte=incertdte))
    return prvsevol


def _parse_scenario_simul(sim):
    """Return a ScenarioSimul from ScnSimul element"""
    scen = sim.subobj('ScnSimul')
    if scen is None:
        return None
    return _simulation.ScenarioSimul(
        libelle=scen.value('LbScnSimul'),
        descriptif=scen.value('DescScnSimul'))


def _parse_validsannee(donnees):
    """Return a ValidsAnnee from ValidsAnneeHydro element"""
    validsannee = []
    for vah in donnees.sublist('ValidsAnneeHydro', 'ValidAnneeHydro'):
        args = {}
        args['annee'] = vah.value('AnneeValidAnneeHydro', int)
        cdentite = vah.value('CdStationHydro')
        if cdentite is not None:
            args['entite'] = _sitehydro.Station(code=cdentite)
        else:
            cdentite = vah.value('CdSiteHydro')
            if cdentite is None:
                raise ValueError(
                    '<CdSiteHydro> or <CdStationHydro> is required')
            args['entite'] = _sitehydro.Sitehydro(code=cdentite)
        args['qualif'] = vah.value('QualifValidAnneeHydro', int)
        args['dispoh'] = vah.value('DispoHValidAnneeHydro', int)
        args['dispoq'] = vah.value('DispoQValidAnneeHydro', int)
        args['cmnt'] = vah.value('ComValidAnneeHydro')
        args['dtmaj'] = vah.value('DtMajValidAnneeHydro')
        validsannee.append(_validannee.ValidAnnee(**args))
    if not validsannee:
        return None
    return _validannee.ValidsAnnee(*validsannee)


def _cast(text, cast=str):
    """Cast tag content"""
    if text is None:
        return None
    if cast == bool:
        return str(text.lower()) in ('true', 'vrai', '1')
    return cast(text)
